package pl.gpcs.wordnet.integrator;

import java.nio.file.Paths;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {
	private static final String RESOURCES_PATH = Paths.get("src", "main", "resources").toString();

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class Polnet {
		public static final String PATH = Paths.get(RESOURCES_PATH, "polnet.xml").toString();
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class PlWordnet {
		public static final String PATH_4_2 = Paths.get(RESOURCES_PATH, "plwordnet_4_2_visdisc.xml").toString();
		public static final String PATH_2_3 = Paths.get(RESOURCES_PATH, "plwordnet_2_3_visdisc.xml").toString();
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class Merge {
		public static final int MIN_SYNONYM_MATCHING_COUNT = 1;
		public static final int EDGE_THRESHOLD = 0;
		public static final String PATH = Paths.get(RESOURCES_PATH, "merged.xml").toString();
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class WordnetCreationInstants {
		public static final boolean REMOVE_ENGLISH_SYNSETS = true;
	}

}
