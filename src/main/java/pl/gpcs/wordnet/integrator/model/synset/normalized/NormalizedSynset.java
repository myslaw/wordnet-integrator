package pl.gpcs.wordnet.integrator.model.synset.normalized;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.gpcs.wordnet.integrator.model.common.DomainAware;
import pl.gpcs.wordnet.integrator.model.common.Synset;

@XmlRootElement(name = "SYNSET")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class NormalizedSynset implements Synset<Ilr, Synonym>, DomainAware {
	@XmlElement(name = "ID")
	private String id;
	@XmlElement(name = "POS")
	private String pos;
	@XmlElement(name = "SYNONYM")
	private Synonym synonym;
	@XmlElement(name = "ILR")
	private List<Ilr> ilr;
	@XmlElement(name = "DEF")
	private String def;
	@XmlElement(name = "USAGE")
	private String usage;
	@XmlElement(name = "DOMAIN")
	private String domain;

}
