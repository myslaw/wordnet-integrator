package pl.gpcs.wordnet.integrator.model.util;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.adapter.polnet.PolnetSynsetAdapter;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Literal;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;

@RequiredArgsConstructor
@EqualsAndHashCode(doNotUseGetters = true, onlyExplicitlyIncluded = true)
@Getter
public class ReducedSynset {

	private static final PolnetSynsetAdapter ADAPTER = new PolnetSynsetAdapter();
	private static final String HYPERNYM = "hypernym";

	@EqualsAndHashCode.Include
	private final String id;
	private final List<String> snote;
	private final Set<String> hypernyms;
	private final Set<String> synonyms;

	public ReducedSynset(PolnetSynset polnetSynset) {
		this.id = polnetSynset.getId();
		this.snote = polnetSynset.getSnote();
		if (polnetSynset.getIlr() != null) {
			this.hypernyms = polnetSynset.getIlr().stream().filter(ilr -> HYPERNYM.equals(ilr.getType()))
					.map(Ilr::getText).collect(Collectors.toSet());
		} else {
			this.hypernyms = Collections.emptySet();
		}
		if (polnetSynset.getSynonym() == null || polnetSynset.getSynonym().getLiteral() == null) {
			this.synonyms = Collections.emptySet();
		} else {
			this.synonyms = polnetSynset.getSynonym().getLiteral().stream().map(Literal::getText)
					.filter(Objects::nonNull).collect(Collectors.toSet());
		}
	}
}
