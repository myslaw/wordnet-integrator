package pl.gpcs.wordnet.integrator.model.common;

public interface SynonymLiteral<SenseT> {

	SenseT getSense();

	void setSense(SenseT sense);

	String getText();

	void setText(String text);
}
