package pl.gpcs.wordnet.integrator.model.common;

public interface SynsetIlr {

	public String getText();

	public void setText(String text);

	public String getType();

	public void setType(String type);

}
