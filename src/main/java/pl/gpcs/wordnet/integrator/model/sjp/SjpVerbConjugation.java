package pl.gpcs.wordnet.integrator.model.sjp;

import java.util.Set;

import pl.gpcs.wordnet.integrator.model.verb.Conjugation;

public class SjpVerbConjugation implements Conjugation {
	private String verb;
	private Set<String> conjugations;

	@Override
	public String get() {
		return verb;
	}

	@Override
	public void set(String verb) {
		this.verb = verb;
	}

	@Override
	public Set<String> getConjugations() {
		return conjugations;
	}

	@Override
	public void setConjugations(Set<String> conjugations) {
		this.conjugations = conjugations;
	}

	@Override
	public String toString() {
		return "SjpVerbConjugation [verb=" + verb + ", conjugations=" + conjugations + "]";
	}

}
