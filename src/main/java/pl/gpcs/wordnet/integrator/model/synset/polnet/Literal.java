package pl.gpcs.wordnet.integrator.model.synset.polnet;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlValue;
import pl.gpcs.wordnet.integrator.model.common.SynonymLiteral;

@XmlRootElement(name = "LITERAL")
@XmlAccessorType(XmlAccessType.FIELD)
public class Literal implements SynonymLiteral<String> {
	@XmlAttribute
	private String lnote;
	@XmlAttribute
	private String sense;
	@XmlValue
	private String text;

	public String getLnote() {
		return lnote;
	}

	public void setLnote(String lnote) {
		this.lnote = lnote;
	}

	@Override
	public String getSense() {
		return sense;
	}

	@Override
	public void setSense(String sense) {
		this.sense = sense;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Literal [sense=" + sense + ", text=" + text + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sense == null) ? 0 : sense.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Literal other = (Literal) obj;
		if (sense == null) {
			if (other.sense != null)
				return false;
		} else if (!sense.equals(other.sense))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

}
