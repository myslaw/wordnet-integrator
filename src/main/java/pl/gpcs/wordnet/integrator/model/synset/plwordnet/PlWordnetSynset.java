package pl.gpcs.wordnet.integrator.model.synset.plwordnet;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import pl.gpcs.wordnet.integrator.model.common.DomainAware;
import pl.gpcs.wordnet.integrator.model.common.Synset;

@XmlRootElement(name = "SYNSET")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlWordnetSynset implements Synset<Ilr, Synonym>, DomainAware {
	@XmlElement(name = "ID")
	private String id;
	@XmlElement(name = "POS")
	private String pos;
	@XmlElement(name = "SYNONYM")
	private Synonym synonym;
	@XmlElement(name = "ILR")
	private List<Ilr> ilr;
	@XmlElement(name = "DEF")
	private String def;
	@XmlElement(name = "USAGE")
	private String usage;
	@XmlElement(name = "DOMAIN")
	private String domain;

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getPos() {
		return pos;
	}

	@Override
	public void setPos(String pos) {
		this.pos = pos;
	}

	@Override
	public Synonym getSynonym() {
		return synonym;
	}

	@Override
	public void setSynonym(Synonym synonym) {
		this.synonym = synonym;
	}

	@Override
	public List<Ilr> getIlr() {
		return ilr;
	}

	@Override
	public void setIlr(List<Ilr> ilr) {
		this.ilr = ilr;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	@Override
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Override
	public String toString() {
		return "PlWordnetSynset [id=" + id + ", pos=" + pos + ", synonym=" + synonym + ", ilr=" + ilr + ", def=" + def
				+ ", usage=" + usage + ", domain=" + domain + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlWordnetSynset other = (PlWordnetSynset) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
