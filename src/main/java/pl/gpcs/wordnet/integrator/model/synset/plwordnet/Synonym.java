package pl.gpcs.wordnet.integrator.model.synset.plwordnet;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import pl.gpcs.wordnet.integrator.model.common.SynsetSynonym;

@XmlRootElement(name = "SYNONYM")
@XmlAccessorType(XmlAccessType.FIELD)
public class Synonym implements SynsetSynonym<Literal> {
	@XmlElement(name = "LITERAL")
	private List<Literal> literal;

	@Override
	public List<Literal> getLiteral() {
		return literal;
	}

	@Override
	public void setLiteral(List<Literal> literal) {
		this.literal = literal;
	}

	@Override
	public String toString() {
		return "Synonym [literal=" + literal + "]";
	}

}
