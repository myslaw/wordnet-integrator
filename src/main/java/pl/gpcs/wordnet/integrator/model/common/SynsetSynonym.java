package pl.gpcs.wordnet.integrator.model.common;

import java.util.List;

public interface SynsetSynonym<SynonymLiteralT extends SynonymLiteral<?>> {

	List<SynonymLiteralT> getLiteral();

	void setLiteral(List<SynonymLiteralT> literal);
}
