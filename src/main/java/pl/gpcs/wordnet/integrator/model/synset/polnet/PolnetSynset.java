package pl.gpcs.wordnet.integrator.model.synset.polnet;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import pl.gpcs.wordnet.integrator.model.common.Synset;

@XmlRootElement(name = "SYNSET")
@XmlAccessorType(XmlAccessType.FIELD)
public class PolnetSynset implements Synset<Ilr, Synonym> {
	@XmlElement(name = "STAMP")
	private String stamp;
	@XmlElement(name = "ILR")
	private List<Ilr> ilr;
	@XmlElement(name = "DEF")
	private String def;
	@XmlElement(name = "SYNONYM")
	private Synonym synonym;
	@XmlElement(name = "ID")
	private String id;
	@XmlElement(name = "BCS")
	private String bcs;
	@XmlElement(name = "CREATED")
	private String created;
	@XmlElement(name = "POS")
	private String pos;
	@XmlElement(name = "GSYN")
	private Boolean gsyn;
	@XmlElement(name = "NL")
	private Boolean nl;
	@XmlElement(name = "USAGE")
	private List<String> usage;
	@XmlElement(name = "SNOTE")
	private List<String> snote;

	public String getStamp() {
		return stamp;
	}

	public void setStamp(String stamp) {
		this.stamp = stamp;
	}

	@Override
	public List<Ilr> getIlr() {
		return ilr;
	}

	@Override
	public void setIlr(List<Ilr> ilr) {
		this.ilr = ilr;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}

	@Override
	public Synonym getSynonym() {
		return synonym;
	}

	@Override
	public void setSynonym(Synonym synonym) {
		this.synonym = synonym;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getBcs() {
		return bcs;
	}

	public void setBcs(String bcs) {
		this.bcs = bcs;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	@Override
	public String getPos() {
		return pos;
	}

	@Override
	public void setPos(String pos) {
		this.pos = pos;
	}

	public Boolean getGsyn() {
		return gsyn;
	}

	public void setGsyn(Boolean gsyn) {
		this.gsyn = gsyn;
	}

	public Boolean getNl() {
		return nl;
	}

	public void setNl(Boolean nl) {
		this.nl = nl;
	}

	public List<String> getUsage() {
		return usage;
	}

	public void setUsage(List<String> usage) {
		this.usage = usage;
	}

	public List<String> getSnote() {
		return snote;
	}

	public void setSnote(List<String> snote) {
		this.snote = snote;
	}

	@Override
	public String toString() {
		return "PolnetSynset [stamp=" + stamp + ", irl=" + ilr + ", def=" + def + ", synonym=" + synonym + ", id=" + id
				+ ", bcs=" + bcs + ", created=" + created + ", pos=" + pos + ", gsyn=" + gsyn + ", nl=" + nl
				+ ", usage=" + usage + ", snote=" + snote + "]";
	}

}
