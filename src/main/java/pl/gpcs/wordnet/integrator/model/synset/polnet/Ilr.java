package pl.gpcs.wordnet.integrator.model.synset.polnet;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlValue;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;

@XmlRootElement(name = "ILR")
@XmlAccessorType(XmlAccessType.FIELD)
public class Ilr implements SynsetIlr {

	@XmlAttribute(name = "type")
	private String type;
	@XmlValue
	private String text;

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Ilr [type=" + type + ", text=" + text + "]";
	}

}
