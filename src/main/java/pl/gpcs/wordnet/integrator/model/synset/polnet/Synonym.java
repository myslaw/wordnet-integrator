package pl.gpcs.wordnet.integrator.model.synset.polnet;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import pl.gpcs.wordnet.integrator.model.common.SynsetSynonym;

@XmlRootElement(name = "SYNONYM")
@XmlAccessorType(XmlAccessType.FIELD)
public class Synonym implements SynsetSynonym<Literal> {
	@XmlElement(name = "WORD")
	private List<String> word;
	@XmlElement(name = "LITERAL")
	private List<Literal> literal;

	public List<String> getWord() {
		return word;
	}

	public void setWord(List<String> word) {
		this.word = word;
	}

	@Override
	public List<Literal> getLiteral() {
		return literal;
	}

	@Override
	public void setLiteral(List<Literal> literal) {
		this.literal = literal;
	}

	@Override
	public String toString() {
		return "Synonym [word=" + word + ", literal=" + literal + "]";
	}

}
