package pl.gpcs.wordnet.integrator.model.synset.normalized;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.gpcs.wordnet.integrator.adapter.plwordnet.IrlAdapter;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;

@XmlJavaTypeAdapter(IrlAdapter.class)
@XmlRootElement(name = "ILR")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Ilr implements SynsetIlr {
	private String text;
	private String type;
}
