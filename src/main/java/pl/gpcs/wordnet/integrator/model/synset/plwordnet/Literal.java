package pl.gpcs.wordnet.integrator.model.synset.plwordnet;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import pl.gpcs.wordnet.integrator.adapter.plwordnet.LiteralAdapter;
import pl.gpcs.wordnet.integrator.model.common.SynonymLiteral;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlJavaTypeAdapter(LiteralAdapter.class)
public class Literal implements SynonymLiteral<Integer> {

	private String text;
	@XmlElement(name = "SENSE")
	private Integer sense;

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public Integer getSense() {
		return sense;
	}

	@Override
	public void setSense(Integer sense) {
		this.sense = sense;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sense == null) ? 0 : sense.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Literal other = (Literal) obj;
		if (sense == null) {
			if (other.sense != null)
				return false;
		} else if (!sense.equals(other.sense))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Literal [text=" + text + ", sense=" + sense + "]";
	}

}
