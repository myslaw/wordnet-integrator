package pl.gpcs.wordnet.integrator.model.synset.normalized;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.gpcs.wordnet.integrator.adapter.plwordnet.LiteralAdapter;
import pl.gpcs.wordnet.integrator.model.common.SynonymLiteral;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlJavaTypeAdapter(LiteralAdapter.class)
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Literal implements SynonymLiteral<String> {

	private String text;
	@XmlElement(name = "SENSE")
	private String sense;

}
