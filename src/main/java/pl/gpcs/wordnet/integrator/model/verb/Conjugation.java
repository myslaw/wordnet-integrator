package pl.gpcs.wordnet.integrator.model.verb;

import java.util.Set;

public interface Conjugation {

	String get();

	void set(String verb);

	Set<String> getConjugations();

	void setConjugations(Set<String> conjugations);

}
