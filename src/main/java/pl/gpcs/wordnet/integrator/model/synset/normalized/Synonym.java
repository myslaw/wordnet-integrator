package pl.gpcs.wordnet.integrator.model.synset.normalized;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.gpcs.wordnet.integrator.model.common.SynsetSynonym;

@XmlRootElement(name = "SYNONYM")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@ToString
public class Synonym implements SynsetSynonym<Literal> {
	@XmlElement(name = "LITERAL")
	private List<Literal> literal;
}
