package pl.gpcs.wordnet.integrator.model.synset.plwordnet;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import pl.gpcs.wordnet.integrator.adapter.plwordnet.IrlAdapter;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;

@XmlJavaTypeAdapter(IrlAdapter.class)
@XmlRootElement(name = "ILR")
@XmlAccessorType(XmlAccessType.FIELD)
public class Ilr implements SynsetIlr {
	private String text;
	private String type;

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Irl [text=" + text + ", type=" + type + "]";
	}

}
