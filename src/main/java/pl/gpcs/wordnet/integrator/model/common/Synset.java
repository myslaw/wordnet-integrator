package pl.gpcs.wordnet.integrator.model.common;

import java.util.List;

public interface Synset<IlrT extends SynsetIlr, SynonymT extends SynsetSynonym<? extends SynonymLiteral<?>>> {
	String getId();

	void setId(String id);

	String getPos();

	void setPos(String n);

	List<IlrT> getIlr();

	void setIlr(List<IlrT> ilr);

	SynonymT getSynonym();

	void setSynonym(SynonymT synonym);

}
