package pl.gpcs.wordnet.integrator.model.common;

public interface DomainAware {

	public String getDomain();
}
