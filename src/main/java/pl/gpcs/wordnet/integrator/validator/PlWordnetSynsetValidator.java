package pl.gpcs.wordnet.integrator.validator;

import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;

public class PlWordnetSynsetValidator implements SynsetValidator<PlWordnetSynset> {

	@Override
	public SynsetValidationResult validate(PlWordnetSynset synset) {
		if (synset == null) {
			return SynsetValidationResult.NULL_REFERENCE;
		}
		if (synset.getId() == null || synset.getId().isBlank()) {
			return SynsetValidationResult.INVALID_ID;
		}
		if (synset.getPos() == null || synset.getPos().isBlank()) {
			return SynsetValidationResult.INVALID_POS;
		}
		if (synset.getSynonym() == null || synset.getSynonym().getLiteral() == null
				|| synset.getSynonym().getLiteral().isEmpty()) {
			return SynsetValidationResult.INVALID_SYNONYMS;
		}
		return SynsetValidationResult.VALID;
	}

}
