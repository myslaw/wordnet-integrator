package pl.gpcs.wordnet.integrator.validator;

import pl.gpcs.wordnet.integrator.model.common.Synset;

public interface SynsetValidator<T extends Synset<?, ?>> {
	SynsetValidationResult validate(T synset);
}
