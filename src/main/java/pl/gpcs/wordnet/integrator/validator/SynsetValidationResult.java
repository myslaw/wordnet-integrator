package pl.gpcs.wordnet.integrator.validator;

public enum SynsetValidationResult {
	VALID, NULL_REFERENCE, INVALID_ID, INVALID_POS, INVALID_SYNONYMS, INVALID_ILR_TEXT, INVALID_ILR_TYPE,
	INVALID_LITERAL;
}
