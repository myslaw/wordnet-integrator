package pl.gpcs.wordnet.integrator.validator;

import java.util.List;

import pl.gpcs.wordnet.integrator.model.synset.polnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Literal;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Synonym;

public class PolnetSynsetValidator implements SynsetValidator<PolnetSynset> {

	@Override
	public SynsetValidationResult validate(PolnetSynset synset) {
		if (synset == null) {
			return SynsetValidationResult.NULL_REFERENCE;
		}
		if (synset.getId() == null || synset.getId().isBlank()) {
			return SynsetValidationResult.INVALID_ID;
		}
		if (synset.getPos() == null || synset.getPos().isBlank()) {
			return SynsetValidationResult.INVALID_POS;
		}
		return SynsetValidationResult.VALID;
	}

	private SynsetValidationResult validateSynonym(Synonym synonym) {
		if (synonym == null) {
			return SynsetValidationResult.INVALID_SYNONYMS;
		}
		List<Literal> literals = synonym.getLiteral();
		if (literals == null || literals.isEmpty()) {
			return SynsetValidationResult.INVALID_SYNONYMS;
		}
		for (Literal literal : literals) {
			if (literal == null || !isValidString(literal.getText()) || !isValidString(literal.getSense())) {
				return SynsetValidationResult.INVALID_LITERAL;
			}
		}
		return SynsetValidationResult.VALID;
	}

	private SynsetValidationResult validateIlr(List<Ilr> ilrs) {
		if (ilrs == null || ilrs.isEmpty()) {
			return SynsetValidationResult.VALID;
		}
		for (Ilr ilr : ilrs) {
			if (!isValidString(ilr.getText())) {
				return SynsetValidationResult.INVALID_ILR_TEXT;
			}
			if (!isValidString(ilr.getType())) {
				return SynsetValidationResult.INVALID_ILR_TYPE;
			}
		}
		return SynsetValidationResult.VALID;
	}

	private boolean isValidString(String string) {
		return string != null && !string.isEmpty();
	}

}
