package pl.gpcs.wordnet.integrator.bench;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.filter.PlWordnetOmmitEnglishFilter;
import pl.gpcs.wordnet.integrator.model.common.SynonymLiteral;
import pl.gpcs.wordnet.integrator.model.common.Synset;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;
import pl.gpcs.wordnet.integrator.model.common.SynsetSynonym;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.model.util.ReducedSynset;
import pl.gpcs.wordnet.integrator.util.WordnetType;

public class SynsetBenchmark {

	final List<PlWordnetSynset> plWordnetSynsetList;
	final List<PolnetSynset> polnetSynsetList;

	public SynsetBenchmark(List<PlWordnetSynset> plWordnetSynsetList, List<PolnetSynset> polnetSynsetList) {
		this.plWordnetSynsetList = plWordnetSynsetList;
		this.polnetSynsetList = polnetSynsetList;
	}

	// TODO zrob bench czy pojawia si� synonim z tym samym tekstem i synonim w
	// roznych entry

//	private void initSynsetCollections() throws SynsetReadException, IOException, InitializationException {
//		plWordnetSynsetList = SynsetUtils.readAllSynsets(PlWordnetSynset.class, Path.of(Constants.PlWordnet.PATH_4_2));
//		polnetSynsetList = SynsetUtils.readAllSynsets(PolnetSynset.class, Path.of(Constants.Polnet.PATH));
//	}

	public void run() {
//		benchPosCount();
//		benchIlrTypeCount();
//		printMatchingTypes();
//		printRepeatedSenses();
		printDomainSizes();
//		printEntriesWithRepeatedSynonymsTexts();
//		printUniqueSynonymsCounts();
//		printSynonymsSharedAmongEntires();
//		System.out.println("Reduced:");
//		printRepeatedSenses(createStrippedEnglishSynsets(plWordnetSynsetList));
//		var stripped = createStrippedEnglishSynsets(plWordnetSynsetList);
		// printCollidingIds();
	}

	private void printDomainSizes() {
		System.out.println("Domains sizes for PlWordnet: ");
		findDomainSizes(plWordnetSynsetList).entrySet().stream()
				.sorted((o1, o2) -> o1.getValue().compareTo(o2.getValue()))
				.forEach(t -> System.out.println("\t" + t.getKey() + " : " + t.getValue()));
		System.out.println("Domains sizes for Polnet: ");
		System.out.println("\tPolnet has no domain definitions");
//		for (Synset<?, ? extends SynsetSynonym<?>> synset : analyzedSynsetCollection) {
//			
//		}
	}

	private List<? extends Synset<? extends SynsetIlr, ? extends SynsetSynonym<? extends SynonymLiteral<?>>>> resolveWordnetType(
			WordnetType wordnetType) {
		switch (wordnetType) {
		case PLWORDNET:
			return plWordnetSynsetList;
		case POLNET:
			return polnetSynsetList;
		default:
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Creates a map, where the key is the number of synonyms, and the value is the
	 * number of entries, that contains this many synonyms.
	 * 
	 * @param synsetList
	 * @return
	 */
	public Map<Integer, Integer> calcSynonymCountToOccurances(
			List<? extends Synset<?, ? extends SynsetSynonym<? extends SynonymLiteral<?>>>> synsetList) {
		Map<Integer, Integer> synonymCountToOccurances = new HashMap<>();
		for (Synset<?, ? extends SynsetSynonym<? extends SynonymLiteral<?>>> synset : synsetList) {
			synonymCountToOccurances.compute(synset.getSynonym().getLiteral().size(), (k, v) -> v == null ? 1 : v + 1);
		}
		return synonymCountToOccurances;
	}

	public Map<String, Integer> findDomainSizes(WordnetType wordnetType) {
		return findDomainSizes((List<PlWordnetSynset>) resolveWordnetType(wordnetType));
	}

	private Map<String, Integer> findDomainSizes(Collection<PlWordnetSynset> analyzedSynsetColleciton) {
		Map<String, Integer> domainToCount = new HashMap<>();
		for (PlWordnetSynset plWordnetSynset : analyzedSynsetColleciton) {
			domainToCount.compute(plWordnetSynset.getDomain(), (t, u) -> {
				if (u == null) {
					return 1;
				}
				return u + 1;
			});
		}
		return domainToCount;
	}

	public Set<PlWordnetSynset> findAllByDomain(WordnetType wordnetType, String domain) {
		if (wordnetType != WordnetType.PLWORDNET) {
			throw new UnsupportedOperationException();
		}
		return plWordnetSynsetList.stream().filter(pws -> Objects.equals(pws.getDomain(), domain))
				.collect(Collectors.toSet());
	}

	/**
	 * Oblicza rozmiar unikalnych synonim�w (para text i sense jest niepowtarzalna)
	 */
	private void printUniqueSynonymsCounts() {
		System.out.println("Number of unique synonyms in PlWordnet: " + calcUniqueSynonyms(plWordnetSynsetList));
		System.out.println("Number of unique synonyms in Polnet: " + calcUniqueSynonyms(polnetSynsetList));
	}

	private <T> long calcUniqueSynonyms(
			Collection<? extends Synset<?, ? extends SynsetSynonym<? extends SynonymLiteral<T>>>> analyzedSynsetCollection) {
		return analyzedSynsetCollection.stream().flatMap(synset -> synset.getSynonym().getLiteral().stream()).distinct()
				.count();
	}

	/**
	 * Wypisz synonimy, ktore maja taki sam tekst i sense w dw�ch (lub wi�cej) w
	 * r�nych WordnetEntry
	 */
	private void printSynonymsSharedAmongEntires() {
		System.out.println("Shared synonyms for PlWordnet: ");
		printSharedSynonymsForSynsetCollection(plWordnetSynsetList);
		System.out.println("Shared synonyms for Polnet: ");
		printSharedSynonymsForSynsetCollection(polnetSynsetList);
	}

	private void printSharedSynonymsForSynsetCollection(
			Collection<? extends Synset<?, ? extends SynsetSynonym<?>>> analyzedSynsetCollection) {
		var repeatedWrapper = new Object() {
			boolean isRepeated = false;
		};
		Map<SynonymLiteral<?>, Integer> synonymLiteralToCount = new HashMap<>();
		for (Synset<?, ? extends SynsetSynonym<?>> synset : analyzedSynsetCollection) {
			synset.getSynonym().getLiteral();
			for (SynonymLiteral<?> literal : synset.getSynonym().getLiteral()) {
				synonymLiteralToCount.compute(literal, (key, value) -> {
					if (value == null) {
						return 1;
					}
					repeatedWrapper.isRepeated = true;
					return value + 1;
				});
			}
		}
		if (repeatedWrapper.isRepeated) {
			List<Entry<SynonymLiteral<?>, Integer>> sharedSynonyms = synonymLiteralToCount.entrySet().stream()
					.filter(e -> e.getValue() > 1).collect(Collectors.toList());
			System.out.println("Shared synonyms:");
			for (Map.Entry<SynonymLiteral<?>, Integer> entry : sharedSynonyms) {
				if (entry.getValue() > 1) {
					System.out.println("\tshared synonym: " + entry.getKey());
				}
			}
			System.out.println("Total shared count: " + sharedSynonyms.size());
		}
	}

	private void printEntriesWithRepeatedSynonymsTexts() {
		System.out.println("Doubled synonyms texts for PlWordnet:");
		printEntriesWithRepeatedSynonymsTexts(plWordnetSynsetList);
		System.out.println("Doubled synonyms texts for Polnet:");
		printEntriesWithRepeatedSynonymsTexts(polnetSynsetList);
	}

	private void printEntriesWithRepeatedSynonymsTexts(
			Collection<? extends Synset<?, ? extends SynsetSynonym<? extends SynonymLiteral<?>>>> analyzedSynsetCollection) {
		for (Synset<?, ? extends SynsetSynonym<? extends SynonymLiteral<?>>> synset : analyzedSynsetCollection) {
			Map<String, Integer> textToCount = new HashMap<>();
			var repeatedWrapper = new Object() {
				boolean isRepeated = false;
			};
			for (SynonymLiteral<?> literal : synset.getSynonym().getLiteral()) {
				textToCount.compute(literal.getText(), (key, value) -> {
					if (value == null) {
						return 1;
					}
					repeatedWrapper.isRepeated = true;
					return value + 1;
				});
			}
			if (repeatedWrapper.isRepeated) {
				System.out.println("Found repeated synonym text for ID: " + synset.getId());
				for (Entry<String, Integer> entry : textToCount.entrySet()) {
					if (entry.getValue() > 1) {
						System.out.println("\trepeated " + entry.getValue() + " times for text: " + entry.getKey());
					}
				}
			}
		}
	}

	private void printRepeatedSenses() {
		System.out.println("Duplicated senses for PlWordnet:");
		printRepeatedSenses(plWordnetSynsetList);
		System.out.println("Duplicated senses for Polnet: ");
		printRepeatedSenses(polnetSynsetList);
	}

	private <T extends Synset<? extends SynsetIlr, ?>> void printRepeatedSenses(List<T> synsetList) {
		Map<String, List<T>> posToSynsets = synsetList.stream().collect(Collectors.groupingBy(T::getPos));
		for (Entry<String, List<T>> entry : posToSynsets.entrySet()) {
			System.out.println("\tpos " + entry.getKey() + ":");
			List<T> posSynsetList = entry.getValue();
			Map<SynonymLiteral<?>, Integer> synsetSensesCount = countSenses(posSynsetList);
			List<Entry<SynonymLiteral<?>, Integer>> foundDoubles = synsetSensesCount.entrySet().stream()
					.filter(sEntry -> sEntry.getValue() > 1).collect(Collectors.toList());
			System.out.println("\t\treapeated senses: " + foundDoubles.size() + "/" + synsetSensesCount.size() + " ("
					+ calcPerc(foundDoubles.size(), synsetSensesCount.size()) + "%)");
		}
//		synsetList.stream().collect(Collectors.groupingBy(PolnetSynset::getPos));
	}

	private List<PlWordnetSynset> createStrippedEnglishSynsets(List<PlWordnetSynset> synsetList) {
		PlWordnetOmmitEnglishFilter englishFilter = new PlWordnetOmmitEnglishFilter();
		List<PlWordnetSynset> filteredSynset = new LinkedList<>(synsetList);
		for (Iterator<PlWordnetSynset> iterator = filteredSynset.iterator(); iterator.hasNext();) {
			PlWordnetSynset plWordnetSynset = iterator.next();
			try {
				if (englishFilter.filter(plWordnetSynset) == false) {
					iterator.remove();
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
//		filteredSynset.removeIf(synset -> englishFilter.filter(synset));
		return filteredSynset;
	}

	private double calcPerc(int partSize, int totalSize) {
		return (partSize * 100f) / totalSize;
	}

	private Map<SynonymLiteral<?>, Integer> countSenses(
			List<? extends Synset<? extends SynsetIlr, ? extends SynsetSynonym<? extends SynonymLiteral<?>>>> synsetList) {
		Map<SynonymLiteral<?>, Integer> synonymToCount = new HashMap<>();
		for (Synset<? extends SynsetIlr, ? extends SynsetSynonym<? extends SynonymLiteral<?>>> synset : synsetList) {
			List<? extends SynonymLiteral<?>> literals = synset.getSynonym().getLiteral();
			for (SynonymLiteral<?> literal : literals) {
				synonymToCount.compute(literal, this::incrementKeyOccurance);
			}
		}
		return synonymToCount;
	}

	private Map<SynsetIlr, Integer> countIlrs(List<? extends Synset<?, ?>> synsetList) {
		Map<SynsetIlr, Integer> ilrToCount = new HashMap<>();
		for (Synset<?, ?> synset : synsetList) {
			if (synset.getIlr() == null) {
				continue;
			}
			for (SynsetIlr synsetIlr : synset.getIlr()) {
				ilrToCount.compute(synsetIlr, this::incrementKeyOccurance);
			}
		}
		return ilrToCount;
	}

	/**
	 * Used for counting object occurances in map. Put instead of writing several
	 * same lambdas
	 * 
	 * @param key
	 * @param val
	 * @return
	 */
	private final Integer incrementKeyOccurance(Object key, Integer val) {
		if (val == null) {
//			System.err.println("return 1");
			return 1;
		}
//		System.err.println("return val + 1");
		return val + 1;
	}

	private void printCollidingIds() {
		System.out.println("Common IDs:");
		Set<String> plWordnetIdSet = plWordnetSynsetList.stream().map(Synset::getId).collect(Collectors.toSet());
		Set<String> polnetIdSet = polnetSynsetList.stream().map(Synset::getId).collect(Collectors.toSet());
		for (String plWordnetId : plWordnetIdSet) {
			if (polnetIdSet.contains(plWordnetId)) {
				System.out.println("\t" + plWordnetId);
			}
		}
	}

	private void printMatchingTypes() {
		Set<String> uniquePolnetIlrTypeSet = uniquePolnetIlrType(polnetSynsetList).stream().map(s -> s.toLowerCase())
				.collect(Collectors.toSet());
		System.out.println("Common ILR types:");
		for (String plWordnetIlrType : uniquePlWordnetIlrType(plWordnetSynsetList).stream().map(s -> s.toLowerCase())
				.collect(Collectors.toSet())) {
			if (uniquePolnetIlrTypeSet.contains(plWordnetIlrType)) {
				System.out.println("\t" + plWordnetIlrType);
			}
		}
	}

	public Map<String, Integer> countPos(Collection<? extends Synset> synsetCollection) {
		HashMap<String, Integer> posNameToPosCount = new HashMap<>();
		for (String posName : uniquePos(synsetCollection)) {
			posNameToPosCount.put(posName, 0);
		}
		for (Synset synset : synsetCollection) {
			posNameToPosCount.compute(synset.getPos(), (key, val) -> val + 1);
		}
		return posNameToPosCount;

	}

	public Set<String> uniquePos(Collection<? extends Synset> synsetCollection) {
		HashSet<String> uniquePosNames = new HashSet<>();
		for (Synset synset : synsetCollection) {
			uniquePosNames.add(synset.getPos());
		}
		return uniquePosNames;
	}

	public void printPosCount(Collection<? extends Synset> synsetCollection) {
		Map<String, Integer> posNameToPosCount = countPos(synsetCollection);
		for (String posName : posNameToPosCount.keySet()) {
			System.out.println(posName + ":" + posNameToPosCount.get(posName));
		}
	}

	public void benchPosCount() {
		System.out.println("POS in " + PlWordnetSynset.class.getSimpleName());
		printPosCount(plWordnetSynsetList);
		System.out.println("\nPOS in " + PolnetSynset.class.getSimpleName());
		printPosCount(polnetSynsetList);
	}

	private Map<String, Integer> countPlWordnetIlrType(Collection<PlWordnetSynset> synsetCollection) {
		HashMap<String, Integer> ilrTypeToIlrTypeCount = new HashMap<>();
		for (PlWordnetSynset synset : synsetCollection) {
			if (synset.getIlr() != null) {
				for (Ilr ilr : synset.getIlr()) {
					ilrTypeToIlrTypeCount.compute(ilr.getType(), this::incrementKeyOccurance);
				}
			}
		}
		return ilrTypeToIlrTypeCount;
	}

	private Set<String> uniquePlWordnetIlrType(Collection<PlWordnetSynset> synsetCollection) {
		HashSet<String> uniqueIlrTypeNames = new HashSet<>();
		for (PlWordnetSynset synset : synsetCollection) {
			if (synset.getIlr() != null) {
				for (Ilr ilr : synset.getIlr()) {
					if (ilr.getType().equals("similar_to")) {
						System.err.println("SIM!");
					}
					uniqueIlrTypeNames.add(ilr.getType());
				}
			}
		}
		return uniqueIlrTypeNames;
	}

	private void printPlWordnetIlrTypeCount(Collection<PlWordnetSynset> synsetCollection) {
		Map<String, Integer> ilrTypeNameToIlrTypeCount = countPlWordnetIlrType(synsetCollection);
		List<Entry<String, Integer>> list = new ArrayList<>(ilrTypeNameToIlrTypeCount.entrySet());
		list.sort(Entry.comparingByValue());
		for (Entry<String, Integer> entry : list) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
	}

	private void printPolnetIlrTypeCount(List<PolnetSynset> polnetSynset2) {
		Map<String, Integer> ilrTypeNameToIlrTypeCount = countPolnetIlrType(polnetSynset2);
//		ilrTypeNameToIlrTypeCount.stre
//		List<String> list = new LinkedList<>(ilrTypeNameToIlrTypeCount.keySet());
		List<Entry<String, Integer>> list = new ArrayList<>(ilrTypeNameToIlrTypeCount.entrySet());
		list.sort(Entry.comparingByValue());
		for (Entry<String, Integer> entry : list) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
	}

	private Map<String, Integer> countPolnetIlrType(List<PolnetSynset> polnetSynset2) {
		HashMap<String, Integer> ilrTypeToIlrTypeCount = new HashMap<>();
		for (PolnetSynset synset : polnetSynset2) {
			if (synset.getIlr() != null) {
				for (pl.gpcs.wordnet.integrator.model.synset.polnet.Ilr ilr : synset.getIlr()) {
					ilrTypeToIlrTypeCount.compute(ilr.getType(), this::incrementKeyOccurance);
				}
			}
		}
		return ilrTypeToIlrTypeCount;
	}

	private Set<String> uniquePolnetIlrType(List<PolnetSynset> polnetSynset2) {
		HashSet<String> uniqueIlrTypeNames = new HashSet<>();
		for (PolnetSynset synset : polnetSynset2) {
			if (synset.getIlr() != null) {
				for (pl.gpcs.wordnet.integrator.model.synset.polnet.Ilr ilr : synset.getIlr()) {
					if (ilr.getType() != null) { // w Wordnecie pojawiaja sie <ILR/>
						uniqueIlrTypeNames.add(ilr.getType());
					}
				}
			}
		}
		return uniqueIlrTypeNames;
	}

	private void benchIlrTypeCount() {
		System.out.println("ILR type " + PlWordnetSynset.class.getSimpleName());
		printPlWordnetIlrTypeCount(plWordnetSynsetList);
		System.out.println("\nILR type " + PolnetSynset.class.getSimpleName());
		printPolnetIlrTypeCount(polnetSynsetList);
	}

	public Collection<ReducedSynset> findSNotes(Collection<PolnetSynset> polnetSynsets) {
		return polnetSynsets.stream().filter(synset -> synset.getSnote() != null && !synset.getSnote().isEmpty())
				.map(ReducedSynset::new).collect(Collectors.toList());
	}

}
