package pl.gpcs.wordnet.integrator.bench;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import pl.gpcs.wordnet.integrator.crawler.WordnetCrawler;
import pl.gpcs.wordnet.integrator.io.ConjugationReader;
import pl.gpcs.wordnet.integrator.joiner.impl.WordnetJoinerImpl;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.service.ConjugationEnrichmentService;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.service.impl.ConjugationEnrichmentServiceImpl;
import pl.gpcs.wordnet.integrator.util.GeneralRelation;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;
import pl.gpcs.wordnet.integrator.util.RelationPair;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.SynsetUtils;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.helper.WordnetRelativesFinder;
import pl.gpcs.wordnet.integrator.util.tuples.Pair;

@Slf4j
public class WordnetBenchmark {

	private static final WordnetCrawler WORDNET_CRAWLER = new WordnetCrawler();
	final Wordnet wordnetPlWordnet;
	final Wordnet wordnetPolnet;
	private List<PlWordnetSynset> plwordnetSynsetList;

	public WordnetBenchmark(Wordnet wordnetPlWordnet, Wordnet wordnetPolnet,
			List<PlWordnetSynset> plWordnetSynsetsList) {
		this.wordnetPlWordnet = wordnetPlWordnet;
		this.wordnetPolnet = wordnetPolnet;
		this.plwordnetSynsetList = plWordnetSynsetsList;
	}

	public Set<WordnetEntry> findRootHypernyms(WordnetEntry startingEntry) {
		Queue<WordnetEntry> queuedEntries = new LinkedList<>();
		queuedEntries.add(startingEntry);
		Set<WordnetEntry> tabooEntries = new HashSet<>();
		tabooEntries.add(startingEntry);
		Set<WordnetEntry> rootHypernyms = new HashSet<>();

		WordnetEntry wordnetEntry;
		while ((wordnetEntry = queuedEntries.poll()) != null) {
			Set<WordnetEntry> hypernyms = wordnetEntry.getHypernyms();
			if (hypernyms == null || hypernyms.isEmpty()) {
				rootHypernyms.add(wordnetEntry);
				continue;
			}
			for (WordnetEntry hypernym : hypernyms) {
				if (tabooEntries.add(hypernym)) {
					queuedEntries.add(hypernym);
				}
			}
		}
		return rootHypernyms;
	}

	public int countUniqueSynonyms(Wordnet wordnet) {
		Set<SynonymEntry> synonymEntrySet = new HashSet<>(wordnet.size());
		for (WordnetEntry wordnetEntry : wordnet) {
			synonymEntrySet.addAll(wordnetEntry.getSynonyms());
		}
		return synonymEntrySet.size();
	}

	public double calcAvgSynonymCountPerWordnetEntry(Wordnet wordnet) {
		return calcAvgSynonymToCountPerWordnetEntry(
				wordnet.stream().filter(WordnetEntry::isInitialized).toList());
	}

	public double calcAvgSynonymCountPerWordnetEntryOnEdge(Wordnet wordnet, MergingStrategy strategy) {
		Set<WordnetEntry> edgeWordnetEntries;
		if (strategy == MergingStrategy.HYPERNYM_ORIGIN) {
			edgeWordnetEntries = WORDNET_CRAWLER.findEdgeHypernyms(wordnet);
		} else {
			edgeWordnetEntries = WORDNET_CRAWLER.findEdgeHyponyms(wordnet);
		}
		return calcAvgSynonymToCountPerWordnetEntry(edgeWordnetEntries);
	}

	private double calcAvgSynonymToCountPerWordnetEntry(Collection<WordnetEntry> wordnetEntries) {
		final int synonymCount = wordnetEntries.stream().mapToInt(entry -> entry.getSynonyms().size()).sum();
		return (double) synonymCount / wordnetEntries.size();
	}


	public Set<String> findEdgeHypernymsToWholeCommonSynonymsTexts(Wordnet edgeOnly, Wordnet whole) {
		Set<String> edgeSynonymsTexts = WordnetJoinerImpl
				.extractSynonymsAsStringSet(WORDNET_CRAWLER.findEdgeHypernyms(edgeOnly));
		Set<String> wholeSynonymsTexts = WordnetJoinerImpl.extractSynonymsAsStringSet(whole);
		if (wholeSynonymsTexts.size() > edgeSynonymsTexts.size()) {
			return findCommonStrings(wholeSynonymsTexts, edgeSynonymsTexts);
		} else {
			return findCommonStrings(edgeSynonymsTexts, wholeSynonymsTexts);
		}
	}

	public Set<String> findEdgeCommonSynonymsTexts(Wordnet wordnet1, Wordnet wordnet2) {
		Set<String> synonymsTexts1 = WordnetJoinerImpl
				.extractSynonymsAsStringSet(WORDNET_CRAWLER.findEdgeHypernyms(wordnet1));
		Set<String> synonymsTexts2 = WordnetJoinerImpl
				.extractSynonymsAsStringSet(WORDNET_CRAWLER.findEdgeHypernyms(wordnet2));
		if (synonymsTexts1.size() > synonymsTexts2.size()) {
			return findCommonStrings(synonymsTexts1, synonymsTexts2);
		} else {
			return findCommonStrings(synonymsTexts2, synonymsTexts1);
		}
	}

	public Set<String> findAllCommonSynonymsTexts(Wordnet wordnet1, Wordnet wordnet2) {
		Set<String> synonymsTexts1 = WordnetJoinerImpl.extractSynonymsAsStringSet(wordnet1);
		Set<String> synonymsTexts2 = WordnetJoinerImpl.extractSynonymsAsStringSet(wordnet2);
		if (synonymsTexts1.size() > synonymsTexts2.size()) {
			return findCommonStrings(synonymsTexts1, synonymsTexts2);
		} else {
			return findCommonStrings(synonymsTexts2, synonymsTexts1);
		}
	}

	private Set<String> findCommonStrings(Set<String> greaterSet, Set<String> lesserSet) {
		return lesserSet.stream().filter(greaterSet::contains).collect(Collectors.toSet());
	}

	/**
	 * Calculates how many times specific synonym count occured for entries. For
	 * example if there are two entries with 4 synonyms, the resulting map would
	 * contain mapping of 4 to 2.
	 * 
	 * @param wordnet
	 * @return
	 */
	public Map<Integer, Integer> calcSynonymCountToOccurances(Wordnet wordnet) {
		Map<Integer, Integer> synonymCountToOccurance = new HashMap<>();
		for (WordnetEntry wordnetEntry : wordnet) {
			int synonymCount = wordnetEntry.getSynonyms().size();
//			synonymCountToOccurance.compute(synonymCount, k -> {
//				return 2;
//			});
			synonymCountToOccurance.compute(synonymCount, (key, value) -> value != null ? value + 1 : 1);
		}
		return synonymCountToOccurance;
	}

	@Deprecated
	public void printSynonymEntryCount() {
		int synonymCount = 0;
		for (WordnetEntry wordnetEntry : wordnetPlWordnet) {
			if (wordnetEntry.isInitialized()) {
				synonymCount += wordnetEntry.getSynonyms().size();
			}
		}
		System.out.println("\nTotal number of synonym entries: " + synonymCount);
		System.out.println("Synonyms per wordnet entry (avg): " + synonymCount * 1.0 / wordnetPlWordnet.size());
	}

	public void printSynonymEntryCountWithConjugations() {
		System.out.println("\nSYNONYMS DATA (alters data):");
		System.out.print("\n\tbefore conjugation enriching:");
		printSynonymEntryCount();
		ConjugationEnrichmentService conjugationEnrichmentService = new ConjugationEnrichmentServiceImpl();
		try {
			conjugationEnrichmentService.enrichWordnet(wordnetPlWordnet,
					new ConjugationReader(Paths.get("src", "main", "resources", "odm.txt")));
			System.out.print("\n\tafter conjugation enriching:");
			printSynonymEntryCount();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e); // FIXME
		}
	}

	public List<Entry<String, Integer>> calcSemanticRelationStats(Wordnet wordnet) {
		List<Entry<String, Integer>> list = new ArrayList<>(getSemanticRelationsStats(wordnet).entrySet());
		list.sort(Entry.comparingByValue());
		return list;
	}

	private Map<String, Integer> getSemanticRelationsStats(Wordnet wordnet) {
		ConcurrentMap<String, Integer> cmap = new ConcurrentHashMap<>();
		wordnet.getAllEntries().parallelStream().forEach(wordnetEntry -> {
			if (wordnetEntry.getHypernyms() != null) {
				cmap.merge("hypernym", wordnetEntry.getHypernyms().size(), (oldV, newV) -> oldV + newV);
			}
			if (wordnetEntry.getHyponyms() != null) {
				cmap.merge("hyponym", wordnetEntry.getHyponyms().size(), (oldV, newV) -> oldV + newV);
			}
			if (wordnetEntry.getGeneralSemanticRelations() != null) {
				for (GeneralRelation<String> gr : wordnetEntry.getGeneralSemanticRelations()) {
					try {
						cmap.merge(gr.getRelation(), 1, (oldV, newV) -> oldV + newV);
					} catch (NullPointerException e) {
						System.err.println("Exception on GENERAL SEMANTIC RELATIONS!");
					}
				}
			}
			// TODO dodaj jeszcze referencing entries

		});
		return cmap;
	}

//	public Set<WordnetEntry> discoverRelativesAtDepth(WordnetEntry root, int depth) {
//		Set<WordnetEntry> discoveredRelatives = new HashSet<>();
////		discoverRelativesAtDepth(root, depth);
//		return discoveredRelatives;
//	}

	@Deprecated
	public Set<WordnetEntry> findDomainEntries(String domain, int relativesDepth) {
		Set<WordnetEntry> domainEntriesWithRelatives = new HashSet<>();
		Set<WordnetEntry> rootDomainEntries = findPlWordnetEntriesByDomain(domain);
		for (WordnetEntry rootDomainEntry : rootDomainEntries) {
			domainEntriesWithRelatives.addAll(discoverRelativesAtDepth(rootDomainEntry, relativesDepth));
		}
		domainEntriesWithRelatives.addAll(rootDomainEntries);
		return domainEntriesWithRelatives;
	}

	/**
	 * depth 0 - self depth 1 - direct relatives depth 2 - direct relatives of
	 * relatives
	 * 
	 * @param root
	 * @param depth
	 * @return
	 */
	public Set<WordnetEntry> discoverRelativesAtDepth(WordnetEntry root, int depth) {
		if (depth == 0) {
			return Collections.emptySet();
		}
		WordnetRelativesFinder finder = new WordnetRelativesFinder(wordnetPlWordnet);
		Set<WordnetEntry> discoveredRelatives = new HashSet<>();
		Queue<WordnetEntry> currentDepth = finder.findDirectRelatives(root).stream()
				.collect(Collectors.toCollection(LinkedList::new));
		Queue<WordnetEntry> nextDepth = new LinkedList<>();
		WordnetEntry relative;

		for (; depth > 1; --depth) {
			while ((relative = currentDepth.poll()) != null) {
				if (!discoveredRelatives.add(relative)) {
					// rekord byl juz znaleziony, zignoruj.
					continue;
				}
				finder.findDirectRelatives(relative).stream().filter(Predicate.not(discoveredRelatives::contains))
						.forEach(nextDepth::add);
			}
			if (nextDepth.isEmpty()) {
				break;
			}
			currentDepth = nextDepth;
			nextDepth = new LinkedList<>();
		}
		discoveredRelatives.addAll(currentDepth);
		return discoveredRelatives;
	}

	public Set<WordnetEntry> findBranchesOfDomain(Wordnet wordnet, final String domain) {
		WordnetRelativesFinder helper = new WordnetRelativesFinder(wordnet);
		Set<WordnetEntry> worndetEntries = new HashSet<>();
		Set<WordnetEntry> domainWordnetEntries = findPlWordnetEntriesByDomain(domain);
		int count = 1;
		for (WordnetEntry domainEntry : domainWordnetEntries) {
			System.out.println("Processing " + count + "/" + domainWordnetEntries.size());
			++count;
			worndetEntries.add(domainEntry);
			worndetEntries.addAll(helper.findAllVRelatives(domainEntry));
		}
		return worndetEntries;
	}

	private Set<WordnetEntry> findPlWordnetEntriesByDomain(final String domain) {
		return SynsetUtils.findAllPlWordnetSynsetsByDomain(domain, plwordnetSynsetList).stream()
				.map(PlWordnetSynset::getId).map(wordnetPlWordnet::findById).collect(Collectors.toSet());
	}

	public void printWordnetGroups() {
		Set<Pair<WordnetEntry, Integer>> groupsCounts = findWordnetGroupsCounts(wordnetPlWordnet);
		groupsCounts.stream().sorted((o1, o2) -> o1.getObj1().compareTo(o2.getObj1()))
				.forEach(pair -> System.out.println("Leader: " + pair.getObj0() + ", count: " + pair.getObj1()));
	}

	private Set<Pair<WordnetEntry, Integer>> findWordnetGroupsCounts(Wordnet wordnet) {
		WordnetRelativesFinder helper = new WordnetRelativesFinder(wordnet);
		Set<Pair<WordnetEntry, Integer>> groups = ConcurrentHashMap.newKeySet();
		AtomicInteger ai = new AtomicInteger(0);
		final int worndetSize = wordnet.size();
		wordnet.parallelStream().forEach(we -> {
			System.out.println(LocalDateTime.now() + " Processing [" + ai.incrementAndGet() + "/" + worndetSize + "]"
					+ we.getId());
			groups.add(Pair.of(we, helper.findAllVRelatives(we).size()));
		});

//		for (WordnetEntry wordnetEntry : wordnet) {
//			System.out.println(LocalDateTime.now() + " Processing " + wordnetEntry.getId());
//			groups.add(Pair.of(wordnetEntry, helper.findAllRelatives(wordnetEntry).size()));
//		}
		return groups;
	}

	public Pair<WordnetEntry, Integer> findSmallestRelativeCount(Wordnet wordnet) {
		WordnetRelativesFinder helper = new WordnetRelativesFinder(wordnet);
		Set<WordnetEntry> taboos = new HashSet<>();
		Pair<WordnetEntry, Integer> bestPair = Pair.of(null, Integer.MAX_VALUE);
		for (WordnetEntry wordnetEntry : wordnet) {
			if (taboos.contains(wordnetEntry)) {
				continue;
			}
			Set<WordnetEntry> relatives = helper.findAllRelatives(wordnetEntry);
			taboos.addAll(relatives);
			if (relatives.size() < bestPair.getObj1()) {
				bestPair = Pair.of(wordnetEntry, relatives.size());
				System.out.println("Best: " + wordnetEntry.getId() + " with: " + relatives.size());
			}
		}
		return bestPair;
	}

	@Deprecated
	private void printDomainStats(final String domain) {
		Set<WordnetEntry> domainEntries = findPlWordnetEntriesByDomain(domain);
		WordnetRelativesFinder helper = new WordnetRelativesFinder(wordnetPlWordnet);
		for (WordnetEntry domainEntry : domainEntries) {
			System.out.println(domainEntry.getId() + ":");
			System.out.println("\tdirectly references: " + countDirectlyReferencedEntries(domainEntry));
			System.out.println("\tis directly referenced: " + countDirectlyReferencingEntries(domainEntry));
			System.out.println("\trelative count: " + helper.findAllRelatives(domainEntry).size());
		}
		System.out.println("Domain size: " + domainEntries.size());
	}

	private int countDirectlyReferencedEntries(WordnetEntry wordnetEntry) {
		int count = 0;
		if (wordnetEntry.getHyponyms() != null) {
			count += wordnetEntry.getHyponyms().size();
		}
		if (wordnetEntry.getHypernyms() != null) {
			count += wordnetEntry.getHypernyms().size();
		}
		if (wordnetEntry.getGeneralSemanticRelations() != null) {
			count += wordnetEntry.getGeneralSemanticRelations().size();
		}
		return count;
	}

	private int countDirectlyReferencingEntries(WordnetEntry wordnetEntry) {
		int count = 0;
		Set<WordnetEntry> referencingToHypernym = wordnetPlWordnet.findReferencingEntriesToHypernym(wordnetEntry);
		if (referencingToHypernym != null) {
			count += referencingToHypernym.size();
		}
		Set<WordnetEntry> referencingToHyponym = wordnetPlWordnet.findReferencingEntriesToHyponym(wordnetEntry);
		if (referencingToHyponym != null) {
			count += referencingToHyponym.size();
		}
		Set<RelationPair<WordnetEntry, GeneralRelation<String>>> referencingToGeneral = wordnetPlWordnet
				.findReferencingEntriesInGeneralRaltion(wordnetEntry);
		if (referencingToGeneral != null) {
			count += referencingToGeneral.size();
		}
		return count;
	}

	public List<List<WordnetEntry>> findPathsToEdgeHypernym(WordnetEntry edgeHyponym) {
		return findFromEdgeHyponym(edgeHyponym, new HashSet<>());
	}

	private List<List<WordnetEntry>> findFromEdgeHyponym(WordnetEntry edgeHyponym,
			Set<WordnetEntry> ancestors) {
		// XXX mozna zmienic set na linkedHashSet i modyfikowac rekurencyjnied
		ancestors.add(edgeHyponym);
		final Set<WordnetEntry> hypernyms = edgeHyponym.getHypernyms();
		if (hypernyms == null || hypernyms.isEmpty()) {
			return List.of(List.of(edgeHyponym));
		}
		List<List<WordnetEntry>> result = new LinkedList<>();
		for (WordnetEntry hypernym : edgeHyponym.getHypernyms()) {
			if (!ancestors.add(hypernym)) {
				LOG.warn("Cycle detected, ommiting redundant path generation for entry with id: {}", hypernym.getId());
				continue;
			}
			LOG.debug("Processing {}...", hypernym.getId());
			for (List<WordnetEntry> path : findFromEdgeHyponym(hypernym, new HashSet<>(ancestors))) {
				List<WordnetEntry> tempPath = new ArrayList<>(path.size() + 1);
				tempPath.add(edgeHyponym);
				tempPath.addAll(path);
				result.add(tempPath);
			}
		}
		return result;
	}

	public Set<WordnetEntry> findEdgeWordnetEntries(Wordnet worndet, MergingStrategy strategy) {
		switch (strategy) {
		case HYPERNYM_ORIGIN:
			return WORDNET_CRAWLER.findEdgeHypernyms(worndet);
		case HYPONYM_ORIGIN:
			return WORDNET_CRAWLER.findEdgeHyponyms(worndet);
		default:
			throw new IllegalArgumentException("No implementation for strategy: " + strategy);
		}
	}
	
	public Collection<WordnetEntry> findAllSynsets(Wordnet wordnet) {
		return wordnet.getAllEntries();
	}

	public Set<WordnetEntry> findWordnetEntriesOnEdge(Wordnet wordnet, MergingStrategy mergingStrategy, int margin) {
		switch (mergingStrategy) {
		case HYPERNYM_ORIGIN:
			return WORDNET_CRAWLER.findEdgeHypernyms(wordnet, margin);
		case HYPONYM_ORIGIN:
			return WORDNET_CRAWLER.findEdgeHyponyms(wordnet, margin);
		default:
			throw new UnsupportedOperationException(mergingStrategy + " is not supported for finding edge entries.");
		}
	}

	public void countHyponymsAndHypernymsRelations(Wordnet wordnet) {
		long hyponymsCount = 0;
		long hypernymsCount = 0;
		for (WordnetEntry wordnetEntry : wordnet) {
			Set<WordnetEntry> tempHypernyms = wordnetEntry.getHypernyms();
			if (tempHypernyms != null) {
				hypernymsCount += tempHypernyms.size();
			}
			Set<WordnetEntry> tempHyponyms = wordnetEntry.getHyponyms();
			if (tempHyponyms != null) {
				hyponymsCount += tempHyponyms.size();
			}
		}
		LOG.info("hypernyms: {}\nhyponyms: {}", hypernymsCount, hyponymsCount);

	}

	public Map<PartOfSpeech, Long> calcPartOfSpeachStats(Wordnet wordnet) {
		return wordnet.stream().collect(Collectors.groupingBy(WordnetEntry::getPos, Collectors.counting()));
	}

	public Map<PartOfSpeech, Set<SynonymEntry>> findSynonymEntriesGroupedByPos(Wordnet wordnet) {
		return wordnet.stream().collect(Collectors.groupingBy(WordnetEntry::getPos,
				Collectors.mapping(WordnetEntry::getSynonyms,
						Collectors.flatMapping(Set::stream, Collectors.toSet()))));
	}

}
