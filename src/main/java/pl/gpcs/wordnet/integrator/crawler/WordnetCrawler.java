package pl.gpcs.wordnet.integrator.crawler;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.crawler.impl.HypernymsGetter;
import pl.gpcs.wordnet.integrator.crawler.impl.HyponymsGetter;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public class WordnetCrawler {

	private static final HypernymsGetter HYPERNYMS_GETTER = new HypernymsGetter();
	private static final HyponymsGetter HYPONYMS_GETTER = new HyponymsGetter();

	public Set<WordnetEntry> findEdgeHypernyms(Wordnet wordnet) {
		return findEdgeHypernyms(wordnet, 0);
	}

	public Set<WordnetEntry> findEdgeHypernyms(Wordnet wordnet, int edgeThreshold) {
		return findEdgeWordnetEntries(wordnet, edgeThreshold, HYPERNYMS_GETTER, HYPONYMS_GETTER);
	}

	public Set<WordnetEntry> findEdgeHyponyms(Wordnet wordnet) {
		return findEdgeHyponyms(wordnet, 0);
	}

	public Set<WordnetEntry> findEdgeHyponyms(Wordnet wordnet, int edgeThreshold) {
		return findEdgeWordnetEntries(wordnet, edgeThreshold, HYPONYMS_GETTER, HYPERNYMS_GETTER);
	}

	private Set<WordnetEntry> findEdgeWordnetEntries(Wordnet wordnet, int edgeThreshold,
			RelativesGetter getter, RelativesGetter reverseGetter) {
		Set<WordnetEntry> edgeEntries = wordnet.stream().filter(getter::emptyRelatives)
				.collect(Collectors.toCollection(HashSet::new));
		if (edgeThreshold < 1) {
			return edgeEntries;
		}
		edgeEntries.addAll(findRelatives(edgeEntries, edgeThreshold, reverseGetter));
		return edgeEntries;
	}

	private Set<? extends WordnetEntry> findRelatives(WordnetEntry originEntry, final int edgeThreshold,
			RelativesGetter relativesGetter) {
		return findRelatives(Collections.singleton(originEntry), edgeThreshold, relativesGetter);
	}

	private Set<? extends WordnetEntry> findRelatives(Set<WordnetEntry> originEntries, final int edgeThreshold,
			RelativesGetter relativesGetter) {
		assert edgeThreshold > 0 : "Edge threshold must be greater than zero when searching for relatives";
		if (relativesGetter.emptyRelativesMulti(originEntries)) {
			return Collections.emptySet();
		}

		Set<WordnetEntry> foundRelatives = new HashSet<>();
		Set<WordnetEntry> parents = originEntries;
		for (int depth = 0; depth < edgeThreshold; ++depth) {
			Set<WordnetEntry> directRelatives = relativesGetter.applyMulti(parents);
			foundRelatives.addAll(directRelatives);
			parents = directRelatives;
		}
		return foundRelatives;
	}

	public Set<WordnetEntry> traverseHypernymDirection(WordnetEntry originEntry) {
		return traverseRelativesDirection(originEntry, HYPERNYMS_GETTER);
	}

	public Set<WordnetEntry> traverseHyponymDirection(WordnetEntry originEntry) {
		return traverseRelativesDirection(originEntry, HYPONYMS_GETTER);
	}

	private Set<WordnetEntry> traverseRelativesDirection(WordnetEntry originEntry, RelativesGetter relativesGetter) {
		if (relativesGetter.emptyRelatives(originEntry)) {
			return new HashSet<>(0);
		}
		Set<WordnetEntry> traversedEntries = new HashSet<>();
		Queue<WordnetEntry> pendingEntries = new LinkedList<>(relativesGetter.apply(originEntry));
		WordnetEntry entry;
		while ((entry = pendingEntries.poll()) != null) {
			if (!traversedEntries.contains(entry)) {
				traversedEntries.add(entry);
				pendingEntries.addAll(relativesGetter.apply(entry));
			}
		}
		return traversedEntries;
	}

	public static Function<WordnetEntry, Set<WordnetEntry>> traverseFunction(MergingStrategy mergingStrategy) {
		WordnetCrawler wordnetCrawler = new WordnetCrawler();
		switch (mergingStrategy) {
		case HYPERNYM_ORIGIN:
			return wordnetCrawler::traverseHyponymDirection;
		case HYPONYM_ORIGIN:
			return wordnetCrawler::traverseHypernymDirection;
		default:
			throw new UnsupportedOperationException(
					mergingStrategy + " is unsupported for retrieving traverse function.");
		}
	}

}
