package pl.gpcs.wordnet.integrator.crawler;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public interface RelativesGetter extends Function<WordnetEntry, Set<WordnetEntry>> {

	default boolean emptyRelatives(WordnetEntry we) {
		Set<WordnetEntry> relatives = apply(we);
		return relatives == null || relatives.isEmpty();
	}

	default boolean emptyRelativesMulti(Collection<WordnetEntry> wordnetEntries) {
		for (WordnetEntry wordnetEntry : wordnetEntries) {
			if (!emptyRelatives(wordnetEntry)) {
				return false;
			}
		}
		return true;
	}

	default int relativesSize(WordnetEntry we) {
		Set<WordnetEntry> relatives = apply(we);
		if (relatives == null || relatives.isEmpty()) {
			return 0;
		}
		return relatives.size();
	}

	default Set<WordnetEntry> applyMulti(Collection<WordnetEntry> wordnetEntries) {
		return wordnetEntries.stream().map(this::apply).filter(Objects::nonNull).flatMap(Set::stream)
				.collect(Collectors.toCollection(HashSet::new));
	}
}
