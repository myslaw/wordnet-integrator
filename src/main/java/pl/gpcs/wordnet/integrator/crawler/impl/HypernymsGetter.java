package pl.gpcs.wordnet.integrator.crawler.impl;

import java.util.Collections;
import java.util.Set;

import pl.gpcs.wordnet.integrator.crawler.RelativesGetter;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public class HypernymsGetter implements RelativesGetter {

	@Override
	public Set<WordnetEntry> apply(WordnetEntry we) {
		return we.getHypernyms() == null ? Collections.emptySet() : we.getHypernyms();
	}

}
