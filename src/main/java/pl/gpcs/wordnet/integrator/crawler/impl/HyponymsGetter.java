package pl.gpcs.wordnet.integrator.crawler.impl;

import java.util.Collections;
import java.util.Set;

import pl.gpcs.wordnet.integrator.crawler.RelativesGetter;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public class HyponymsGetter implements RelativesGetter {

	@Override
	public Set<WordnetEntry> apply(WordnetEntry we) {
		return we.getHyponyms() == null ? Collections.emptySet() : we.getHyponyms();
	}

}
