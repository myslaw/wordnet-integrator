package pl.gpcs.wordnet.integrator.repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.exception.ConjugationReadException;
import pl.gpcs.wordnet.integrator.io.ConjugationReader;
import pl.gpcs.wordnet.integrator.model.verb.Conjugation;

public class VerbRespository implements Repository<Conjugation, String> {
	private final Map<String, Conjugation> verbConjugations;

	public VerbRespository(ConjugationReader reader) throws InitializationException {
		try {
			this.verbConjugations = createVerbConjugationsMap(reader);
		} catch (ConjugationReadException e) {
			throw new InitializationException(e);
		}
	}

	private Map<String, Conjugation> createVerbConjugationsMap(ConjugationReader reader)
			throws ConjugationReadException {
		Map<String, Conjugation> verbConjugationsMap = new HashMap<>();
		Conjugation verbConjugation;
		while ((verbConjugation = reader.read()) != null) {
			verbConjugationsMap.put(verbConjugation.get(), verbConjugation);
		}
		return verbConjugationsMap;
	}

	@Override
	public Conjugation findById(String id) {
		return verbConjugations.get(id);
	}

	@Override
	public Collection<Conjugation> findAll() {
		return verbConjugations.values();
	}

	@Override
	public Conjugation removeById(String id) {
		return verbConjugations.remove(id);
	}

	@Override
	public Conjugation save(Conjugation entity) {
		if (entity == null || entity.get() == null) {
			throw new NullPointerException("Null object nor verb cannot be saved.");
		}
		if (entity.getConjugations() == null || entity.getConjugations().isEmpty()) {
			throw new IllegalArgumentException("Verb conjugations cannot be null nor empty.");
		}
		return verbConjugations.put(entity.get(), entity);
	}

	@Override
	public long count() {
		return verbConjugations.size();
	}

}
