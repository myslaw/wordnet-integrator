package pl.gpcs.wordnet.integrator.repository;

import java.util.Collection;

public interface Repository<T, ID> {
	T findById(ID id);

	Collection<T> findAll();

	T removeById(ID id);

	T save(T entity);

	long count();

}
