package pl.gpcs.wordnet.integrator.matcher;

import java.util.List;
import java.util.Set;

import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.SynonymNavigable;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public interface WordnetMatcher {

	WordnetEntry findBestCandidate(WordnetEntry targetEntry, SynonymNavigable candidatesSource,
			MergingStrategy mergingStrategy);

	Set<WordnetEntry> findCandidatesByTargetEntry(WordnetEntry targetEntry, SynonymNavigable sourceWordnet);

	List<Fit> findFitsByTargetEntry(WordnetEntry targetEntry, SynonymNavigable sourceWordnet,
			MergingStrategy mergingStrategy);

	int calcSynonymsSimilarity(WordnetEntry entry1, WordnetEntry entry2);

	int calcBranchSynonymsSimilarity(WordnetEntry entry1, WordnetEntry entry2, MergingStrategy mergingStrategy);

}
