package pl.gpcs.wordnet.integrator.matcher.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.crawler.WordnetCrawler;
import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.matcher.WordnetMatcher;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.SynonymNavigable;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

@RequiredArgsConstructor
public class WordnetMatcherImpl implements WordnetMatcher {

	private static final WordnetCrawler WORDNET_CRAWLER = new WordnetCrawler();

	@Override
	public WordnetEntry findBestCandidate(WordnetEntry targetEntry, SynonymNavigable candidatesSource,
			MergingStrategy mergingStrategy) {
		Set<WordnetEntry> potentialCandidates = findCandidatesByTargetEntry(targetEntry, candidatesSource);
		int bestSynonymIntersection = 0;
		List<WordnetEntry> bestCandidates = new LinkedList<>();
		for (WordnetEntry potentialCandidate : potentialCandidates) {
			int tempCount = calcSynonymsSimilarity(potentialCandidate, targetEntry);
			if (tempCount > bestSynonymIntersection) {
				bestCandidates.clear();
				bestCandidates.add(potentialCandidate);
				bestSynonymIntersection = tempCount;
			} else if (tempCount == bestSynonymIntersection) {
				bestCandidates.add(potentialCandidate);
			}
		}
		if (bestCandidates.isEmpty()) {
			return null;
		} else if (bestCandidates.size() == 1) {
			return bestCandidates.get(0);
		} else {
			return resolveBestCandidate(targetEntry, bestCandidates, mergingStrategy);
		}
	}

	private WordnetEntry resolveBestCandidate(WordnetEntry targetEntry, Collection<WordnetEntry> bestCandidates,
			MergingStrategy mergingStrategy) {
		Iterator<WordnetEntry> candidatesItr = bestCandidates.iterator();
		WordnetEntry bestCandidate = candidatesItr.next();
		int bestBranchIntersection = calcBranchSynonymsSimilarity(targetEntry, bestCandidate, mergingStrategy);
		while (candidatesItr.hasNext()) {
			WordnetEntry tempCandidate = candidatesItr.next();
			int tempBranchIntersection = calcBranchSynonymsSimilarity(targetEntry, tempCandidate, mergingStrategy);
			if (tempBranchIntersection > bestBranchIntersection) {
				bestCandidate = tempCandidate;
				bestBranchIntersection = tempBranchIntersection;
			}
		}
		return bestCandidate;
	}

	@Override
	public Set<WordnetEntry> findCandidatesByTargetEntry(WordnetEntry targetEntry, SynonymNavigable sourceWordnet) {
		final PartOfSpeech targetPos = targetEntry.getPos();
		Set<WordnetEntry> foundEntries = new HashSet<>();
		for (SynonymEntry synonymEntry : targetEntry.getSynonyms()) {
			Set<WordnetEntry> sourceEntries = sourceWordnet.findBySynonym(synonymEntry.getText());
			if (sourceEntries != null) {
				for (WordnetEntry sourceEntry : sourceEntries) {
					if (targetPos == sourceEntry.getPos()) {
						foundEntries.add(sourceEntry);
					}
				}
			}
		}
		return foundEntries;
	}

	@Override
	public List<Fit> findFitsByTargetEntry(WordnetEntry targetEntry, SynonymNavigable sourceWordnet,
			MergingStrategy mergingStrategy) {
		return findCandidatesByTargetEntry(targetEntry, sourceWordnet).stream()
				.map(sourceEntry -> Fit.of(sourceEntry, targetEntry, mergingStrategy))
				.collect(Collectors.toCollection(LinkedList::new));
	}

	@Override
	public int calcSynonymsSimilarity(WordnetEntry entry1, WordnetEntry entry2) {
		int similarity = 0;
		final Set<String> synonyms1 = entry1.getSynonyms().stream().map(SynonymEntry::getText)
				.collect(Collectors.toSet());
		for (SynonymEntry synonymEntry2 : entry2.getSynonyms()) {
			if (synonyms1.contains(synonymEntry2.getText())) {
				++similarity;
			}
		}
		return similarity;
	}

	@Override
	public int calcBranchSynonymsSimilarity(WordnetEntry entry1, WordnetEntry entry2, MergingStrategy mergingStrategy) {
		// hypernym origin = hyponym direction branch
		switch (mergingStrategy) {
		case HYPERNYM_ORIGIN:
			return calcHyponymBranchSynonymsSimilarity(entry1, entry2);
		case HYPONYM_ORIGIN:
			return calcHypernymBranchSynonymsSimilarity(entry1, entry2);
		default:
			throw new UnsupportedOperationException(mergingStrategy + " is not handled by " + WordnetMatcherImpl.class);
		}
	}

	public int calcHyponymBranchSynonymsSimilarity(WordnetEntry entry1, WordnetEntry entry2) {
		final Set<WordnetEntry> set1 = WORDNET_CRAWLER.traverseHyponymDirection(entry1);
		final Set<WordnetEntry> set2 = WORDNET_CRAWLER.traverseHyponymDirection(entry2);
		return calcSynonymsSimilarity(set1, set2);
	}

	public int calcHypernymBranchSynonymsSimilarity(WordnetEntry entry1, WordnetEntry entry2) {
		final Set<WordnetEntry> set1 = WORDNET_CRAWLER.traverseHypernymDirection(entry1);
		final Set<WordnetEntry> set2 = WORDNET_CRAWLER.traverseHypernymDirection(entry2);
		return calcSynonymsSimilarity(set1, set2);
	}

	private int calcSynonymsSimilarity(Set<WordnetEntry> wordnetEntries1, Set<WordnetEntry> wordnetEntries2) {
		Map<String, Long> synonymToCount2 = countSynonymsOccurances(wordnetEntries2);
		Map<String, Long> synonymToCount1 = wordnetEntries1.stream().map(WordnetEntry::getSynonyms).flatMap(Set::stream)
				.map(SynonymEntry::getText).filter(synonymToCount2::containsKey)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		int similarity = 0;
		for (Entry<String, Long> synonymWithCount2 : synonymToCount2.entrySet()) {
			Long count1 = synonymToCount1.get(synonymWithCount2.getKey());
			if (count1 != null) {
				similarity += Math.min(count1, synonymWithCount2.getValue());
			}
		}
		return similarity;
	}

	private static Map<String, Long> countSynonymsOccurances(Set<WordnetEntry> wordnetEntries) {
		return wordnetEntries.stream().map(WordnetEntry::getSynonyms).flatMap(Set::stream)
				.collect(Collectors.groupingBy(SynonymEntry::getText, Collectors.counting()));
	}

}
