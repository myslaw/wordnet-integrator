package pl.gpcs.wordnet.integrator;

import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.slf4j.helpers.MessageFormatter;

import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import pl.gpcs.wordnet.integrator.io.SynsetCollectionReader;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.printer.DataEvaluator;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.service.WordnetService;
import pl.gpcs.wordnet.integrator.service.impl.WordnetServiceImpl;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetType;

@Slf4j
@Command(name = "wordnet-integrator", mixinStandardHelpOptions = true, version = "wordnet-integrator 1.0.0", description = "Application created for analyzing and merging lexical databases.")
public class Main implements Callable<Integer> {

	private static final String FILE_FORMAT = "mergedWordnet-{}-{}.xml";
	private static final String COMMON_MERGE_DIR = "mergedWordnets";
	private static final String POLNET_TO_PLWORDNET_NAME = "polnetToPlWordnet";
	private static final String PLWORDNET_TO_POLNET_NAME = "plWordnetToPolnet";

	@Option(names = { "-polp", "--polnet-path" }, description = "Path to polnet xml file.", required = true)
	private String polnetPath;

	@Option(names = { "-pwnp", "--plwordnet-path" }, description = "Path to plWordNet xml file.", required = true)
	private String plWordnetPath;

	@Option(names = { "-ow",
			"--output-wordnets" }, description = "Directory to which merged wordnets shoud be exported. Default is ${DEFAULT-VALUE}")
	private String mergedWordnetsDir = Path.of(COMMON_MERGE_DIR).toString();

	@Option(names = { "-sm",
			"--skip-merge" }, description = "Determines if the merging process should run, or just the benchmarking part. Default is ${DEFAULT-VALUE}", arity = "0", negatable = true)
	private boolean skipMerge = false;

	@Option(names = { "-ma",
			"--margins" }, description = "Determines margins for which merging and benchmarking process should run for. The input should consist of integer separated with \",\". Default is ${DEFAULT-VALUE}", split = ",")
	private Set<Integer> margins = Set.of(0);

	public static void main(String[] args) {
		new CommandLine(new Main()).setCaseInsensitiveEnumValuesAllowed(true).execute(args);
	}

	@Override
	public Integer call() throws Exception {
		try {
			LOG.info(
					"Running WordnetIntegrator with params:\npolnet path: {}\nplWordnet path: {}\nmergedWordnets dir: {}\nskipping merge: {}\nmargins: {}",
					polnetPath, plWordnetPath, mergedWordnetsDir, skipMerge, margins);
			final Map<String, Path> nameToPath;
			if (!skipMerge) {
				nameToPath = mergeBothWays();
			} else {
				nameToPath = prepPaths();
			}
			benchmark(nameToPath);
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 1;
		}
	}

	private void benchmark(Map<String, Path> nameToPath) {
		LOG.info("Initializing benchmark...");
		DataEvaluator dataEvaluator = new DataEvaluator(Path.of(plWordnetPath), Path.of(polnetPath), nameToPath);
		LOG.info("Running benchmark...");
		dataEvaluator.printSynsetCountForAllWordnets(); // MGR
		dataEvaluator.printSynonymEntryCountPerWordnetEntry();// MGR
		dataEvaluator.printSynonymCountOccurancesInWordnetEntries(); // MGR
		dataEvaluator.printSemanticRelationStats(Integer.MAX_VALUE); // MGR
		dataEvaluator.printPartOfSpeechStats(); // MGR
		dataEvaluator.printMatchingSynonymTextStats(WordnetType.PLWORDNET, WordnetType.POLNET); // MGR
		LOG.info("Finished benchmark run.");
	}

	private Map<String, Path> prepPaths() {
		final Map<String, Path> nameToPath = new HashMap<>();
		final String nameFormat = "merged-{}-{}-{}";
		final MergingStrategy[] mergingStrategies = { MergingStrategy.HYPERNYM_ORIGIN, MergingStrategy.HYPONYM_ORIGIN };
		final String commonDir = Path.of(mergedWordnetsDir).toString();
		String[] mergeDirections = { PLWORDNET_TO_POLNET_NAME, POLNET_TO_PLWORDNET_NAME };
		for (String mergeDirection : mergeDirections) {
			for (MergingStrategy mergingStrategy : mergingStrategies) {
				for (int margin : margins) {
					String fileName = MessageFormatter
							.arrayFormat(FILE_FORMAT, new Object[] { mergingStrategy, margin }).getMessage();
					Path filePath = Path.of(commonDir, mergeDirection, fileName);
					String wordnetName = MessageFormatter
							.arrayFormat(nameFormat, new Object[] { mergeDirection, mergingStrategy, margin })
							.getMessage();
					nameToPath.put(wordnetName, filePath);
				}
			}
		}
		return nameToPath;
	}

	private Map<String, Path> mergeBothWays() {
		final Map<String, Path> nameToPath = new HashMap<>();
		final Path commonDir = Path.of(mergedWordnetsDir);
		nameToPath.putAll(merge(commonDir.resolve(POLNET_TO_PLWORDNET_NAME), true));
		nameToPath.putAll(merge(commonDir.resolve(PLWORDNET_TO_POLNET_NAME), false));
		return Collections.unmodifiableMap(nameToPath);
	}

	private Map<String, Path> merge(Path baseDir, boolean polnetToPlWordnetMerge) {
		LOG.info("Initializing merge...");
		final List<PlWordnetSynset> plWordnetSynsets = DataEvaluator.readAllSynsets(PlWordnetSynset.class,
				Path.of(plWordnetPath));
		final List<PolnetSynset> polnetSynsets = DataEvaluator.readAllSynsets(PolnetSynset.class, Path.of(polnetPath));
		final WordnetService wordnetService = new WordnetServiceImpl();
		final Map<String, Path> nameToPath = new HashMap<>();
		for (MergingStrategy mergingStrategy : MergingStrategy.values()) {
			for (int edgeThreshold : margins) {
				LOG.info("Running merge for {} with margin: {}...", mergingStrategy, edgeThreshold);
				Wordnet polnet = wordnetService.createWordnetForPolnet(new SynsetCollectionReader<>(polnetSynsets));
				Wordnet plWordnet = wordnetService
						.createWordnetForPlWordnet(new SynsetCollectionReader<>(plWordnetSynsets));
				Wordnet mergedWordnet;
				if (polnetToPlWordnetMerge) {
					mergedWordnet = wordnetService.mergeWordnets(plWordnet, polnet, mergingStrategy, edgeThreshold); // ORIGINAL
				} else {
					mergedWordnet = wordnetService.mergeWordnets(polnet, plWordnet, mergingStrategy, edgeThreshold); // REVERSE
				}
				String fileName = MessageFormatter
						.arrayFormat(FILE_FORMAT, new Object[] { mergingStrategy, edgeThreshold }).getMessage();
				LOG.info("Finished merging for {} with margin: {}. Exporting merged Wordnet to file {}...",
						mergingStrategy, edgeThreshold, fileName);
				Path path = baseDir.resolve(fileName);
				DataEvaluator.exportWordnetAsNormalized(mergedWordnet, path); // REVERSE
				nameToPath.put(fileName, path);
				LOG.info("Finished exporting to {}.", path);
			}
		}
		String generatedWordnetsMsg = nameToPath.entrySet().stream()
				.map(entry -> entry.getKey() + ", path: " + entry.getValue()).collect(Collectors.joining("\n"));
		LOG.info("Generated wordnets:\n{}", generatedWordnetsMsg);
		return nameToPath;
	}

}
