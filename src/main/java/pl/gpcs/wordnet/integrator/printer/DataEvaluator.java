package pl.gpcs.wordnet.integrator.printer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import pl.gpcs.wordnet.integrator.Constants;
import pl.gpcs.wordnet.integrator.adapter.normalized.NormalizedAdapter;
import pl.gpcs.wordnet.integrator.adapter.plwordnet.PlWordnetSynsetAdapter;
import pl.gpcs.wordnet.integrator.bench.SynsetBenchmark;
import pl.gpcs.wordnet.integrator.bench.WordnetBenchmark;
import pl.gpcs.wordnet.integrator.crawler.WordnetCrawler;
import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.filter.PlWordnetOmmitEnglishFilter;
import pl.gpcs.wordnet.integrator.filter.SynsetFilter;
import pl.gpcs.wordnet.integrator.io.SynsetCollectionReader;
import pl.gpcs.wordnet.integrator.io.SynsetFileReader;
import pl.gpcs.wordnet.integrator.io.SynsetFileWriter;
import pl.gpcs.wordnet.integrator.model.common.SynonymLiteral;
import pl.gpcs.wordnet.integrator.model.common.Synset;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;
import pl.gpcs.wordnet.integrator.model.common.SynsetSynonym;
import pl.gpcs.wordnet.integrator.model.synset.normalized.NormalizedSynset;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.model.util.ReducedSynset;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.service.WordnetService;
import pl.gpcs.wordnet.integrator.service.impl.WordnetServiceImpl;
import pl.gpcs.wordnet.integrator.util.Identifiable;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.WordnetType;
import pl.gpcs.wordnet.integrator.util.tuples.Pair;

/**
 * Responsible for pritning data collected in test/benchmarks. Also responsible
 * for exporting it to appropriate formats if needed
 * 
 * @author przem
 *
 */
public class DataEvaluator {

	private static final Logger LOG = LoggerFactory.getLogger(DataEvaluator.class);
	private static final WordnetCrawler WORDNET_CRAWLER = new WordnetCrawler();
	private static final PlWordnetSynsetAdapter PLWORDNET_ADAPTER = new PlWordnetSynsetAdapter();
	private static final NormalizedAdapter NORMALIZED_ADAPTER = new NormalizedAdapter();
	private static final boolean SKIP_PLWORDNET = false;
	private static final boolean SKIP_POLNET = false;
	private static final boolean SKIP_MERGED = false;
	private static final JsonMapper JSON_MAPPER = JsonMapper.builder().build();

	@Deprecated
	private final SynsetBenchmark synsetBenchmark;
	private final WordnetBenchmark wordnetBenchmark;
	private final WordnetService wordnetService = new WordnetServiceImpl();
	private final List<PlWordnetSynset> plWordnetSynsetList;
	private final List<PolnetSynset> polnetSynsetList;
	private final boolean latexFormatEnabled = true;
	private final Set<String> processedWordnets;
	private final Map<String, Wordnet> typeToWordnet = new HashMap<>();

	@SuppressWarnings("resource")
	public DataEvaluator(Path plWordnetPath, Path polnetPath, Map<String, Path> mergedWordnets) {
		final Wordnet plWordnet;
		final Wordnet polnet;
		Set<String> tempProcessedWordnets = new HashSet<>();
		if (SKIP_PLWORDNET) {
			plWordnet = new Wordnet(PlWordnetSynset.class.getSimpleName() + "-UninitializedWordnet",
					WordnetType.PLWORDNET);
			plWordnetSynsetList = Collections.emptyList();
		} else {
			plWordnetSynsetList = readAllSynsets(PlWordnetSynset.class, plWordnetPath);
			plWordnet = wordnetService.createWordnetForPlWordnet(new SynsetCollectionReader<>(plWordnetSynsetList));
			tempProcessedWordnets.add(WordnetType.PLWORDNET.toString());
		}
		typeToWordnet.put(plWordnet.getWordnetType().toString(), plWordnet);

		if (SKIP_POLNET) {
			polnet = new Wordnet(PolnetSynset.class.getSimpleName() + "UninitializedWordnet", WordnetType.POLNET);
			polnetSynsetList = Collections.emptyList();
		} else {
			polnetSynsetList = readAllSynsets(PolnetSynset.class, polnetPath);
			polnet = wordnetService.createWordnetForPolnet(new SynsetCollectionReader<>(polnetSynsetList));
			tempProcessedWordnets.add(WordnetType.POLNET.toString());
		}
		typeToWordnet.put(polnet.getWordnetType().toString(), polnet);

		synsetBenchmark = new SynsetBenchmark(plWordnetSynsetList, polnetSynsetList);

		if (!SKIP_MERGED) {
			for (Entry<String, Path> nameWithPath : mergedWordnets.entrySet()) {
				String wordnetName = nameWithPath.getKey();
				Wordnet mergedWordnet = wordnetService.createWordnetForNormalized(
						new SynsetCollectionReader<>(readAllSynsets(NormalizedSynset.class, nameWithPath.getValue())),
						wordnetName);
				typeToWordnet.put(wordnetName, mergedWordnet);
				tempProcessedWordnets.add(wordnetName);
			}
		}
		this.processedWordnets = Collections.unmodifiableSet(tempProcessedWordnets);
		wordnetBenchmark = new WordnetBenchmark(plWordnet, polnet, plWordnetSynsetList);
	}

	public static <T extends Synset<?, ?>> List<T> readAllSynsets(Class<T> clazz, Path path) {
		SynsetFilter<T> pojoFilter = null;
		if (clazz == PlWordnetSynset.class) {
			pojoFilter = (SynsetFilter<T>) new PlWordnetOmmitEnglishFilter();
		}
		try (SynsetFileReader<T> reader = new SynsetFileReader<>(clazz, path, null, pojoFilter)) {
			List<T> list = new LinkedList<>();
			T synset;
			while ((synset = reader.read()) != null) {
				list.add(synset);
			}
			return list;
		} catch (Exception e) {
			throw new RuntimeException(e); // FIXME
		}
	}

	private Wordnet wordnetFor(String name) {
		final Wordnet wordnet = typeToWordnet.get(name);
		if (wordnet == null) {
			throw new UnsupportedOperationException("Wordnet for " + name + " was not found in " + DataEvaluator.class);
		}
		return wordnet;
	}

	private List<? extends Synset<? extends SynsetIlr, ? extends SynsetSynonym<? extends SynonymLiteral<?>>>> synsetListFor(
			WordnetType wordnetType) {
		return wordnetType == WordnetType.PLWORDNET ? plWordnetSynsetList : polnetSynsetList;
	}

	public void printOccurancesToSynonymCount(WordnetType wordnetType) {
		StringBuilder sb = new StringBuilder(
				"Number of occurances for each synonym count (synonym_count: occurances):");
		synsetBenchmark.calcSynonymCountToOccurances(synsetListFor(wordnetType)).entrySet().stream()
				.sorted((v1, v2) -> Integer.compare(v1.getKey(), v2.getKey()))
				.map(e -> e.getKey() + ": " + e.getValue()).forEach(str -> sb.append("\n").append(str));
		LOG.info(sb.toString());
	}

	public void printEdgeHypernyms(WordnetType wordnetType) {
		Set<WordnetEntry> edgeHypernyms = WORDNET_CRAWLER.findEdgeHypernyms(wordnetFor(wordnetType.toString()));
		StringBuilder sb = new StringBuilder("Edge hypernyms ").append("(size: ").append(edgeHypernyms.size())
				.append(") for ").append(wordnetType).append(" [id: synonyms]: ");
		for (WordnetEntry edgeHypernym : edgeHypernyms) {
			sb.append("\n").append(edgeHypernym.getId()).append(": ").append(
					edgeHypernym.getSynonyms().stream().map(SynonymEntry::getText).collect(Collectors.joining(", ")));
		}
		LOG.info(sb.toString());
	}

	public void printRootHypernymsForEntry(WordnetType wordnetType, String entryId) {
		printRootHypernymsForEntry(wordnetFor(wordnetType.toString()).findById(entryId));
	}

	public void printRootHypernymsForEntry(WordnetEntry wordnetEntry) {
		StringBuilder sb = new StringBuilder("WordnetEntry with ID: ").append(wordnetEntry.getId())
				.append(", has following root hypernyms: \n");
		for (WordnetEntry rootHypernym : wordnetBenchmark.findRootHypernyms(wordnetEntry)) {
			sb.append("ID: ").append(rootHypernym.getId());
			sb.append(", synonyms: ").append(
					rootHypernym.getSynonyms().stream().map(SynonymEntry::getText).collect(Collectors.joining(", ")))
					.append("\n");
		}
		LOG.info(sb.toString());
	}

	public void printUniqueSynonymCount(WordnetType wordnetType) {
		int count = wordnetBenchmark.countUniqueSynonyms(wordnetFor(wordnetType.toString()));
		LOG.info("Number of unique synonyms for {} is {}.", wordnetType, count);
	}

	public void printAvgSynonymCountPerWordnetEntry(WordnetType wordnetType) {
		double avgSynonymCountPerWordnetEntry = wordnetBenchmark
				.calcAvgSynonymCountPerWordnetEntry(wordnetFor(wordnetType.toString()));
		LOG.info("Average number of synonyms per {} entry is {}", wordnetType, avgSynonymCountPerWordnetEntry);
	}

	/**
	 * Prints count per each semantic realtion
	 */
	public void printSemanticRelationStats(int topLimit) {
		StringBuilder latexPlot = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			List<Entry<String, Integer>> list = wordnetBenchmark.calcSemanticRelationStats(wordnetFor(wordnetName));
			Collections.reverse(list);
			if (topLimit != Integer.MAX_VALUE) {
				list = list.subList(0, topLimit);
			}
			int allRelationsCount = list.stream().map(Entry::getValue).mapToInt(Integer::intValue).sum();
			latexPlot.append("\n\t").append(wordnetName).append("(relations count: ").append(allRelationsCount)
					.append("):");
			String labels = list.stream().map(Entry::getKey).map(label -> label.replace("_", "\\_"))
					.collect(Collectors.joining(", "));
			latexPlot.append("\n\txticklabels={").append(labels).append("}");
			int index = 0;
			for (Entry<String, Integer> entry : list) {
				latexPlot.append("\n\t\t").append("(").append(index).append(", ").append(entry.getValue()).append(")");
				++index;
			}
		}
		LOG.info("Showing count for each semantic relation:{}", latexPlot);
	}

	public void printDomainEntries(Set<PlWordnetSynset> domainEntries) {
		LOG.info("Entries in " + domainEntries.iterator().next().getDomain() + " domain.");
		String entries = domainEntries.stream().flatMap(entry -> entry.getSynonym().getLiteral().stream())
				.map(SynonymLiteral::getText).sorted().collect(Collectors.joining(", "));
		LOG.info(entries);
	}

	public void printDomainSizes(Map<String, Integer> domainToSize) {
		String string = domainToSize.entrySet().stream().sorted((o1, o2) -> o1.getValue().compareTo(o2.getValue()))
				.map(entry -> entry.getKey() + ":" + entry.getValue()).collect(Collectors.joining("\n\t"));
//				.forEach(t -> System.out.println("\t" + t.getKey() + " : " + t.getValue()));
		LOG.info("Domain sizes: {}\n\t", string);
	}

	public void exportSpecificFigures() {

		Set<String> specificFigures = new HashSet<>(Arrays.asList("deltoid", "dodekaedr", "elipsa", "gwiazda",
				"heksagram", "hiperbola", "jajo", "koło", "krzywa", "krzyż", "krąg", "kula", "kwadrat", "kółko",
				"linia", "okrąg", "owal", "prostokąt", "pryzma", "półelipsa", "półkole", "półokrąg", "romboid",
				"rombościan", "stożek", "trójkąt", "walec"));
		LOG.debug("Specific figures size: {}", specificFigures.size());
		Set<WordnetEntry> wordnetEntrySet = synsetBenchmark.findAllByDomain(WordnetType.PLWORDNET, "ksz => kształty")
				.stream()
				.filter(synset -> synset.getSynonym().getLiteral().stream().map(SynonymLiteral::getText)
						.anyMatch(specificFigures::contains))
				.map(PLWORDNET_ADAPTER::createWordnetEntry).collect(Collectors.toSet());
		exportWordnetEntriesAsPlWordnet(makeHermeticWordnetEntries(wordnetEntrySet),
				Path.of("src", "test", "resources", "ksztalty-DOMAIN_SUBSET.xml"));
	}

	public static void exportWordnetEntriesAsPlWordnet(Set<WordnetEntry> wordnetEntrySet, Path path) {
		Set<PlWordnetSynset> sysnsetSet = wordnetEntrySet.stream().map(PLWORDNET_ADAPTER::createSynset)
				.collect(Collectors.toSet());
		LOG.info("Exported wordnet entries size: {}", sysnsetSet.size());
		try (SynsetFileWriter<PlWordnetSynset> writer = new SynsetFileWriter<>(PlWordnetSynset.class, path)) {
			sysnsetSet.forEach(writer::write);
		} catch (IOException | InitializationException e) {
			throw new RuntimeException(e);
		}
	}

	public static void exportWordnetAsNormalized(Wordnet wordnet, Path path) {
		Set<NormalizedSynset> synsets = wordnet.stream().map(NORMALIZED_ADAPTER::createSynset)
				.collect(Collectors.toSet());
		LOG.info("Exported wordnet entries size: {}", synsets.size());
		try (SynsetFileWriter<NormalizedSynset> writer = new SynsetFileWriter<>(NormalizedSynset.class, path)) {
			for (NormalizedSynset synset : synsets) {
				try {
					writer.write(synset);
				} catch (RuntimeException e) {
					LOG.error("Failed to export following {}:{}", NormalizedSynset.class.getSimpleName(), synset);
					throw e;
				}
			}
		} catch (IOException | InitializationException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Wordnet entries that may reference only entries provided in parameter
	 * 
	 * @param originalEntries entries to create new subWordnet
	 * @return
	 */
	private Set<WordnetEntry> makeHermeticWordnetEntries(Collection<WordnetEntry> originalEntries) {
		Set<String> knownIds = originalEntries.stream().map(WordnetEntry::getId).collect(Collectors.toSet());
		Set<WordnetEntry> independentEntries = new HashSet<>(originalEntries.size());
		for (WordnetEntry originalEntry : originalEntries) {
			WordnetEntry independentEntry = new WordnetEntry(originalEntry.getId());
			independentEntry.setSynonyms(new HashSet<>(originalEntry.getSynonyms()));
			independentEntry.setHypernyms(collectIncludedIds(originalEntry.getHypernyms(), knownIds));
			independentEntry.setHyponyms(collectIncludedIds(originalEntry.getHyponyms(), knownIds));
			independentEntry.setGeneralSemanticRelations(
					collectIncludedIds(originalEntry.getGeneralSemanticRelations(), knownIds));
			independentEntries.add(independentEntry);
		}
		return independentEntries;
	}

	private <I, T extends Identifiable<I>> Set<T> collectIncludedIds(Set<T> entries, Set<I> ids) {
		if (entries == null) {
			return null;
		}
		if (ids == null || ids.isEmpty()) {
			return entries;
		}
		Set<T> includedEntries = new HashSet<>();
		for (T entry : entries) {
			if (ids.contains(entry.getId())) {
				includedEntries.add(entry);
			}
		}
		return includedEntries;
	}

	public void printSynonymEntryCount(WordnetType wordnetType) {
		Wordnet wordnet = wordnetFor(wordnetType.toString());
		int synonymCount = 0;
		for (WordnetEntry wordnetEntry : wordnet) {
			if (wordnetEntry.isInitialized()) {
				synonymCount += wordnetEntry.getSynonyms().size();
			}
		}
		LOG.info("\nTotal number of synonym entries: {}", synonymCount);
		LOG.info("Synonyms per wordnet entry (avg): {}", synonymCount * 1.0 / wordnet.size());
	}

	public void printCommonSynonymsTexts(WordnetType wordnetType1, WordnetType wordnetType2) {
		Set<String> commonSynonymsTexts = wordnetBenchmark
				.findAllCommonSynonymsTexts(wordnetFor(wordnetType1.toString()), wordnetFor(wordnetType2.toString()));
		LOG.info("Found {} common synonyms texts between {} and {}:\n{}", commonSynonymsTexts.size(), wordnetType1,
				wordnetType2, commonSynonymsTexts.stream().collect(Collectors.joining("\n")));
	}

	public void printCommonEdgeSynonymsTexts(WordnetType wordnetType1, WordnetType wordnetType2) {
		Set<String> commonEdgeSynonymsTexts = wordnetBenchmark
				.findEdgeCommonSynonymsTexts(wordnetFor(wordnetType1.toString()), wordnetFor(wordnetType2.toString()));
		LOG.info("Found {} common edge synonyms texts between {} and {}:\n{}", commonEdgeSynonymsTexts.size(),
				wordnetType1, wordnetType2, commonEdgeSynonymsTexts.stream().collect(Collectors.joining("\n")));
	}

	public void printCommonEdgeToWholeSynonymsTexts(WordnetType wordnetType1, WordnetType wordnetType2) {
		Set<String> edgeToWholeCommonSynonymsTexts = wordnetBenchmark.findEdgeHypernymsToWholeCommonSynonymsTexts(
				wordnetFor(wordnetType1.toString()), wordnetFor(wordnetType2.toString()));
		LOG.info("Found {} common edge synonyms texts between {} and {}:\n{}", edgeToWholeCommonSynonymsTexts.size(),
				wordnetType1, wordnetType2, edgeToWholeCommonSynonymsTexts.stream().collect(Collectors.joining("\n")));
	}

	public void printAvgSynonymCountPerEntry(WordnetType wordnetType) {
		StringBuilder sb = new StringBuilder("Average synonyms count per entry for ").append(wordnetType).append(": ");
		double average = wordnetBenchmark.calcAvgSynonymCountPerWordnetEntry(wordnetFor(wordnetType.toString()));
		sb.append(average);
		LOG.info(sb.toString());
	}

	public void printAvgSynonymCountOnEdge(WordnetType wordnetType, MergingStrategy mergingStrategy) {
		StringBuilder sb = new StringBuilder("Average synonyms count per entry on edge for ").append(wordnetType)
				.append(": ");
		sb.append(wordnetBenchmark.calcAvgSynonymCountPerWordnetEntryOnEdge(wordnetFor(wordnetType.toString()),
				mergingStrategy));
		LOG.info(sb.toString());
	}

	public void exportReducedSynset(Path path) {
		final XmlMapper mapper = new XmlMapper();
		Collection<ReducedSynset> snotes = synsetBenchmark.findSNotes(polnetSynsetList);
		List<String> serialized = new ArrayList<>(snotes.size());
		for (ReducedSynset reducedSynset : snotes) {
			try {
				serialized.add(mapper.writeValueAsString(reducedSynset));
			} catch (JsonProcessingException e) {
				throw new RuntimeException(e);
			}
		}
		try {
			Files.write(path, serialized);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void printSynonymsToEdgeHypernym(WordnetType wordnetType, String id) {
		WordnetEntry superHyponym = wordnetFor(wordnetType.toString()).findById(id);
		if (superHyponym == null) {
			throw new IllegalArgumentException("Entry with id " + id + " does not exist");
		}
		List<List<WordnetEntry>> paths = wordnetBenchmark.findPathsToEdgeHypernym(superHyponym);
		StringBuilder sb = new StringBuilder();
		final String levelSeparator = " -> ";
		int pathNumber = 1;
		for (List<WordnetEntry> path : paths) {
			sb.append(pathNumber).append(". ");
			for (WordnetEntry pathEntry : path) {
				String synonyms = pathEntry.getSynonyms().stream().map(SynonymEntry::getText)
						.collect(Collectors.joining(","));
				sb.append(synonyms).append(levelSeparator);
			}
			sb.delete(sb.length() - levelSeparator.length(), sb.length());
			sb.append("\n");
			++pathNumber;
		}
		LOG.info("Synonyms paths found from entry with id {} to ultra hypernym \n{}", id, sb.toString());
	}

	/**
	 * Prints length for a path starting at edge hyponym and going to edge hypernym.
	 * Result shows starting hyponym ID, ending hypernym ID and length itself.
	 * Reverse order (from hypernym to hyponym) would not change the results
	 * 
	 * @param wordnetType
	 */
	public void printPathLenghtPerBranch(WordnetType wordnetType) {
		Map<Integer, List<Pair<String, String>>> lengthToEdgeIds = new HashMap<>();
		Map<Pair<String, String>, Integer> edgeIdToPathLength = new HashMap<>();
		final Set<WordnetEntry> edgeHyponyms = WORDNET_CRAWLER.findEdgeHyponyms(wordnetFor(wordnetType.toString()));
		for (WordnetEntry hyponym : edgeHyponyms) {
			LOG.debug("Processing hyponym with id {}", hyponym.getId());
			List<List<WordnetEntry>> paths = wordnetBenchmark.findPathsToEdgeHypernym(hyponym);
			for (List<WordnetEntry> path : paths) {
				WordnetEntry first = path.get(0);
				WordnetEntry last = path.get(path.size() - 1);
				edgeIdToPathLength.computeIfAbsent(Pair.of(first.getId(), last.getId()), pair -> path.size());
			}
		}

		final String pathLengthPerBranch = edgeIdToPathLength.entrySet().stream()
				.sorted(Comparator.comparingInt(Entry::getValue)).map(entry -> {
					Pair<String, String> key = entry.getKey();
					StringBuilder stringRecord = new StringBuilder();
					stringRecord.append(key.getObj0()).append("-").append(key.getObj1());
					stringRecord.append(": ").append(entry.getValue());
					return stringRecord.toString();
				}).collect(Collectors.joining("\n"));
		LOG.info("Length per branch from edge to edge ([edgeStartId-edgeEndId]: length):\n{}", pathLengthPerBranch);
	}

	// MGR
	public void printSynsetCountForAllWordnets() {
		StringBuilder sb = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			Collection<WordnetEntry> tempSynsents = wordnetBenchmark.findAllSynsets(wordnetFor(wordnetName));
			sb.append(wordnetName).append(": ").append(tempSynsents.size()).append("\n");
		}
		LOG.info("Number of synset per each Wordnet:\n{}", sb.toString());
	}

	// MGR
	public void printSynsetCountOnEdge(int... margins) {
		StringBuilder msg = new StringBuilder();
		StringBuilder latex = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			Wordnet tempWordnet = wordnetFor(wordnetName);
			for (MergingStrategy mergingStrategy : MergingStrategy.values()) {
				latex.append("\n\t").append(wordnetName).append("-").append(mergingStrategy).append(":");
				msg.append("\n\t").append(wordnetName).append("-").append(mergingStrategy);
				for (Integer margin : margins) {
					Set<WordnetEntry> edgeEntries = wordnetBenchmark.findWordnetEntriesOnEdge(tempWordnet,
							mergingStrategy, margin);
					msg.append("\n\t\t(margin: ").append(margin).append("): ").append(edgeEntries.size());
					latex.append("\n\t\t").append("(").append(margin).append(", ").append(edgeEntries.size())
							.append(")");
				}
			}
		}
		if (latexFormatEnabled) {
			LOG.info("Edges size for each wordnet latex format:{}", latex.toString());
		}
		LOG.info("Edges size for each wordnet:{}", msg.toString());

	}

	public void printCountHyponymsAndHypernymsRelations() {
		for (String wordnetName : processedWordnets) {
			wordnetBenchmark.countHyponymsAndHypernymsRelations(wordnetFor(wordnetName));
		}
	}

	// MGR
	public void printSynonymEntryCount() {
		StringBuilder latex = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			Wordnet wordnet = wordnetFor(wordnetName);
			int uniqueSynonymEntryCount = wordnetBenchmark.countUniqueSynonyms(wordnet);
			latex.append("\n\t").append(wordnetName).append(": ").append(uniqueSynonymEntryCount);
		}
		LOG.info("Unique SynonymEntry count per wordnet:{}", latex);
	}

	// MGR
	public void printSynonymEntryCountPerWordnetEntry() {
		StringBuilder latex = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			Wordnet wordnet = wordnetFor(wordnetName);
			int uniqueSynonymEntryCount = wordnetBenchmark.countUniqueSynonyms(wordnet);
			int wordnetSize = wordnet.size();
			double avg = (double) uniqueSynonymEntryCount / wordnetSize;
			latex.append("\n\t").append(wordnetName).append(": ").append(avg);
		}
		LOG.info("Unique SynonymEntry count per WordnetEntry:{}", latex);
	}

	public void printSynonymCountOccurancesInWordnetEntries() {
		StringBuilder latex = new StringBuilder();
		StringBuilder latexTable = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			Wordnet wordnet = wordnetFor(wordnetName);
			latex.append("\n\t").append(wordnetName).append(":");
			latexTable.append("\n\t").append(wordnetName).append(":");
			Map<Integer, Integer> synonymCountToOccurances = wordnetBenchmark.calcSynonymCountToOccurances(wordnet);
			for (Entry<Integer, Integer> entry : synonymCountToOccurances.entrySet()) {
				latex.append("\n\t\t(").append(entry.getKey()).append(", ").append(entry.getValue()).append(")");
				latexTable.append("\n").append(entry.getKey()).append("\t&\t").append(entry.getValue()).append("\\\\");
			}
			LOG.info("SynonymEntries total: {}", synonymCountToOccurances.entrySet().stream()
					.mapToInt(entry -> entry.getKey() * entry.getValue()).sum());
		}
		LOG.info("Number of specific synonymEntry size occurances in wordnets:{}", latex.toString());
		LOG.info("Table data for latex:\n{}", latexTable.toString());
	}

	public void printMostPopularSynonymTexts(int topCount) {
		StringBuilder latex = new StringBuilder();
		for (String wordnetName : processedWordnets) {
			throw new UnsupportedOperationException();
			// FIXME
		}
		LOG.info("Top {} most popular synonyms:{}", topCount, latex);
	}

	public void printPartOfSpeechStats() {
		StringBuilder latexTable = new StringBuilder();
		StringBuilder latexPieChart = new StringBuilder();
		DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));
		for (String wordnetName : processedWordnets) {
			Map<PartOfSpeech, Long> posToCount = wordnetBenchmark.calcPartOfSpeachStats(wordnetFor(wordnetName));
			long totalPosCount = posToCount.values().stream().mapToLong(Long::longValue).sum();
			latexTable.append("\n\t").append(wordnetName).append("(total pos: ").append(totalPosCount).append("):");
			latexPieChart.append("\n\t").append(wordnetName).append("(total pos: ").append(totalPosCount).append("):");
			latexPieChart.append("\n\t\t\\pie {");
			for (Entry<PartOfSpeech, Long> posWithCount : posToCount.entrySet()) {
				latexTable.append("\n\t\t").append(polishNameForPos(posWithCount.getKey())).append("\t&\t")
						.append(posWithCount.getValue()).append("\\\\");
				double percentage = 100 * posWithCount.getValue().doubleValue() / totalPosCount;
				latexPieChart.append("\n\t\t").append(df.format(percentage)).append("/")
						.append(polishNameForPos(posWithCount.getKey())).append(",");
			}
			latexPieChart.deleteCharAt(latexPieChart.length() - 1);
			latexPieChart.append("\n\t\t}");
		}
		LOG.info("Part of Speach count:{}", latexTable.toString());
		LOG.info("Part of Speach percentage share:{}", latexPieChart.toString());
	}

	public void printMatchingSynonymTextStats(WordnetType firstType, WordnetType secondType) {
		StringBuilder latexTable = new StringBuilder();
		StringBuilder latexPieChart = new StringBuilder("\t\t\\pie[text=legend] {");
		Map<PartOfSpeech, Set<SynonymEntry>> firstPosToSynonymEntries = wordnetBenchmark
				.findSynonymEntriesGroupedByPos(wordnetFor(firstType.toString()));
		Map<PartOfSpeech, Set<SynonymEntry>> secondPosToSynonymEntries = wordnetBenchmark
				.findSynonymEntriesGroupedByPos(wordnetFor(firstType.toString()));
		Map<PartOfSpeech, Integer> posToCommonSynonyms = new EnumMap<>(PartOfSpeech.class);
		for (PartOfSpeech partOfSpeech : PartOfSpeech.values()) {
			Set<String> firstSynonyms = firstPosToSynonymEntries.get(partOfSpeech).stream().map(SynonymEntry::getText)
					.collect(Collectors.toSet());
			Set<String> secondSynonyms = secondPosToSynonymEntries.get(partOfSpeech).stream().map(SynonymEntry::getText)
					.collect(Collectors.toSet());
			int commonSynonymsCount = 0;
			for (String secondSynonym : secondSynonyms) {
				if (firstSynonyms.contains(secondSynonym)) {
					++commonSynonymsCount;
				}
			}
			posToCommonSynonyms.put(partOfSpeech, commonSynonymsCount);
		}
		final int totalCommonSynonymsCount = posToCommonSynonyms.values().stream().mapToInt(Integer::intValue).sum();
		final DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));
		for (Entry<PartOfSpeech, Integer> pwc : posToCommonSynonyms.entrySet()) {
			latexTable.append("\n\t\t").append(polishNameForPos(pwc.getKey())).append("\t&\t").append(pwc.getValue())
					.append("\\\\");
			double percentageShare = 100d * pwc.getValue() / totalCommonSynonymsCount;
			latexPieChart.append("\n\t\t").append(df.format(percentageShare)).append("/")
					.append(polishNameForPos(pwc.getKey())).append(",");
		}

		latexPieChart.deleteCharAt(latexPieChart.length() - 1);
		latexPieChart.append("\n\t\t}");
		LOG.info("Synonym entries texts common for {} and {} wordnets:{}", firstType, secondType, latexTable);
		LOG.info("Pie chart for common synonyms:\n{}", latexPieChart);
	}

	private static String polishNameForPos(PartOfSpeech pos) {
		switch (pos) {
		case ADJECTIVE:
			return "przymiotnik";
		case ADVERB:
			return "przysłówek";
		case NOUN:
			return "rzeczownik";
		case VERB:
			return "czasownik";
		default:
			throw new UnsupportedOperationException(pos + " is not supported for polish translation yet.");
		}
	}
}
