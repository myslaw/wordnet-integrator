package pl.gpcs.wordnet.integrator.printer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;

@RequiredArgsConstructor
public class MergeStatsPreparator {

	private static final DecimalFormat TWO_DECIMALS_FORMAT = new DecimalFormat("0.00",
			new DecimalFormatSymbols(Locale.US));
	private static final String PIE_CHART = "\\pie[text=legend] {";

	private final String eachLinePrefix;

	private static String calcPercentageShare(long part, long total) {
		double percentageShare = 100d * part / total;
		return TWO_DECIMALS_FORMAT.format(percentageShare);
	}
	
	private static String polishNameForPos(PartOfSpeech pos) {
		switch (pos) {
		case ADJECTIVE:
			return "przymiotnik";
		case ADVERB:
			return "przysłówek";
		case NOUN:
			return "rzeczownik";
		case VERB:
			return "czasownik";
		default:
			throw new UnsupportedOperationException(pos + " is not supported for polish translation yet.");
		}
	}

	public String prepMergedSynsetEntryPercentageStats(Map<PartOfSpeech, Integer> posToMergedSynonymsCount) {
		StringBuilder latexPie = new StringBuilder(eachLinePrefix).append(PIE_CHART);
		final int totalCount = posToMergedSynonymsCount.values().stream().mapToInt(Integer::intValue).sum();
		for (Entry<PartOfSpeech, Integer> posWithCount : posToMergedSynonymsCount.entrySet()) {
			latexPie.append(eachLinePrefix).append(calcPercentageShare(posWithCount.getValue(), totalCount)).append("/")
					.append(polishNameForPos(posWithCount.getKey())).append(",");
		}
		latexPie.deleteCharAt(latexPie.length() - 1);
		latexPie.append("}");
		return latexPie.toString();
	}

	public String prepMergedWordnetEntriesPercentageStats(Map<PartOfSpeech, Integer> posToMergedWordnetEntries) {
		StringBuilder latexPie = new StringBuilder(eachLinePrefix).append(PIE_CHART);
		final int totalCount = posToMergedWordnetEntries.values().stream().mapToInt(Integer::intValue).sum();
		for (Entry<PartOfSpeech, Integer> posWithCount : posToMergedWordnetEntries.entrySet()) {
			latexPie.append(eachLinePrefix).append(calcPercentageShare(posWithCount.getValue(), totalCount)).append("/")
					.append(polishNameForPos(posWithCount.getKey())).append(",");

		}
		latexPie.deleteCharAt(latexPie.length() - 1);
		latexPie.append("}");
		return latexPie.toString();
	}

	public String prepMergedWordnetEntriesStats(Map<PartOfSpeech, Integer> posToMergedWordnetEntries) {
		throw new UnsupportedOperationException("Not implemented yet");
		// FIXME
	}
}
