package pl.gpcs.wordnet.integrator.exception;

public class ConjugationReadException extends EntityReadException {

	public ConjugationReadException() {
		super();
	}

	public ConjugationReadException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ConjugationReadException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConjugationReadException(String message) {
		super(message);
	}

	public ConjugationReadException(Throwable cause) {
		super(cause);
	}

}
