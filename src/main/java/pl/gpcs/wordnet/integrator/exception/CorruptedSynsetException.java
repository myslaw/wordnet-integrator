package pl.gpcs.wordnet.integrator.exception;

public class CorruptedSynsetException extends IllegalArgumentException {

	public CorruptedSynsetException() {
		super();
	}

	public CorruptedSynsetException(String message, Throwable cause) {
		super(message, cause);
	}

	public CorruptedSynsetException(String message) {
		super(message);
	}

	public CorruptedSynsetException(Throwable cause) {
		super(cause);
	}

}
