package pl.gpcs.wordnet.integrator.exception;

public class SynsetWriteException extends RuntimeException {

	public SynsetWriteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SynsetWriteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SynsetWriteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SynsetWriteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SynsetWriteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
