package pl.gpcs.wordnet.integrator.exception;

public class SynsetReadException extends EntityReadException {

	public SynsetReadException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SynsetReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SynsetReadException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SynsetReadException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SynsetReadException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
