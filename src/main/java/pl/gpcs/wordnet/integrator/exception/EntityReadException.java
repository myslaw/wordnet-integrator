package pl.gpcs.wordnet.integrator.exception;

public class EntityReadException extends Exception {

	public EntityReadException() {
		super();
	}

	public EntityReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public EntityReadException(String message, Throwable cause) {
		super(message, cause);
	}

	public EntityReadException(String message) {
		super(message);
	}

	public EntityReadException(Throwable cause) {
		super(cause);
	}

}
