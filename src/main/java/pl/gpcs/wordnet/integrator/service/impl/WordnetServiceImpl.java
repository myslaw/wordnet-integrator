package pl.gpcs.wordnet.integrator.service.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.Constants;
import pl.gpcs.wordnet.integrator.Constants.WordnetCreationInstants;
import pl.gpcs.wordnet.integrator.adapter.SynsetAdapter;
import pl.gpcs.wordnet.integrator.adapter.normalized.NormalizedAdapter;
import pl.gpcs.wordnet.integrator.adapter.plwordnet.PlWordnetSynsetAdapter;
import pl.gpcs.wordnet.integrator.adapter.polnet.PolnetSynsetAdapter;
import pl.gpcs.wordnet.integrator.corrector.SynsetCorrector;
import pl.gpcs.wordnet.integrator.corrector.impl.AcceptingCorrector;
import pl.gpcs.wordnet.integrator.corrector.impl.PolnetSynsetCorrector;
import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.exception.SynsetReadException;
import pl.gpcs.wordnet.integrator.filter.PlWordnetEnglishFilter;
import pl.gpcs.wordnet.integrator.filter.SynsetFilter;
import pl.gpcs.wordnet.integrator.io.SynsetFileReader;
import pl.gpcs.wordnet.integrator.io.SynsetReader;
import pl.gpcs.wordnet.integrator.joiner.impl.WordnetJoinerImpl;
import pl.gpcs.wordnet.integrator.model.common.Synset;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;
import pl.gpcs.wordnet.integrator.model.synset.normalized.NormalizedSynset;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.service.WordnetService;
import pl.gpcs.wordnet.integrator.util.JsonUtils;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.WordnetType;
import pl.gpcs.wordnet.integrator.validator.PlWordnetSynsetValidator;
import pl.gpcs.wordnet.integrator.validator.PolnetSynsetValidator;
import pl.gpcs.wordnet.integrator.validator.SynsetValidationResult;
import pl.gpcs.wordnet.integrator.validator.SynsetValidator;

@RequiredArgsConstructor
public class WordnetServiceImpl implements WordnetService {

	private static final Logger LOG = LoggerFactory.getLogger(WordnetServiceImpl.class);

	private final SynsetAdapter<PlWordnetSynset> plWordnetAdapter;
	private final SynsetAdapter<PolnetSynset> polnetAdapter;
	private final SynsetValidator<PlWordnetSynset> plWordnetSynsetValidator;
	private final SynsetValidator<PolnetSynset> polnetSynsetValidator;
	// TODO: add set containing marged elements so they won't be merged again

	public WordnetServiceImpl() {
		plWordnetAdapter = new PlWordnetSynsetAdapter();
		polnetAdapter = new PolnetSynsetAdapter();
		plWordnetSynsetValidator = new PlWordnetSynsetValidator();
		polnetSynsetValidator = new PolnetSynsetValidator();
	}

	@Override
	public Future<Wordnet> createWordnetAsync(Path path, WordnetType wordnetType) {
		return createWordnetAsync(path, wordnetType, wordnetType.toString());
	}

	@Override
	public Future<Wordnet> createWordnetAsync(Path path, WordnetType wordnetType, String name) {
		// FIXME handle name for polnet and plWordnet
		switch (wordnetType) {
		case PLWORDNET:
			return CompletableFuture.supplyAsync(() -> createWordnetForPlWordnet(path));
		case POLNET:
			return CompletableFuture.supplyAsync(() -> createWordnetForPolnet(path));
		case MERGED:
			return CompletableFuture.supplyAsync(() -> createWordnetForNormalized(path, name));
		default:
			throw new UnsupportedOperationException(wordnetType + " is not supported");
		}
	}

	@Override
	public Wordnet createWordnetForPlWordnet(Path path) {
		SynsetFilter<PlWordnetSynset> pojoFilter = WordnetCreationInstants.REMOVE_ENGLISH_SYNSETS
				? new PlWordnetEnglishFilter()
				: null;
		try (SynsetFileReader<PlWordnetSynset> reader = new SynsetFileReader<>(PlWordnetSynset.class, path, null,
				pojoFilter)) {
			Wordnet wordnet = createWordnetForPlWordnet(reader);
			removeUinitializedEntries(wordnet);
			return wordnet;
		} catch (IOException | InitializationException e) {
			throw new RuntimeException(e); // FIXME throw custom exception;
		}
	}

	@Override
	public Wordnet createWordnetForPlWordnet(SynsetReader<PlWordnetSynset> synsetReader) {
		return createWordnet(synsetReader, plWordnetSynsetValidator, plWordnetAdapter, new AcceptingCorrector<>(),
				PlWordnetSynset.class);
	}

	@Override
	public Wordnet createWordnetForPolnet(Path path) {
		try (SynsetFileReader<PolnetSynset> reader = new SynsetFileReader<>(PolnetSynset.class, path)) {
			return createWordnetForPolnet(reader);
		} catch (IOException | InitializationException e) {
			throw new RuntimeException(e); // FIXME throw custom exception;
		}
	}

	@Override
	public Wordnet createWordnetForPolnet(SynsetReader<PolnetSynset> synsetReader) {
		Wordnet wordnet = createWordnet(synsetReader, polnetSynsetValidator, polnetAdapter, new PolnetSynsetCorrector(),
				PolnetSynset.class);
		removeUinitializedEntries(wordnet);
		populateWithHyponyms(wordnet); // polnet has only hypernyms, hyponyms have to be artificially created
		return wordnet;
	}

	@Override
	public Wordnet createWordnetForNormalized(Path path, String name) {
		try (SynsetFileReader<NormalizedSynset> reader = new SynsetFileReader<>(NormalizedSynset.class, path, null,
				null)) {
			return createWordnetForNormalized(reader, name);
		} catch (IOException | InitializationException e) {
			throw new RuntimeException(e); // FIXME throw custom exception;
		}
	}

	@Override
	public Wordnet createWordnetForNormalized(SynsetReader<NormalizedSynset> synsetReader, String name) {
		return createWordnet(synsetReader, synset -> SynsetValidationResult.VALID, new NormalizedAdapter(),
				new AcceptingCorrector<>(), NormalizedSynset.class, name);
	}

	private void removeUinitializedEntries(Wordnet wordnet) {
		List<WordnetEntry> uninitializedEntries = new LinkedList<>();
		for (WordnetEntry wordnetEntry : wordnet) {
			if (!wordnetEntry.isInitialized()) {
				uninitializedEntries.add(wordnetEntry);
			}
		}
		wordnet.removeAll(uninitializedEntries);
	}

	private <T extends Synset<? extends SynsetIlr, ?>> Wordnet createWordnet(SynsetReader<T> reader,
			SynsetValidator<T> validator, SynsetAdapter<T> adapter, SynsetCorrector<T> corrector, Class<T> clazz) {
		return createWordnet(reader, validator, adapter, corrector, clazz,
				determineWordnetTypeBySynset(clazz).toString());
	}

	private static <T extends Synset<? extends SynsetIlr, ?>> WordnetType determineWordnetTypeBySynset(
			Class<T> synsetClazz) {
		if (synsetClazz == PlWordnetSynset.class) {
			return WordnetType.PLWORDNET;
		} else if (synsetClazz == PolnetSynset.class) {
			return WordnetType.POLNET;
		} else if (synsetClazz != NormalizedSynset.class) {
			return WordnetType.MERGED;
		} else {
			throw new UnsupportedOperationException(
					synsetClazz + " has not mapping for " + WordnetType.class.getSimpleName() + " yet.");
		}
	}

	private <T extends Synset<? extends SynsetIlr, ?>> Wordnet createWordnet(SynsetReader<T> reader,
			SynsetValidator<T> validator, SynsetAdapter<T> adapter, SynsetCorrector<T> corrector, Class<T> clazz,
			String name) {
		try {
			Wordnet wordnet = new Wordnet(name, determineWordnetTypeBySynsetClass(clazz));
			T synset;
			while ((synset = reader.read()) != null) {
				SynsetValidationResult validationResult = validator.validate(synset);
				if (validationResult != SynsetValidationResult.VALID) {
					logFailedValidation(synset, validationResult);
					continue;
				}
				if (corrector.correct(synset)) {
					wordnet.add(adapter.createWordnetEntry(synset));
				}
			}
			return wordnet;
		} catch (SynsetReadException e) {
			throw new RuntimeException("Reading " + clazz + " has failed.", e); // FIXME change to custom exception
		}
	}

	private static WordnetType determineWordnetTypeBySynsetClass(
			Class<? extends Synset<? extends SynsetIlr, ?>> synsetClass) {
		if (synsetClass == PolnetSynset.class) {
			return WordnetType.POLNET;
		} else if (synsetClass == PlWordnetSynset.class) {
			return WordnetType.PLWORDNET;
		} else if (synsetClass == NormalizedSynset.class) {
			return WordnetType.MERGED;
		} else {
			throw new IllegalArgumentException("Failed to determine WordnetType for " + synsetClass);
		}
	}

	private <T extends Synset<? extends SynsetIlr, ?>> void logFailedValidation(T synset,
			SynsetValidationResult validationResult) {
		LOG.warn("Synset with id: {} of {} failed to pass validation. The cause is: {}", synset.getId(),
				synset.getClass(), validationResult);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Synset failed to pass validation: {}", JsonUtils.pojoToJson(synset));
		}
	}

	private <T extends Synset<? extends SynsetIlr, ?>> void removeInvalidIlrs(T synset) {
		for (Iterator<? extends SynsetIlr> iterator = synset.getIlr().iterator(); iterator.hasNext();) {
			SynsetIlr ilr = iterator.next();
			if (ilr.getType() == null || ilr.getType().isBlank() || ilr.getText() == null || ilr.getText().isBlank()) {
				iterator.remove();
			}
		}
	}

	private void populateWithHyponyms(Wordnet wordnet) {
		Set<WordnetEntry> tempHypernyms;
		Set<WordnetEntry> tempHyponyms;
		for (WordnetEntry wordnetEntry : wordnet.getAllEntries()) {
			tempHypernyms = wordnetEntry.getHypernyms();
			if (tempHypernyms != null) {
				for (WordnetEntry hypernym : tempHypernyms) {
					tempHyponyms = hypernym.getHyponyms();
					if (tempHyponyms == null) {
						tempHyponyms = new HashSet<>();
						hypernym.setHyponyms(tempHyponyms);
					}
					tempHyponyms.add(wordnetEntry);
				}
			}
		}
	}

	@Override
	public Wordnet mergeWordnets(Wordnet targetWordnet, Wordnet sourceWordnet, MergingStrategy mergingStrategy,
			final int edgeThreshold) {
		new WordnetJoinerImpl(mergingStrategy, sourceWordnet, targetWordnet, Constants.Merge.MIN_SYNONYM_MATCHING_COUNT,
				edgeThreshold).join();
		return targetWordnet;
	}

	@Override
	public <T extends Synset<?, ?>> Collection<T> convertToSynsetCollection(Collection<WordnetEntry> wordnetEntries,
			Class<T> clazz) {
		if (clazz == PlWordnetSynset.class) {
			return (Collection<T>) wordnetEntries.stream().map(plWordnetAdapter::createSynset).toList();
		} else { // polnet
			return (Collection<T>) wordnetEntries.stream().map(polnetAdapter::createSynset).toList();
		}
	}

}
