package pl.gpcs.wordnet.integrator.service;

public enum MergingStrategy {
	HYPONYM_ORIGIN, HYPERNYM_ORIGIN;
}
