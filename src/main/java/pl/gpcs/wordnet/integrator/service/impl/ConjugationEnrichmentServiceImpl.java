package pl.gpcs.wordnet.integrator.service.impl;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import pl.gpcs.wordnet.integrator.exception.ConjugationReadException;
import pl.gpcs.wordnet.integrator.io.ConjugationReader;
import pl.gpcs.wordnet.integrator.model.verb.Conjugation;
import pl.gpcs.wordnet.integrator.service.ConjugationEnrichmentService;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public class ConjugationEnrichmentServiceImpl implements ConjugationEnrichmentService {

	@Override
	public void enrichWordnet(Wordnet wordnet, ConjugationReader conjugationReader) {
		Conjugation conjugation;
		Set<WordnetEntry> wordnetEntrySet;
		SynonymEntry baseSynonymEntry = null;
		try {
			while ((conjugation = conjugationReader.read()) != null) {
				wordnetEntrySet = wordnet.findBySynonym(conjugation.get());
				if (wordnetEntrySet != null) {
					for (WordnetEntry wordnetEntry : wordnetEntrySet) {
						for (SynonymEntry synonymEntry : wordnetEntry.getSynonyms()) {
							if (conjugation.get().equals(synonymEntry.getText())) {
								baseSynonymEntry = synonymEntry;
								break;
							}
						}
						wordnetEntry.getSynonyms().addAll(
								createConjugatedSynonymEntries(conjugation.getConjugations(), baseSynonymEntry));
					}
				}
			}
		} catch (ConjugationReadException e) {
			e.printStackTrace(); // FIXME
			throw new RuntimeException(e); // FIXME
		}
	}

	private Collection<? extends SynonymEntry> createConjugatedSynonymEntries(Set<String> conjugations,
			SynonymEntry baseSynonymEntry) {
		List<SynonymEntry> synonymEntryList = new LinkedList<>();
		for (String conjugatedString : conjugations) {
			synonymEntryList.add(new SynonymEntry(conjugatedString, baseSynonymEntry.getSense()));
		}
		return synonymEntryList;
	}

}
