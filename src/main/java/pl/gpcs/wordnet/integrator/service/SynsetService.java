package pl.gpcs.wordnet.integrator.service;

import java.util.Map;

import pl.gpcs.wordnet.integrator.io.SynsetFileReader;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;

public interface SynsetService {
	public Map<String, PlWordnetSynset> createIdToPlWordnetSynsetMap(SynsetFileReader<PlWordnetSynset> synsetReader);

	public Map<String, PolnetSynset> createIdToPolnetSynsetMap(SynsetFileReader<PlWordnetSynset> synsetReader);
}
