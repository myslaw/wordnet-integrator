package pl.gpcs.wordnet.integrator.service;

import pl.gpcs.wordnet.integrator.io.ConjugationReader;
import pl.gpcs.wordnet.integrator.util.Wordnet;

public interface ConjugationEnrichmentService {

	void enrichWordnet(Wordnet wordnet, ConjugationReader conjugationReader);

}
