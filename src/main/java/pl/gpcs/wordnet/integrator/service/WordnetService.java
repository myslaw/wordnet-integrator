package pl.gpcs.wordnet.integrator.service;

import java.nio.file.Path;
import java.util.Collection;
import java.util.concurrent.Future;

import pl.gpcs.wordnet.integrator.io.SynsetReader;
import pl.gpcs.wordnet.integrator.model.common.Synset;
import pl.gpcs.wordnet.integrator.model.synset.normalized.NormalizedSynset;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.WordnetType;

public interface WordnetService {
	Wordnet createWordnetForPlWordnet(SynsetReader<PlWordnetSynset> synsetReader);

	Wordnet createWordnetForPolnet(SynsetReader<PolnetSynset> synsetReader);

	Wordnet createWordnetForNormalized(SynsetReader<NormalizedSynset> synsetReader, String name);

	Wordnet mergeWordnets(Wordnet targetWordnet, Wordnet sourceWordnet, MergingStrategy mergingStrategy,
			int edgeThreshold);

	<T extends Synset<?, ?>> Collection<T> convertToSynsetCollection(Collection<WordnetEntry> wordnetEntries,
			Class<T> clazz);

	Wordnet createWordnetForPlWordnet(Path path);

	Wordnet createWordnetForPolnet(Path path);

	Future<Wordnet> createWordnetAsync(Path path, WordnetType wordnetType, String name);

	Future<Wordnet> createWordnetAsync(Path path, WordnetType wordnetType);

	Wordnet createWordnetForNormalized(Path path, String name);


}
