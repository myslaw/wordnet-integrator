package pl.gpcs.wordnet.integrator.service.impl;

import java.util.HashMap;
import java.util.Map;

import pl.gpcs.wordnet.integrator.exception.SynsetReadException;
import pl.gpcs.wordnet.integrator.io.SynsetFileReader;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.service.SynsetService;

public class SynsetServiceImpl implements SynsetService {

	private final static String ENGLIH_POS_MARK = "_pwn";

	@Override
	public Map<String, PlWordnetSynset> createIdToPlWordnetSynsetMap(SynsetFileReader<PlWordnetSynset> synsetReader) {
		HashMap<String, PlWordnetSynset> idToPlWordnetSynsetMap = new HashMap<>();
		PlWordnetSynset synset;
		try {
			while ((synset = synsetReader.read()) != null) {
				if (!synset.getPos().contains(ENGLIH_POS_MARK)) { // consider only polish words

				}
			}
		} catch (SynsetReadException e) {
			e.printStackTrace();
		}

		return idToPlWordnetSynsetMap;
	}

	@Override
	public Map<String, PolnetSynset> createIdToPolnetSynsetMap(SynsetFileReader<PlWordnetSynset> synsetReader) {
		return null;
	}

}
