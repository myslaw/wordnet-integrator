package pl.gpcs.wordnet.integrator.entangler;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.crawler.WordnetCrawler;
import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.impl.MapSynonymNavigableAdapter;

public class WordnetEntryBranchEntangler extends AbstractWordnetEntryEntangler implements WordnetEntryEntangler {

	private final Collection<Fit> originFits;
	private final MergingStrategy mergingStrategy;
	private final Function<WordnetEntry, Set<WordnetEntry>> traverseFunction;

	public WordnetEntryBranchEntangler(Collection<Fit> originFits, MergingStrategy mergingStrategy) {
		this.originFits = originFits;
		this.mergingStrategy = mergingStrategy;
		this.traverseFunction = WordnetCrawler.traverseFunction(mergingStrategy);
	}

	@Override
	public Map<WordnetEntry, Fit> entangle() {
		if (targetEntryToFitsQueue != null) {
			return entangledSourceEntryToFit;
		}
		final Set<WordnetEntry> tabooSourceEntries = originFits.stream().map(Fit::getSource)
				.collect(Collectors.toSet());
		Map<WordnetEntry, PriorityQueue<Fit>> map = originFits.parallelStream().unordered()
				.map(this::fitToBranchFitsQueues).filter(Objects::nonNull).flatMap(Collection::stream)
				.flatMap(Queue::stream).distinct().filter(fit -> !tabooSourceEntries.contains(fit.getSource()))
				.collect(Collectors.groupingBy(Fit::getTarget,
						Collectors.toCollection(AbstractWordnetEntryEntangler::createDescedingSimilarityFitsQueue)));
		// is Collectors.groupingByConcurrent safe to use without thread safe
		// collection?
		targetEntryToFitsQueue = new ConcurrentHashMap<>(map);

		for (WordnetEntry targetWordnetEntry : targetEntryToFitsQueue.keySet()) {
			entangleTarget(targetWordnetEntry);
		}
		return entangledSourceEntryToFit;
	}

	private Collection<Queue<Fit>> fitToBranchFitsQueues(Fit fit) {
		Set<WordnetEntry> sourceBranch = traverseFunction.apply(fit.getSource());
		if (sourceBranch.isEmpty()) {
			return null;
		}
		Set<WordnetEntry> targetBranch = traverseFunction.apply(fit.getTarget());
		if (targetBranch.isEmpty()) {
			return null;
		}
		return initializeFitsQueues(targetBranch, MapSynonymNavigableAdapter.of(sourceBranch), mergingStrategy)
				.values();
	}

}
