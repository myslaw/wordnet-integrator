package pl.gpcs.wordnet.integrator.entangler;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.matcher.WordnetMatcher;
import pl.gpcs.wordnet.integrator.matcher.impl.WordnetMatcherImpl;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.SynonymNavigable;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractWordnetEntryEntangler implements WordnetEntryEntangler {

	private static final int DEFAULT_FITS_QUEUE_SIZE = 11;

	protected static final WordnetMatcher WORDNET_MATCHER = new WordnetMatcherImpl();

	protected final Map<WordnetEntry, Fit> entangledSourceEntryToFit = new HashMap<>();
	protected volatile ConcurrentMap<WordnetEntry, Queue<Fit>> targetEntryToFitsQueue;
	
	protected static PriorityQueue<Fit> createDescedingSimilarityFitsQueue() {
		return createDescedingSimilarityFitsQueue(DEFAULT_FITS_QUEUE_SIZE);
	}

	protected static PriorityQueue<Fit> createDescedingSimilarityFitsQueue(final int size){
		return new PriorityQueue<>(size, Comparator.reverseOrder());
	}

	protected static ConcurrentMap<WordnetEntry, Queue<Fit>> initializeFitsQueues(
			Set<WordnetEntry> targetWordnetEntries, SynonymNavigable sourceSynonymNavigable,
			MergingStrategy mergingStrategy) {
		ConcurrentMap<WordnetEntry, Queue<Fit>> targetEntryToFitsQueue = new ConcurrentHashMap<>();
		targetWordnetEntries.parallelStream().forEach(we -> {
			List<Fit> fits = WORDNET_MATCHER.findFitsByTargetEntry(we, sourceSynonymNavigable, mergingStrategy);
			if (!fits.isEmpty()) {
				PriorityQueue<Fit> fitsQueue = createDescedingSimilarityFitsQueue(fits.size());
				fitsQueue.addAll(fits);
				targetEntryToFitsQueue.put(we, fitsQueue);
			}
		});
		return targetEntryToFitsQueue;
	}

	protected void entangleTarget(WordnetEntry targetWordnetEntry) {
		Queue<Fit> fits = targetEntryToFitsQueue.get(targetWordnetEntry);
		Fit currentFit;
		while ((currentFit = fits.poll()) != null) {
			Fit oldFit = entangledSourceEntryToFit.get(currentFit.getSource());
			boolean currentSucceed = settleFits(currentFit, oldFit, entangledSourceEntryToFit);
			if (currentSucceed) {
				break;
			}
		}
	}

	protected boolean settleFits(Fit currentFit, Fit oldFit, Map<WordnetEntry, Fit> wordnetEntryToFits) {
		if (oldFit == null) {
			wordnetEntryToFits.put(currentFit.getSource(), currentFit);
			return true;
		}
		if (currentFit.compareTo(oldFit) > 0) {
			wordnetEntryToFits.put(currentFit.getSource(), currentFit);
			entangleTarget(oldFit.getTarget());
			return true;
		}
		return false;
	}
}
