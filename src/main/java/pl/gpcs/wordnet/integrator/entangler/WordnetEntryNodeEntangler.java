package pl.gpcs.wordnet.integrator.entangler;

import java.util.Map;
import java.util.Set;

import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.SynonymNavigable;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

@RequiredArgsConstructor
public class WordnetEntryNodeEntangler extends AbstractWordnetEntryEntangler implements WordnetEntryEntangler {

	private final MergingStrategy mergingStrategy;
	private final Set<WordnetEntry> targetWordnetEntries;
	private final SynonymNavigable sourceSynonymNavigable;

	@Override
	public Map<WordnetEntry, Fit> entangle() {
		if (targetEntryToFitsQueue != null) {
			return entangledSourceEntryToFit;
		}
		targetEntryToFitsQueue = initializeFitsQueues(targetWordnetEntries, sourceSynonymNavigable, mergingStrategy);
		for (WordnetEntry targetWordnetEntry : targetEntryToFitsQueue.keySet()) {
			entangleTarget(targetWordnetEntry);
		}
		return entangledSourceEntryToFit;
	}
}
