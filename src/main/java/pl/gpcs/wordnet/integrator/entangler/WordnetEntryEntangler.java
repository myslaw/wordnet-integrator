package pl.gpcs.wordnet.integrator.entangler;

import java.util.Map;

import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public interface WordnetEntryEntangler {

	Map<WordnetEntry, Fit> entangle();

}
