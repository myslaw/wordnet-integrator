package pl.gpcs.wordnet.integrator.adapter.plwordnet;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import pl.gpcs.wordnet.integrator.adapter.SynsetAdapter;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Literal;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Synonym;
import pl.gpcs.wordnet.integrator.util.GeneralRelation;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public class PlWordnetSynsetAdapter implements SynsetAdapter<PlWordnetSynset> {

	private static final String HYPERNYM_MAIN_NAME_PL = "hypernym";
	private static final String HYPONYM_MAIN_NAME_PL = "hiponimia";
	private static final Set<String> HYPERNYM_NAMES_ENG = Set.of("Hypernym", "Instance_Hypernym");
	private static final Set<String> HYPERNYM_NAMES_PL = Set.of(HYPERNYM_MAIN_NAME_PL, "Hiper_PWN-plWN",
			"Hiper_plWN-PWN");
	private static final Set<String> HYPONYM_NAMES_ENG = Set.of("Hyponym", "Instance_Hyponym");
	private static final Set<String> HYPONYM_NAMES_PL = Set.of(HYPONYM_MAIN_NAME_PL, "Hipo_plWN-PWN", "Hipo_PWN-plWN");
	private static final String DEFAULT_SYNSET_STRING = "";

	@Override
	public WordnetEntry createWordnetEntry(PlWordnetSynset synset) {
		WordnetEntry wordnetEntry = new WordnetEntry(synset.getId());
		wordnetEntry.setPos(determinePos(synset.getPos()));
		wordnetEntry.setSynonyms(createSynonymEntrySet(synset.getSynonym().getLiteral()));
		createSemanticRelations(wordnetEntry, synset.getIlr());
		return wordnetEntry;
	}

	@Override
	public PlWordnetSynset createSynset(WordnetEntry entry) {
		PlWordnetSynset synset = new PlWordnetSynset();
		synset.setId(entry.getId());
		synset.setPos(extractPos(entry.getId()));
		synset.setSynonym(createSynonym(entry.getSynonyms()));
		synset.setIlr(createIlrs(entry.getHypernyms(), entry.getHyponyms(), entry.getGeneralSemanticRelations()));
		synset.setDef(DEFAULT_SYNSET_STRING);
		synset.setUsage(DEFAULT_SYNSET_STRING);
		synset.setDomain(DEFAULT_SYNSET_STRING);
		return synset;
	}

	private List<Ilr> createIlrs(Set<WordnetEntry> hypernyms, Set<WordnetEntry> hyponyms,
			Set<GeneralRelation<String>> generalSemanticRelations) {
		List<Ilr> ilrs = new LinkedList<>();
		if (hypernyms != null) {
			for (WordnetEntry hypernym : hypernyms) {
				ilrs.add(createIlr(hypernym.getId(), HYPERNYM_MAIN_NAME_PL));
			}
		}
		if (hyponyms != null) {
			for (WordnetEntry hyponym : hyponyms) {
				ilrs.add(createIlr(hyponym.getId(), HYPONYM_MAIN_NAME_PL));
			}
		}
		if (generalSemanticRelations != null) {
			for (GeneralRelation<String> generalSemanticRelation : generalSemanticRelations) {
				ilrs.add(createIlr(generalSemanticRelation.getId(), generalSemanticRelation.getRelation()));
			}
		}
		return ilrs;
	}

	private Ilr createIlr(String id, String type) {
		Ilr ilr = new Ilr();
		ilr.setText(id);
		ilr.setType(type);
		return ilr;
	}

	private Synonym createSynonym(Set<SynonymEntry> synonyms) {
		List<Literal> literals = new LinkedList<>();
		for (SynonymEntry synonymEntry : synonyms) {
			Literal literal = new Literal();
			literal.setSense(Integer.parseInt(synonymEntry.getSense()));
			literal.setText(synonymEntry.getText());
			literals.add(literal);
		}
		Synonym synonym = new Synonym();
		synonym.setLiteral(literals);
		return synonym;
	}

	private String extractPos(String id) {
		int posIdx = id.lastIndexOf("-") + 1;
		return id.substring(posIdx);
	}

	private PartOfSpeech determinePos(String pos) {
		if (pos == null || pos.isEmpty()) {
			throw new IllegalArgumentException("Empty part of speech field");
		}
		switch (pos) {
		case "a", "a_pwn":
			return PartOfSpeech.ADVERB;
		case "j", "j_pwn":
			return PartOfSpeech.ADJECTIVE;
		case "v", "v_pwn":
			return PartOfSpeech.VERB;
		case "n", "n_pwn":
			return PartOfSpeech.NOUN;
		}
		throw new IllegalArgumentException("Unknown part of speech: " + pos);
	}

	private void createSemanticRelations(WordnetEntry wordnetEntry, Collection<Ilr> ilrCollection) {
		if (ilrCollection == null || ilrCollection.isEmpty()) {
			return;
		}
		Set<Ilr> hyponyms = new HashSet<>();
		Set<Ilr> hypernyms = new HashSet<>();
		Set<Ilr> others = new HashSet<>();
		for (Ilr ilr : ilrCollection) {
			if (HYPONYM_NAMES_PL.contains(ilr.getType())) {
				hyponyms.add(ilr);
			} else if (HYPERNYM_NAMES_PL.contains(ilr.getType())) {
				hypernyms.add(ilr);
			} else {
				others.add(ilr);
			}
		}
		wordnetEntry.setHyponyms(createWordnetEntrySet(hyponyms));
		wordnetEntry.setHypernyms(createWordnetEntrySet(hypernyms));
		wordnetEntry.setGeneralSemanticRelations(createGeneralWordnetEntrySet(others));
	}

	private Set<GeneralRelation<String>> createGeneralWordnetEntrySet(Set<Ilr> ilrSet) {
		Set<GeneralRelation<String>> relationSet = new HashSet<>();
		WordnetEntry wordnetEntry;
		for (Ilr ilr : ilrSet) {
			wordnetEntry = new WordnetEntry(ilr.getText());
			wordnetEntry.setInitialized(false);
			relationSet.add(new GeneralRelation<>(wordnetEntry, ilr.getType()));
		}
		return relationSet;
	}

	private Set<WordnetEntry> createWordnetEntrySet(Set<Ilr> ilrSet) {
		Set<WordnetEntry> wordnetSet = new HashSet<>();
		WordnetEntry wordnetEntry;
		for (Ilr ilr : ilrSet) {
			wordnetEntry = new WordnetEntry(ilr.getText());
			wordnetEntry.setInitialized(false); // object is created, but it contains no real data (only id)
			wordnetSet.add(wordnetEntry);
		}
		return wordnetSet;
	}

	private Set<SynonymEntry> createSynonymEntrySet(List<Literal> literalList) {
		HashSet<SynonymEntry> synonymEntrySet = new HashSet<>();
		for (Literal literal : literalList) {
			synonymEntrySet.add(new SynonymEntry(literal.getText(), literal.getSense().toString()));
		}
		return synonymEntrySet;
	}

}
