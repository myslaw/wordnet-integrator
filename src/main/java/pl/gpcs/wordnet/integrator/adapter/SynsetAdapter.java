package pl.gpcs.wordnet.integrator.adapter;

import pl.gpcs.wordnet.integrator.model.common.SynonymLiteral;
import pl.gpcs.wordnet.integrator.model.common.Synset;
import pl.gpcs.wordnet.integrator.model.common.SynsetIlr;
import pl.gpcs.wordnet.integrator.model.common.SynsetSynonym;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public interface SynsetAdapter<T extends Synset<? extends SynsetIlr, ? extends SynsetSynonym<? extends SynonymLiteral<?>>>> {
	WordnetEntry createWordnetEntry(T synset);

	T createSynset(WordnetEntry entry);

}
