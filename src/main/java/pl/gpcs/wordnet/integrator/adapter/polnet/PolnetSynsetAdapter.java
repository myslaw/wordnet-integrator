package pl.gpcs.wordnet.integrator.adapter.polnet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.gpcs.wordnet.integrator.adapter.SynsetAdapter;
import pl.gpcs.wordnet.integrator.exception.CorruptedSynsetException;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Literal;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.util.GeneralRelation;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

public class PolnetSynsetAdapter implements SynsetAdapter<PolnetSynset> {

	private static final Logger LOG = LoggerFactory.getLogger(PolnetSynsetAdapter.class);

	private static final Pattern NATURAL_NUMBER_PATTERN = Pattern.compile("[1-9][0-9]*");
	private static final float ALTERNATIVE_VERSION_BASIC_RESOLUTION = 0.01f;
	private static final float ALTERNATIVE_VERSION_SHIFT = 0.01f;

	private static final String HYPERNYM = "hypernym";

	@Override
	public WordnetEntry createWordnetEntry(PolnetSynset synset) {
		if (synset.getSynonym().getLiteral() == null) {
			throw new CorruptedSynsetException("Synset with ID: " + synset.getId() + " has no synonyms");
		}
		WordnetEntry wordnetEntry = new WordnetEntry(synset.getId());
		wordnetEntry.setPos(determinePos(synset.getPos()));
		wordnetEntry.setSynonyms(createSynonymEntrySet(synset.getSynonym().getLiteral()));
		createSemanticRelations(wordnetEntry, synset.getIlr());
		return wordnetEntry;
	}

	@Override
	public PolnetSynset createSynset(WordnetEntry entry) {
		throw new UnsupportedOperationException(
				"Method createSynset is not implemented for " + PolnetSynsetAdapter.class);
	}

//	public PolnetSynset createSynset(WordnetEntry entry) {
//		PolnetSynset synset = new PolnetSynset();
//		synset.setId(entry.getId());
//		synset.setPos(determinePos(entry.getPos()));
//		synset.setSynonym(null);
//		return synset;
//	}
//
//	/**
//	 * For Wordnet to Synset convertion
//	 * 
//	 * @param pos
//	 * @return
//	 */
//	private String determinePos(PartOfSpeech pos) {
//		switch (pos) {
//		case ADJECTIVE:
//			return "a";
//		case ADVERB:
//			return "v";
//		case NOUN:
//			return "n";
//		case VERB:
//			return "b";
//		}
//		throw new IllegalArgumentException("Unknown part of speech: " + pos);
//	}

	/**
	 * For Synset to Wordnet conversion
	 * 
	 * @param pos
	 * @return
	 */
	private PartOfSpeech determinePos(String pos) {
		if (pos == null || pos.isEmpty()) {
			throw new IllegalArgumentException("Empty part of speech field");
		}
		switch (pos) {
		case "a":
			return PartOfSpeech.ADJECTIVE;
		case "v":
			return PartOfSpeech.VERB;
		case "n":
			return PartOfSpeech.NOUN;
		case "b":
			return PartOfSpeech.ADVERB;
		}
		throw new IllegalArgumentException("Unknown part of speech: " + pos);
	}

	private void createSemanticRelations(WordnetEntry wordnetEntry, List<Ilr> ilrCollection) {
		if (ilrCollection == null || ilrCollection.isEmpty()) {
			return;
		}
//		Set<Ilr> hyponyms = new HashSet<>(); //Polnet has no hyponyms
		Set<Ilr> hypernyms = new HashSet<>();
		Set<Ilr> others = new HashSet<>();
		for (Ilr ilr : ilrCollection) {
			if (HYPERNYM.equals(ilr.getType())) {
				hypernyms.add(ilr);
			} else {
				others.add(ilr);
			}
		}
		wordnetEntry.setHypernyms(createWordnetEntrySet(hypernyms));
		wordnetEntry.setGeneralSemanticRelations(createGeneralWordnetEntrySet(others));
	}

	private Set<GeneralRelation<String>> createGeneralWordnetEntrySet(Set<Ilr> ilrSet) {
		Set<GeneralRelation<String>> relationSet = new HashSet<>();
		WordnetEntry wordnetEntry;
		for (Ilr ilr : ilrSet) {
			if (ilr.getType() != null) {
				wordnetEntry = new WordnetEntry(ilr.getText());
				wordnetEntry.setInitialized(false);
				relationSet.add(new GeneralRelation<>(wordnetEntry, ilr.getType()));
			} else {
				System.err.println("Could not create relation " + WordnetEntry.class.getName() + " because "
						+ Ilr.class.getName() + " type is null");
			}
		}
		return relationSet;
	}

	private Set<WordnetEntry> createWordnetEntrySet(Set<Ilr> ilrSet) {
		Set<WordnetEntry> wordnetSet = new HashSet<>();
		WordnetEntry wordnetEntry;
		for (Ilr ilr : ilrSet) {
			wordnetEntry = new WordnetEntry(ilr.getText());
			wordnetEntry.setInitialized(false);
			wordnetSet.add(wordnetEntry);
		}
		return wordnetSet;
	}

	private Set<SynonymEntry> createSynonymEntrySet(List<Literal> literalList) {
		HashSet<SynonymEntry> synonymEntrySet = new HashSet<>();
		for (Literal literal : literalList) {
			synonymEntrySet.add(new SynonymEntry(literal.getText(), literal.getSense()));
		}
		return synonymEntrySet;
	}

	private float calcSense(String sense) {
		if (sense == null || sense.isEmpty()) {
			return 0;
		}
		Matcher matcher = NATURAL_NUMBER_PATTERN.matcher(sense);
		if (matcher.matches()) {
			return Float.parseFloat(sense);
		}
		matcher.find();
		float senseVal = Float.parseFloat(sense.substring(0, matcher.end()));
		String senseAlphaPart = sense.substring(matcher.end());
		float resolution = ALTERNATIVE_VERSION_BASIC_RESOLUTION;
		for (int i = 0; i < senseAlphaPart.length(); ++i) {
			senseVal += (senseAlphaPart.charAt(i) - 'a') * resolution;
			resolution *= ALTERNATIVE_VERSION_SHIFT; // shift by two digits to the right
		}
		return senseVal;
	}

}
