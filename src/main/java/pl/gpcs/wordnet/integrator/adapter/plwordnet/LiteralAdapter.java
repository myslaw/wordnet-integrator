package pl.gpcs.wordnet.integrator.adapter.plwordnet;

import java.util.LinkedList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlMixed;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Literal;

public class LiteralAdapter extends XmlAdapter<LiteralAdapter.AdaptedLiteral, Literal> {

	@Override
	public Literal unmarshal(LiteralAdapter.AdaptedLiteral v) throws Exception {
		Literal literal = new Literal();
		literal.setText(v.text.get(0));
		literal.setSense(v.sense);
		return literal;
	}

	@Override
	public LiteralAdapter.AdaptedLiteral marshal(Literal v) throws Exception {
		AdaptedLiteral adaptedLiteral = new AdaptedLiteral();
		adaptedLiteral.text = new LinkedList<>();
		adaptedLiteral.text.add(v.getText());
		adaptedLiteral.sense = v.getSense();
		return adaptedLiteral;
	}

	public static class AdaptedLiteral {
		@XmlMixed
		public List<String> text;
		@XmlElement(name = "SENSE")
		public Integer sense;
	}
}
