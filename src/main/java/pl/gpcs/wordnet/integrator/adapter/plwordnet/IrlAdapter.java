package pl.gpcs.wordnet.integrator.adapter.plwordnet;

import java.util.LinkedList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlMixed;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Ilr;

public class IrlAdapter extends XmlAdapter<IrlAdapter.AdaptedIrl, Ilr> {

	@Override
	public Ilr unmarshal(AdaptedIrl v) throws Exception {
		Ilr irl = new Ilr();
		irl.setText(v.text.get(0));
		irl.setType(v.type);
		return irl;
	}

	@Override
	public AdaptedIrl marshal(Ilr v) throws Exception {
		AdaptedIrl adaptedIrl = new AdaptedIrl();
		adaptedIrl.text = new LinkedList<>();
		adaptedIrl.text.add(v.getText());
		adaptedIrl.type = v.getType();
		return adaptedIrl;
	}

	public static class AdaptedIrl {
		@XmlMixed
		public List<String> text;
		@XmlElement(name = "TYPE")
		public String type;
	}

}
