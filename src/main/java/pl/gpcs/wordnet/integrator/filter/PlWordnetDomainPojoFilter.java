package pl.gpcs.wordnet.integrator.filter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.gpcs.wordnet.integrator.model.synset.plwordnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;

public class PlWordnetDomainPojoFilter implements SynsetFilter<PlWordnetSynset> {

	private final String domain;
	private final Set<String> ids = new HashSet<>();

	public PlWordnetDomainPojoFilter(String domain) {
		this.domain = domain;
	}

	@Override
	public boolean filter(PlWordnetSynset synset) {
		if (ids.contains(synset.getId())) {
			expandIds(synset.getIlr());
			return true;
		}
		if (domain.equals(synset.getDomain())) {
			expandIds(synset.getIlr());
			return true;
		}
		return false;
	}

	private void expandIds(List<Ilr> ilrList) {
		if (ilrList != null) {
			ilrList.stream().forEach(ilr -> ids.add(ilr.getText()));
		}

	}

}
