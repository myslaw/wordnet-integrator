package pl.gpcs.wordnet.integrator.filter;

import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;

public class PlWordnetEnglishFilter implements SynsetFilter<PlWordnetSynset> {

	private static final String ENGLISH_ID_SUFFIX = "_pwn";

	@Override
	public boolean filter(PlWordnetSynset synset) {
		return !synset.getId().endsWith(ENGLISH_ID_SUFFIX);
	}


}
