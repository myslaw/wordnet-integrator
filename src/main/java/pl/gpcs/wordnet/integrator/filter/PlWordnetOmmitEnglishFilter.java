package pl.gpcs.wordnet.integrator.filter;

import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;

public class PlWordnetOmmitEnglishFilter implements SynsetFilter<PlWordnetSynset> {

	private static final String ENGLISH_ID_SUFFIX = "_pwn";

	@Override
	public boolean filter(PlWordnetSynset synset) {
		if (synset.getId().endsWith(ENGLISH_ID_SUFFIX)) {
			return false;
		}
		// remove english references
		if (synset.getIlr() != null) {
			synset.getIlr().removeIf(ilr -> ilr.getText().endsWith(ENGLISH_ID_SUFFIX));
		}

		return true;
	}

}
