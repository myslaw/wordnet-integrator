package pl.gpcs.wordnet.integrator.filter;

public interface SynsetFilter<T> {
	boolean filter(T synset);
}