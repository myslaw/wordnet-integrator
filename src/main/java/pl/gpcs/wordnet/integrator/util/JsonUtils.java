package pl.gpcs.wordnet.integrator.util;

import java.util.TimeZone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;

public class JsonUtils {

	private static final JsonMapper JSON_MAPPER = JsonMapper.builder().defaultTimeZone(TimeZone.getDefault())
			.findAndAddModules().build();

	public static String pojoToJson(Object object) {
		try {
			return JSON_MAPPER.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Failed to serialize pojo to json", e); // FIXME throw custom exception
		}
	}
}
