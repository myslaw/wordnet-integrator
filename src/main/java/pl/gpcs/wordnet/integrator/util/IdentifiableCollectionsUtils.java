package pl.gpcs.wordnet.integrator.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * In case of comparing mutually dependent objects regular hashCode & equals
 * results in StackOverflowError
 * 
 * @author przem
 *
 */
public class IdentifiableCollectionsUtils {

	public static <T> int compare(Collection<Identifiable<T>> o1, Collection<Identifiable<T>> o2) {
		// TODO: implement
		return 0;
	}

	public static <T> boolean equals(Collection<? extends Identifiable<T>> o1,
			Collection<? extends Identifiable<T>> o2) {
		if (o1 == o2) {
			return true;
		}
		if (!nullAndSizeCheck(o1, o2)) {
			return false;
		}
		List<T> o1IdList = o1.stream().map(i -> i.getId()).collect(Collectors.toCollection(LinkedList::new));
		List<T> o2IdList = o2.stream().map(i -> i.getId()).collect(Collectors.toCollection(LinkedList::new));
		return o1IdList.equals(o2IdList);
	}

	public static <T> boolean equalsSet(Set<? extends Identifiable<T>> o1, Set<? extends Identifiable<T>> o2) {
		if (o1 == o2) {
			return true;
		}
		if (!nullAndSizeCheck(o1, o2)) {
			return false;
		}
		Set<T> o1IdSet = o1.stream().map(i -> i.getId()).collect(Collectors.toSet());
		Set<T> o2IdSet = o2.stream().map(i -> i.getId()).collect(Collectors.toSet());
		return o1IdSet.equals(o2IdSet);
	}

	public static <T> int hashCodeSet(Set<? extends Identifiable<T>> o1) {
		return Objects.hash(o1.stream().map(i -> i.getId()).collect(Collectors.toCollection(LinkedList::new)));
	}

	private static <T> boolean nullAndSizeCheck(Collection<? extends Identifiable<T>> c1,
			Collection<? extends Identifiable<T>> c2) {
		if (c1 == null) {
			if (c2 != null) {
				return false;
			}
		}
		if (c1.size() != c2.size()) {
			return false;
		}
		return true;
	}

}
