package pl.gpcs.wordnet.integrator.util;

public class GeneralRelation<T> implements Identifiable<String> {

	private WordnetEntry wordnetEntry;
	private final T relation;

	public GeneralRelation(WordnetEntry wordnetEntry, T relation) {
		this.wordnetEntry = wordnetEntry;
		this.relation = relation;
	}

	public WordnetEntry getWordnetEntry() {
		return wordnetEntry;
	}

	public void setWordnetEntry(WordnetEntry wordnetEntry) {
		this.wordnetEntry = wordnetEntry;
	}

	public T getRelation() {
		return relation;
	}

	@Override
	public String getId() {
		return wordnetEntry.getId();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((relation == null) ? 0 : relation.hashCode());
		result = prime * result + ((wordnetEntry == null) ? 0 : wordnetEntry.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneralRelation other = (GeneralRelation) obj;
		if (relation == null) {
			if (other.relation != null)
				return false;
		} else if (!relation.equals(other.relation))
			return false;
		if (wordnetEntry == null) {
			if (other.wordnetEntry != null)
				return false;
		} else if (!wordnetEntry.equals(other.wordnetEntry))
			return false;
		return true;
	}

}
