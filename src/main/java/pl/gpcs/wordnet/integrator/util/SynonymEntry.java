package pl.gpcs.wordnet.integrator.util;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, doNotUseGetters = true)
@ToString
@Getter
public class SynonymEntry {

	@EqualsAndHashCode.Include
	private final String text;
	@EqualsAndHashCode.Include
	private final String sense;

}
