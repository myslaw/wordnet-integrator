package pl.gpcs.wordnet.integrator.util;

public interface Identifiable<T> {

	public T getId();
}
