package pl.gpcs.wordnet.integrator.util;

public class RelationPair<P, C> {
	private final P referencing;
	private final C referenced;

	public RelationPair(P referencing, C referenced) {
		this.referencing = referencing;
		this.referenced = referenced;
	}

	public P getReferencing() {
		return referencing;
	}

	public C getReferenced() {
		return referenced;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((referenced == null) ? 0 : referenced.hashCode());
		result = prime * result + ((referencing == null) ? 0 : referencing.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelationPair other = (RelationPair) obj;
		if (referenced == null) {
			if (other.referenced != null)
				return false;
		} else if (!referenced.equals(other.referenced))
			return false;
		if (referencing == null) {
			if (other.referencing != null)
				return false;
		} else if (!referencing.equals(other.referencing))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RelationPair [referencing=" + referencing + ", referenced=" + referenced + "]";
	}

}
