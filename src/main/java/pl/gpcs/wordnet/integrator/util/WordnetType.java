package pl.gpcs.wordnet.integrator.util;

public enum WordnetType {
	PLWORDNET, POLNET,
	MERGED;
}
