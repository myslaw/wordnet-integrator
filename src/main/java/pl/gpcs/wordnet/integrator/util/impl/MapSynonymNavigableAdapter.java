package pl.gpcs.wordnet.integrator.util.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.SynonymNavigable;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class MapSynonymNavigableAdapter implements SynonymNavigable {

	private final Map<String, Set<WordnetEntry>> synonymToWordnetEntries;

	public MapSynonymNavigableAdapter(Collection<WordnetEntry> wordnetEntries) {
		final Map<String, Set<WordnetEntry>> map = new HashMap<>();
		for (WordnetEntry wordnetEntry : wordnetEntries) {
			for (SynonymEntry synonymEntry : wordnetEntry.getSynonyms()) {
				map.computeIfAbsent(synonymEntry.getText(), key -> new HashSet<>()).add(wordnetEntry);
			}
		}
		synonymToWordnetEntries = map;
	}

	@Override
	public Set<WordnetEntry> findBySynonym(String synonym) {
		return synonymToWordnetEntries.get(synonym);
	}

	public static SynonymNavigable of(Set<WordnetEntry> wordnetEntries) {
		return new MapSynonymNavigableAdapter(wordnetEntries);
	}

}
