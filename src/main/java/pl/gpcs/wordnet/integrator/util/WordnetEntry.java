package pl.gpcs.wordnet.integrator.util;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, doNotUseGetters = true)
public class WordnetEntry implements Identifiable<String>, Cloneable {
	@EqualsAndHashCode.Include
	private final String id;
	private Set<SynonymEntry> synonyms = new HashSet<>(4);
	private Set<WordnetEntry> hypernyms;// = new HashSet<>(0);
	private Set<WordnetEntry> hyponyms;// = new HashSet<>(0);
	private Set<GeneralRelation<String>> generalSemanticRelations;
	private PartOfSpeech pos;
	private boolean initialized = false;

	public WordnetEntry(String id) {
		this.id = id;
	}

	public WordnetEntry(WordnetEntry wordnetEntry) {
		this.id = wordnetEntry.id;
		this.synonyms = new HashSet<>(wordnetEntry.synonyms);
		this.hypernyms = new HashSet<>(wordnetEntry.hypernyms);
		this.hyponyms = new HashSet<>(wordnetEntry.hyponyms);
		this.generalSemanticRelations = wordnetEntry.generalSemanticRelations.stream()
				.map(gr -> new GeneralRelation<>(gr.getWordnetEntry(), gr.getRelation()))
				.collect(Collectors.toCollection(HashSet::new));
		this.pos = wordnetEntry.pos;
		this.initialized = wordnetEntry.initialized;
	}

	@Override
	public String getId() {
		return id;
	}

	public Set<SynonymEntry> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(Set<SynonymEntry> synonyms) {
		this.synonyms = synonyms;
	}

	public Set<WordnetEntry> getHypernyms() {
		return hypernyms;
	}

	public void setHypernyms(Set<WordnetEntry> hypernyms) {
		this.hypernyms = hypernyms;
	}

	public Set<WordnetEntry> getHyponyms() {
		return hyponyms;
	}

	public void setHyponyms(Set<WordnetEntry> hyponyms) {
		this.hyponyms = hyponyms;
	}

	public Set<GeneralRelation<String>> getGeneralSemanticRelations() {
		return generalSemanticRelations;
	}

	public void setGeneralSemanticRelations(Set<GeneralRelation<String>> generalSemanticRelations) {
		this.generalSemanticRelations = generalSemanticRelations;
	}

	public PartOfSpeech getPos() {
		return pos;
	}

	public void setPos(PartOfSpeech pos) {
		this.pos = pos;
	}

	public boolean isInitialized() {
		return initialized;
	}

	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}

	@Override
	public String toString() {
		return "WordnetEntry [id=" + id + ", synonyms=" + synonyms + ", pos=" + pos + ", initialized=" + initialized
				+ "]";
	}

	@Override
	public WordnetEntry clone() {
		WordnetEntry we = new WordnetEntry(id);
		we.synonyms = new HashSet<>(synonyms);
		we.hypernyms = new HashSet<>(hypernyms);
		we.hyponyms = new HashSet<>(hyponyms);
		we.generalSemanticRelations = new HashSet<>(we.generalSemanticRelations);
		we.pos = pos;
		we.initialized = initialized;
		return we;
	}

}
