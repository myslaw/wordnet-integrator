package pl.gpcs.wordnet.integrator.util.helper;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import pl.gpcs.wordnet.integrator.util.GeneralRelation;
import pl.gpcs.wordnet.integrator.util.RelationPair;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.tuples.Pair;

public class WordnetRelativesFinder {

	private final Wordnet wordnet;
	private long stackNest = 0;

	public WordnetRelativesFinder(Wordnet wordnet) {
		this.wordnet = wordnet;
	}

	public Set<WordnetEntry> findVParents(WordnetEntry child) {
		HashSet<WordnetEntry> vParents = new HashSet<>();
		Set<WordnetEntry> parentsToHypernym = wordnet.findReferencingEntriesToHypernym(child);
		if (parentsToHypernym != null) {
			vParents.addAll(parentsToHypernym);
		}
		Set<WordnetEntry> parentsToHyponym = wordnet.findReferencingEntriesToHyponym(child);
		if (parentsToHyponym != null) {
			vParents.addAll(parentsToHyponym);
		}
		return vParents;
	}

	public Set<WordnetEntry> findParents(WordnetEntry child) {
		Set<WordnetEntry> parents = findVParents(child);
		Set<RelationPair<WordnetEntry, GeneralRelation<String>>> referencingInGeneral = wordnet
				.findReferencingEntriesInGeneralRaltion(child);
		if (referencingInGeneral != null) {
			for (RelationPair<WordnetEntry, GeneralRelation<String>> relationPair : referencingInGeneral) {
				parents.add(relationPair.getReferencing());
			}
		}
		return parents;
	}

	public Set<WordnetEntry> findVChilds(WordnetEntry parent) {
		Set<WordnetEntry> vChilds = new HashSet<>();
		Set<WordnetEntry> hypernyms = parent.getHypernyms();
		if (hypernyms != null) {
			vChilds.addAll(hypernyms);
		}
		Set<WordnetEntry> hyponyms = parent.getHyponyms();
		if (hyponyms != null) {
			vChilds.addAll(hyponyms);
		}
		return vChilds;
	}

	public Set<WordnetEntry> findChilds(WordnetEntry parent) {
		Set<WordnetEntry> childs = findVChilds(parent);
		Set<GeneralRelation<String>> general = parent.getGeneralSemanticRelations();
		if (general != null) {
			for (GeneralRelation<String> generalRelation : general) {
				childs.add(generalRelation.getWordnetEntry());
			}
		}
		return childs;
	}

	public Set<WordnetEntry> findDirectVRelatives(WordnetEntry relative) {
		Set<WordnetEntry> vRelatives = findVChilds(relative);
		vRelatives.addAll(findVParents(relative));
		return vRelatives;
	}

	public Set<WordnetEntry> findDirectRelatives(WordnetEntry relative) {
		Set<WordnetEntry> relatives = findChilds(relative);
		relatives.addAll(findParents(relative));
		return relatives;
	}

	/**
	 * Przeszukiwanie wylacznie w pionie. Brane sa pod uwage tylko hiponimy i
	 * hipernimy
	 * 
	 * @return
	 */
	public Set<WordnetEntry> findAllVRelatives(WordnetEntry relative) {
		Set<WordnetEntry> vRelatives = new HashSet<>();
		findAllVRelatives(relative, vRelatives);
		return vRelatives;
	}

	private void findAllVRelatives(WordnetEntry relative, Set<WordnetEntry> foundVRelatives) {
		Queue<WordnetEntry> unprocessedVRelatives = new LinkedList<>();
		unprocessedVRelatives.add(relative);
		WordnetEntry unprocessedVRelative;
		while ((unprocessedVRelative = unprocessedVRelatives.poll()) != null) {
			if (!foundVRelatives.add(unprocessedVRelative)) {
				// relative juz znaleziony, pomin
				continue;
			}
			Set<WordnetEntry> directVRelatives = findDirectVRelatives(unprocessedVRelative);
			for (WordnetEntry directVRelative : directVRelatives) {
				if (!foundVRelatives.contains(directVRelative)) {
					unprocessedVRelatives.add(directVRelative);
				}
			}
		}
	}

	public Set<WordnetEntry> findAllRelatives(WordnetEntry relative) {
		Set<WordnetEntry> foundRelatives = new HashSet<>();
		findAllRelatives(relative, foundRelatives);
		return foundRelatives;
	}

	private void findAllRelatives(WordnetEntry relative, Set<WordnetEntry> foundRelatives) {
		Queue<WordnetEntry> unprocessedRelatives = new LinkedList<>();
		unprocessedRelatives.add(relative);
		WordnetEntry unprocessedRelative;
		while ((unprocessedRelative = unprocessedRelatives.poll()) != null) {
			if (!foundRelatives.add(unprocessedRelative)) {
				// relative found discovered earlier in Queue
				continue;
			}
			Set<WordnetEntry> directRelatives = findDirectRelatives(unprocessedRelative);
			for (WordnetEntry directRelative : directRelatives) {
				if (!foundRelatives.contains(directRelative)) {
					unprocessedRelatives.add(directRelative);
				}
			}
		}
	}

	public Pair<WordnetEntry, Integer> findSmallestRelativeCount() {
		Set<WordnetEntry> taboos = new HashSet<>();
		Pair<WordnetEntry, Integer> bestPair = Pair.of(null, Integer.MAX_VALUE);
		for (WordnetEntry wordnetEntry : wordnet) {
			if (taboos.contains(wordnetEntry)) {
				continue;
			}
			Set<WordnetEntry> relatives = findAllRelatives(wordnetEntry);
			taboos.addAll(relatives);
			if (relatives.size() < bestPair.getObj1()) {
				bestPair = Pair.of(wordnetEntry, relatives.size());
			}
		}
		return bestPair;
	}

	@Deprecated
	/**
	 * @deprecated throws StackOverflowException
	 * @forRemoval yes
	 * @since 1.0
	 * @param relative
	 * @param foundRelatives
	 */
	private void findRecursivelyAllRelatives(WordnetEntry relative, Set<WordnetEntry> foundRelatives) {
		for (WordnetEntry directRelative : findDirectRelatives(relative)) {
			if (foundRelatives.add(directRelative)) {
				System.out.println("Nest:" + stackNest++);
				findRecursivelyAllRelatives(directRelative, foundRelatives);
			}
		}
	}
}
