package pl.gpcs.wordnet.integrator.util;

import java.util.Set;

public interface SynonymNavigable {

	Set<WordnetEntry> findBySynonym(String synonym);
}
