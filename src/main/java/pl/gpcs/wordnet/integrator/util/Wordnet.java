package pl.gpcs.wordnet.integrator.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, doNotUseGetters = true)
public class Wordnet implements Collection<WordnetEntry>, Cloneable, SynonymNavigable {

	private static final Logger LOG = LoggerFactory.getLogger(Wordnet.class);

	@EqualsAndHashCode.Include
	@Getter
	private final String name;
	@EqualsAndHashCode.Include
	@Getter
	private final WordnetType wordnetType;

	private final Map<String, WordnetEntry> idToWordnetEntry = new HashMap<>();
	private final Map<String, Set<WordnetEntry>> synonymTextToWordnetEntries = new HashMap<>(); // find

	private final Map<String, Set<WordnetEntry>> hypernymEntryIdToReferencingEntries = new HashMap<>();
	private final Map<String, Set<WordnetEntry>> hyponymEntryIdToReferencingEntries = new HashMap<>();
	private final Map<String, Set<RelationPair<WordnetEntry, GeneralRelation<String>>>> generalRelationEntryIdToReferencingEntries = new HashMap<>();

	/**
	 * Make sure that newEntry is containing all necessary synonyms, in order to
	 * register them correctly.
	 * 
	 * @param newEntry
	 * @param oldEntry
	 * @return
	 */
	@Deprecated
	public boolean replaceWordnetEntry(WordnetEntry newEntry, WordnetEntry oldEntry) {
		if (!contains(oldEntry)) {
			return false;
		}
		importToReferencingEntries(newEntry, oldEntry);
		idToWordnetEntry.put(newEntry.getId(), newEntry);
//		associateSynonyms(newEntry); //dont associateSynonyms, since it would affect findingCandidates

		remove(oldEntry);
		// do not discover (add) entries referenced by newEnry (thats what .add()
		// method is for)
		return true;
	}

	public void refreshSynonyms(WordnetEntry wordnetEntry, Set<SynonymEntry> newSynonymEntries) {
		if (idToWordnetEntry.get(wordnetEntry.getId()) != wordnetEntry) { // must be already inside wordnet
			throw new IllegalArgumentException(WordnetEntry.class.getSimpleName() + " with id: " + wordnetEntry.getId()
					+ " does not exist in " + name);
		}
		associateSynonyms(wordnetEntry, newSynonymEntries);
	}

	/**
	 * Makes sourceEntry referencing entries reference to targetEntry too
	 * 
	 * @param targetEntry new reference for referencing entries
	 * @param sourceEntry entry which referencing entries should be expanded with
	 *                    targetEntry
	 */
	@Deprecated
	private void importToReferencingEntries(WordnetEntry targetEntry, WordnetEntry sourceEntry) {
		// handle hypernyms
		Set<WordnetEntry> referencingToHypernym = hypernymEntryIdToReferencingEntries.get(sourceEntry.getId());
		if (referencingToHypernym != null) {
			for (WordnetEntry referencingEntry : referencingToHypernym) {
//				referencingEntry.getHypernyms().remove(sourceEntry);
				referencingEntry.getHypernyms().add(targetEntry);
			}
			hypernymEntryIdToReferencingEntries.put(targetEntry.getId(), referencingToHypernym);
		}
		// handle hyponyms
		Set<WordnetEntry> referencingToHyponyms = hyponymEntryIdToReferencingEntries.get(sourceEntry.getId());
		if (referencingToHyponyms != null) {
			for (WordnetEntry referencingEntry : referencingToHyponyms) {
//				referencingEntry.getHyponyms().remove(sourceEntry);
				referencingEntry.getHyponyms().add(targetEntry);
			}
			hyponymEntryIdToReferencingEntries.put(targetEntry.getId(), referencingToHyponyms);
		}
		// handle general raltions
		Set<RelationPair<WordnetEntry, GeneralRelation<String>>> relationPairs = generalRelationEntryIdToReferencingEntries
				.get(sourceEntry.getId());
		if (relationPairs != null) {
			for (RelationPair<WordnetEntry, GeneralRelation<String>> relationPair : relationPairs) {
				if (!sourceEntry.equals(relationPair.getReferenced().getWordnetEntry())) { // TODO: REMOVE ME!
					throw new IllegalArgumentException("Should never happen!");
				}
//				relationPair.getReferencing().getGeneralSemanticRelations().remove(relationPair.getReferenced());
				relationPair.getReferencing().getGeneralSemanticRelations()
						.add(new GeneralRelation<>(targetEntry, relationPair.getReferenced().getRelation()));
			}
			generalRelationEntryIdToReferencingEntries.put(targetEntry.getId(), relationPairs);
		}
	}

	/**
	 * Finds all WordnetEntries which are referencing to given entry as to hypernym.
	 * 
	 * @param hypernym
	 * @return
	 */
	public Set<WordnetEntry> findReferencingEntriesToHypernym(WordnetEntry hypernym) {
		return hypernymEntryIdToReferencingEntries.get(hypernym.getId());
	}

	public Set<WordnetEntry> findReferencingEntriesToHyponym(WordnetEntry hyponym) {
		return hyponymEntryIdToReferencingEntries.get(hyponym.getId());
	}

	public Set<RelationPair<WordnetEntry, GeneralRelation<String>>> findReferencingEntriesInGeneralRaltion(
			WordnetEntry generalEntry) {
		return generalRelationEntryIdToReferencingEntries.get(generalEntry.getId());
	}

	private void registerReference(WordnetEntry referencingEntry, Collection<WordnetEntry> referencedEntries,
			Map<String, Set<WordnetEntry>> wordnetEntryIdToReferencingEntries) {
		Set<WordnetEntry> wordnetEntrySet;
		for (WordnetEntry we : referencedEntries) {
			wordnetEntrySet = wordnetEntryIdToReferencingEntries.get(we.getId());
			if (wordnetEntrySet == null) {
				wordnetEntrySet = new HashSet<>();
				wordnetEntryIdToReferencingEntries.put(we.getId(), wordnetEntrySet);
			}
			wordnetEntrySet.add(referencingEntry);
		}
	}

	private void registerGeneralReference(WordnetEntry referencingEntry) {
		Set<RelationPair<WordnetEntry, GeneralRelation<String>>> generalRelationSet;
		for (GeneralRelation<String> gr : referencingEntry.getGeneralSemanticRelations()) {
			generalRelationSet = generalRelationEntryIdToReferencingEntries.get(gr.getId());
			if (generalRelationSet == null) {
				generalRelationSet = new HashSet<>();
				generalRelationEntryIdToReferencingEntries.put(gr.getId(), generalRelationSet);
			}
			generalRelationSet.add(new RelationPair<>(referencingEntry, gr));
		}
	}

	/**
	 * Saves information that given WordnetEntry is referencing some other entries.
	 * This way it is possible to move references from one entry to another, without
	 * have to search the whole data structure.
	 * 
	 * @param referencingEntry
	 */
	private void registerReferencedEntries(WordnetEntry referencingEntry) {
		if (referencingEntry.getHypernyms() != null) {
			registerReference(referencingEntry, referencingEntry.getHypernyms(), hypernymEntryIdToReferencingEntries);
		}
		if (referencingEntry.getHyponyms() != null) {
			registerReference(referencingEntry, referencingEntry.getHyponyms(), hyponymEntryIdToReferencingEntries);
		}
		if (referencingEntry.getGeneralSemanticRelations() != null) {
			registerGeneralReference(referencingEntry);
		}
	}

	@Override
	public Wordnet clone() {
		Map<String, WordnetEntry> idToWordnetEntryClone = new HashMap<>();
		WordnetEntry we;
		for (Entry<String, WordnetEntry> entry : idToWordnetEntryClone.entrySet()) {
			// TODO: implement
		}

		Wordnet wordnetClone = new Wordnet(name, wordnetType);
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return idToWordnetEntry.size();
	}

	@Override
	public boolean isEmpty() {
		return idToWordnetEntry.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		WordnetEntry we = (WordnetEntry) o;
		if (we.equals(idToWordnetEntry.get(we.getId()))) {
			return true;
		}
		return false;
	}

	@Override
	public Set<WordnetEntry> findBySynonym(String bySynonym) {
		return synonymTextToWordnetEntries.get(bySynonym);
	}

	public WordnetEntry findById(String id) {
		return idToWordnetEntry.get(id);
	}

	@Override
	public Iterator<WordnetEntry> iterator() {
		return idToWordnetEntry.values().iterator();
	}

	@Override
	public Object[] toArray() {
		return idToWordnetEntry.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return idToWordnetEntry.values().toArray(a);
	}

	public void importWordnet(Wordnet otherWordnet) {
//		for (WordnetEntry srcWordnetEntry : sourceWordnet) {
//			add(srcWordnetEntry);
//		}
		Map<String, WordnetEntry> thisEdges = createEdgeMap();
		throw new UnsupportedOperationException();
		// TODO: integration & merging
	}

	private Map<String, WordnetEntry> createEdgeMap() { // depending
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public void importWordnetEntry(WordnetEntry wordnetEntry, Set<WordnetEntry> referencingEntries) {
		// TODO: implement
		// if sufficient number (M) of synonyms matches, merge entries
		// if two entries match DO
		// go hypernym direction
		// make map for every entry on the way (for depth N)
		// record every synonym of every entry (from map)
		// END
		throw new UnsupportedOperationException();
	}

	@Deprecated
	public void enrichWordnetEntry(WordnetEntry dst, WordnetEntry src) {
		// TODO implement
		throw new UnsupportedOperationException();
	}

	/**
	 * Should only be used when initalizing from synsets, or when moving entring to
	 * this wordnet
	 */
	@Override
	public boolean add(WordnetEntry newEntry) {
		if (!newEntry.isInitialized()) {
			return addUninitializedWordnetEntry(newEntry);
		} else {
			throw new IllegalArgumentException("Cannot already initialized entry to wordnet");
		}

	}

	private boolean addUninitializedWordnetEntry(WordnetEntry newEntry) {
		WordnetEntry entryToInitialize;
		if (idToWordnetEntry.containsKey(newEntry.getId())) {
			// entry exists, so it is (99%) uninitialized
			// but referenced at least once by other entry
			WordnetEntry uninitializedEntry = idToWordnetEntry.get(newEntry.getId());
			// perform dummy entry initialization
			if (uninitializedEntry.isInitialized()) { // TODO: remove in final version
				throw new IllegalArgumentException("Cannot initialize already initialized entry.");
			}
			copyDataReferences(uninitializedEntry, newEntry);
			entryToInitialize = uninitializedEntry;
		} else {
			idToWordnetEntry.put(newEntry.getId(), newEntry);
			entryToInitialize = newEntry;
		}
		initializeWordnetEntry(entryToInitialize);
		registerReferencedEntries(entryToInitialize); // +67% to execution time
		return true;
	}

	private void initializeWordnetEntry(WordnetEntry uninitializedEntry) {
		initializeHyponyms(uninitializedEntry);
		initializeHypernyms(uninitializedEntry);
		initializeGeneralSemanticRelations(uninitializedEntry);
		associateSynonyms(uninitializedEntry);
		uninitializedEntry.setInitialized(true);
	}

	private void initializeHyponyms(WordnetEntry sourceEntry) {
		Set<WordnetEntry> hyponyms = insertDummyEntriesAndGetPersisted(sourceEntry.getHyponyms());
		sourceEntry.setHyponyms(hyponyms); // old dummy hypernyms are removed
	}

	private void initializeHypernyms(WordnetEntry newEntry) {
		Set<WordnetEntry> hypernyms = insertDummyEntriesAndGetPersisted(newEntry.getHypernyms());
		newEntry.setHypernyms(hypernyms);
	}

	private Set<WordnetEntry> insertDummyEntriesAndGetPersisted(Set<WordnetEntry> entriesToPersist) {
		if (entriesToPersist == null || entriesToPersist.isEmpty()) {
			return null;
		}
		Set<WordnetEntry> persistedEntries = new HashSet<>();
		for (WordnetEntry entryToPersist : entriesToPersist) {
			// znajdz entry w wordnecie, moze byc wydmuszka
			WordnetEntry persistedEntry = idToWordnetEntry.get(entryToPersist.getId());
			if (persistedEntry == null) {
				// entry nie istnieje w Wordnecie, dodaj wydmuszke
				idToWordnetEntry.put(entryToPersist.getId(), entryToPersist);
				persistedEntry = entryToPersist;
			}
			persistedEntries.add(persistedEntry);
		}
		return persistedEntries;
	}

	/**
	 * Fills stubEntry fields with dataEntry data.
	 * 
	 * @param stubEntry entry containing only id (uninitialized)
	 * @param dataEntry entry being the real entry
	 */
	private void copyDataReferences(WordnetEntry stubEntry, WordnetEntry dataEntry) {
		stubEntry.setHypernyms(dataEntry.getHypernyms());
		stubEntry.setHyponyms(dataEntry.getHyponyms());
		stubEntry.setGeneralSemanticRelations(dataEntry.getGeneralSemanticRelations());
		stubEntry.setSynonyms(dataEntry.getSynonyms());
		stubEntry.setPos(dataEntry.getPos());
	}

	private void initializeGeneralSemanticRelations(WordnetEntry newEntry) {
		Set<GeneralRelation<String>> otherRelations = persistAndGetRelations(newEntry.getGeneralSemanticRelations());
		newEntry.setGeneralSemanticRelations(otherRelations);
	}

	private Set<GeneralRelation<String>> persistAndGetRelations(Set<GeneralRelation<String>> entriesToPersist) {
		if (entriesToPersist == null || entriesToPersist.isEmpty()) {
			return null;
		}
		Set<GeneralRelation<String>> persistedEntries = new HashSet<>();
		for (GeneralRelation<String> relationEntry : entriesToPersist) {
			// wez entry z relacji
			WordnetEntry entryToPersist = relationEntry.getWordnetEntry();
			// sprawdz czy istnieje w wordnecie
			WordnetEntry persistedEntry = idToWordnetEntry.get(entryToPersist.getId());
			if (persistedEntry == null) {
				// nie istnieje, wiec dodaj do wordnetu wydmuszke
				idToWordnetEntry.put(entryToPersist.getId(), entryToPersist);
				persistedEntry = entryToPersist;
			}
			relationEntry.setWordnetEntry(persistedEntry);
			persistedEntries.add(relationEntry);
		}
		return persistedEntries;
	}

	/**
	 * Iteruje po wszystkich synonimach danego entry i tworzy mapowanie nazwy
	 * synonimu do kolekcji zwierajacej jego. Innymi slowy stworz mapowanie synonyms
	 * do targetEntry
	 * 
	 * @param targetEntry
	 * @param newSynonymEntrySet
	 */
	private void associateSynonyms(WordnetEntry targetEntry, Set<SynonymEntry> newSynonymEntrySet) {
		for (SynonymEntry synonymEntry : newSynonymEntrySet) {
			// wez entry, ktore maja synonym z danym tekstem
			Set<WordnetEntry> wordnetEntrySet = synonymTextToWordnetEntries.get(synonymEntry.getText());
			if (wordnetEntrySet == null) {
				wordnetEntrySet = new HashSet<>();
				synonymTextToWordnetEntries.put(synonymEntry.getText(), wordnetEntrySet);
			}
			wordnetEntrySet.add(targetEntry); // dodaj entry do puli dla wskazanego tekstu synonimu
			// TODO implement
		}
	}

	private void associateSynonyms(WordnetEntry targetEntry) {
		associateSynonyms(targetEntry, targetEntry.getSynonyms());
	}

	@Override
	public boolean remove(Object o) {
		WordnetEntry we = (WordnetEntry) o;
		if (idToWordnetEntry.remove(we.getId()) == null) {
			return false;
		}
		for (SynonymEntry synonym : we.getSynonyms()) {
			Set<WordnetEntry> wordnetEntrySet = synonymTextToWordnetEntries.get(synonym.getText());
			if (wordnetEntrySet != null) { // zawsze powinno byc prawdziwe
				wordnetEntrySet.remove(o);
			}
		}
		removeReferences(we);
		return true;
	}

	private void removeReferences(WordnetEntry we) {
		Set<WordnetEntry> entriesReferencingToHypernym = hypernymEntryIdToReferencingEntries.get(we.getId());
		if (entriesReferencingToHypernym != null) {
			for (WordnetEntry referencingEntry : entriesReferencingToHypernym) {
				referencingEntry.getHypernyms().remove(we);
			}
		}
		Set<WordnetEntry> entriesReferencingToHyponym = hyponymEntryIdToReferencingEntries.get(we.getId());
		if (entriesReferencingToHyponym != null) {
			for (WordnetEntry referencingEntry : entriesReferencingToHyponym) {
				referencingEntry.getHyponyms().remove(we);
			}
		}
		Set<RelationPair<WordnetEntry, GeneralRelation<String>>> entriesReferencingToWe = generalRelationEntryIdToReferencingEntries
				.get(we.getId());
		if (entriesReferencingToWe != null) {
			for (RelationPair<WordnetEntry, GeneralRelation<String>> relationPair : entriesReferencingToWe) {
				relationPair.getReferencing().getGeneralSemanticRelations().remove(relationPair.getReferenced());
			}
		}
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends WordnetEntry> c) {
		int sizeBefore = size();
		for (WordnetEntry wordnetEntry : c) {
			add(wordnetEntry);
		}
		return sizeBefore != size();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean modified = false;
		Iterator<?> it = c.iterator();
		while (it.hasNext()) {
			if (remove(it.next())) {
				modified = true;
			}
		}
		return modified;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		idToWordnetEntry.clear();
		synonymTextToWordnetEntries.clear();
	}

	public Collection<WordnetEntry> getAllEntries() {
		return idToWordnetEntry.values();
	}
}
