package pl.gpcs.wordnet.integrator.util;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.model.common.DomainAware;

public final class SynsetUtils {

	private SynsetUtils() {
		throw new UnsupportedOperationException("Should not access this constructor");
	}

	/**
	 * Finding entries in PlWordnet with specified domain.
	 * 
	 * @param <T>
	 * @param domain
	 * @param domainCollection
	 * @return
	 */
	public static <T extends DomainAware> Set<T> findAllPlWordnetSynsetsByDomain(String domain,
			Collection<T> domainCollection) {
		return domainCollection.stream().filter(element -> domain.equals(element.getDomain()))
				.collect(Collectors.toSet());
	}

}
