package pl.gpcs.wordnet.integrator.util;

public enum PartOfSpeech {
	ADJECTIVE, ADVERB, VERB, NOUN;
}
