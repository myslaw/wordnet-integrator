package pl.gpcs.wordnet.integrator.util.tuples;

public class Pair<A, B> {
	private final A obj0;
	private final B obj1;

	private Pair(A obj0, B obj1) {
		super();
		this.obj0 = obj0;
		this.obj1 = obj1;
	}

	public static <A, B> Pair<A, B> of(A obj0, B obj1) {
		return new Pair<A, B>(obj0, obj1);
	}

	public A getObj0() {
		return obj0;
	}

	public B getObj1() {
		return obj1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((obj0 == null) ? 0 : obj0.hashCode());
		result = prime * result + ((obj1 == null) ? 0 : obj1.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (obj0 == null) {
			if (other.obj0 != null)
				return false;
		} else if (!obj0.equals(other.obj0))
			return false;
		if (obj1 == null) {
			if (other.obj1 != null)
				return false;
		} else if (!obj1.equals(other.obj1))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pair [obj0=" + obj0 + ", obj1=" + obj1 + "]";
	}

}
