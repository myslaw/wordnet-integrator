package pl.gpcs.wordnet.integrator.corrector.impl;

import pl.gpcs.wordnet.integrator.corrector.SynsetCorrector;
import pl.gpcs.wordnet.integrator.model.common.Synset;

/**
 * Dummy corrector to accept any synset as it is.
 * 
 * @param <T>
 */
public class AcceptingCorrector<T extends Synset<?, ?>> implements SynsetCorrector<T> {

	@Override
	public boolean correct(T synset) {
		return true;
	}

}
