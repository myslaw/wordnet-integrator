package pl.gpcs.wordnet.integrator.corrector.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.extern.slf4j.Slf4j;
import pl.gpcs.wordnet.integrator.corrector.SynsetCorrector;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Ilr;
import pl.gpcs.wordnet.integrator.model.synset.polnet.Literal;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;

@Slf4j
public class PolnetSynsetCorrector implements SynsetCorrector<PolnetSynset>{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean correct(PolnetSynset synset) {
		if(!correctSynonyms(synset)) {
			LOG.warn("Synset with id {} did not pass corrector.", synset.getId());
			return false;
		}
		correctIlrs(synset);
		return true;
	}

	private void correctIlrs(PolnetSynset synset) {
		List<Ilr> ilrs = synset.getIlr();
		if(ilrs == null || ilrs.isEmpty()) {
			return;
		}
		for (Iterator<Ilr> iterator = ilrs.iterator(); iterator.hasNext();) {
			Ilr ilr = iterator.next();
			if(!validateString(ilr.getText())) {
				LOG.warn("Found invalid {} for synset with id: {}", Ilr.class, synset.getId());
				iterator.remove();
			}
		}
	}
	
	private boolean correctSynonyms(PolnetSynset synset) {
		if(synset.getSynonym() == null) {
			return false;
		}
		List<Literal> literals = synset.getSynonym().getLiteral();
		if(literals == null) {
			return false;
		}
		for (Iterator<Literal> iterator = literals.iterator(); iterator.hasNext();) {
			Literal literal = iterator.next();
			if(!validateString(literal.getText()) || !validateString(literal.getSense())){
				iterator.remove();
				LOG.warn("{} is invalid, removing from synset with id: {}", Literal.class, synset.getId());
			}
		}
		return !literals.isEmpty();
	}
	

	/**
	 * 
	 * @param string
	 * @return true if string is valid, false otherwise
	 */
	private boolean validateString(String string) {
		return string != null && !string.isEmpty();
	}
	
}
