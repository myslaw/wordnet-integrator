package pl.gpcs.wordnet.integrator.corrector;

import pl.gpcs.wordnet.integrator.model.common.Synset;

public interface SynsetCorrector<T extends Synset<?, ?>> {
	/**
	 * Corrects the synset, so that it wont corrupt Wordnet,
	 * 
	 * @param synset
	 * @return true, if the synset has been corrected, and can be used, false if it
	 *         is uncorrectable
	 */
	boolean correct(T synset);
}
