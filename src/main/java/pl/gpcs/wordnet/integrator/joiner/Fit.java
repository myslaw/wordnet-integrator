package pl.gpcs.wordnet.integrator.joiner;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.gpcs.wordnet.integrator.matcher.WordnetMatcher;
import pl.gpcs.wordnet.integrator.matcher.impl.WordnetMatcherImpl;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode(doNotUseGetters = true, onlyExplicitlyIncluded = true)
public class Fit implements Comparable<Fit> {

	private static final WordnetMatcher WORDNET_MATCHER = new WordnetMatcherImpl();
	private static final int UNINITIALIZED = -1;

	@EqualsAndHashCode.Include
	private final WordnetEntry source;
	@EqualsAndHashCode.Include
	private final WordnetEntry target;
	@EqualsAndHashCode.Include
	private final MergingStrategy mergingStrategy;
	private final int similarity;
	@Getter(AccessLevel.NONE)
	private final AtomicInteger branchSimilarity = new AtomicInteger(UNINITIALIZED);

	public int getBranchSimilarity() {
		return branchSimilarity.updateAndGet(value -> value != UNINITIALIZED ? value
				: WORDNET_MATCHER.calcBranchSynonymsSimilarity(target, source, mergingStrategy));
	}

	@Override
	public int compareTo(Fit o) {
		if (equals(o)) {
			return 0;
		}
		incomparableThrowing(this, o);
		final int similarityDifference = this.similarity - o.similarity;
		if (similarityDifference != 0) {
			return similarityDifference;
		}
		return getBranchSimilarity() - o.getBranchSimilarity();
	}

	private static void incomparableThrowing(Fit fit1, Fit fit2) {
		if (fit1.mergingStrategy != fit2.mergingStrategy) {
			throw new IllegalArgumentException(
					"Cannot use compareTo on " + Fit.class + " with different " + MergingStrategy.class);
		}
		if ((!Objects.equals(fit1.source, fit2.source)) && (!Objects.equals(fit1.target, fit2.target))) {
			throw new IllegalArgumentException("Fits are not comparable, since they have no common source nor target");
		}
	}

	public static Fit of(WordnetEntry source, WordnetEntry target, MergingStrategy direction) {
		return new Fit(source, target, direction, WORDNET_MATCHER.calcSynonymsSimilarity(source, target));
	}

}
