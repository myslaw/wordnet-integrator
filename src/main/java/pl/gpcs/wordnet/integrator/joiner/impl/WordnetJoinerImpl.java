package pl.gpcs.wordnet.integrator.joiner.impl;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.helpers.MessageFormatter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.gpcs.wordnet.integrator.crawler.WordnetCrawler;
import pl.gpcs.wordnet.integrator.entangler.WordnetEntryBranchEntangler;
import pl.gpcs.wordnet.integrator.entangler.WordnetEntryNodeEntangler;
import pl.gpcs.wordnet.integrator.joiner.Fit;
import pl.gpcs.wordnet.integrator.joiner.WordnetJoiner;
import pl.gpcs.wordnet.integrator.printer.MergeStatsPreparator;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.util.GeneralRelation;
import pl.gpcs.wordnet.integrator.util.PartOfSpeech;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;

@Slf4j
@RequiredArgsConstructor
public class WordnetJoinerImpl implements WordnetJoiner {

	private static final WordnetCrawler WORDNET_CRAWLER = new WordnetCrawler();

	final MergingStrategy mergingStrategy;
	final Wordnet sourceWordnet;
	final Wordnet targetWordnet;

	/**
	 * Merged SynonymEntries
	 */
	private final Map<SynonymEntry, SynonymEntry> sourceSynonymToTargetSynonym = new HashMap<>();
	private final Set<SynonymEntry> mappedTargetSynonymEntries = new HashSet<>();
	private final Set<SynonymEntry> movedSourceEntries = new HashSet<>();
	private final Map<PartOfSpeech, Integer> posToMergedSynonymsCount = new EnumMap<>(PartOfSpeech.class);
	private final Map<PartOfSpeech, Integer> posToMovedSynonymsCount = new EnumMap<>(PartOfSpeech.class);

	private final int minSynonymMatchingCount;
	/**
	 * Used to how many edge childs should be interpreted as being edge. To ignore
	 * childs and interpet only edge elements, the threshold value should be set to
	 * 0
	 */
	private final int edgeThreshold;

	private Map<WordnetEntry, Fit> nodeEntangledSourceEntryToFit;
	private Map<WordnetEntry, Fit> branchEntangledSourceEntryToFit;
	private Map<PartOfSpeech, Integer> posToMergedWordnetEntries = new EnumMap<>(PartOfSpeech.class);

	/**
	 * Used to check which entries have been merged together. To make some data
	 * review after the merging process. Fits sum of nodeEntangledSourceEntryToFit &
	 * branchEntangledSourceEntryToFit
	 */
	private final Map<WordnetEntry, WordnetEntry> entangledSourceToTargetWordnetEntry = new HashMap<>();

	@Override
	public void join() {
		switch (mergingStrategy) {
		case HYPERNYM_ORIGIN:
			entangleFromHypernymEdge();
			break;
		case HYPONYM_ORIGIN:
			entangleFromHyponymEdge();
			break;
		default:
			throw new UnsupportedOperationException("Merging strategy: " + mergingStrategy + " is not supported.");
		}
		consolidateEntaledEntries();
		mergeAllEntangledEntries();
		moveUnmergedSourceWordnetEntriesToTargetWordnet();
		printMergeStats();
	}

	private void printMergeStats() {
		if (!LOG.isInfoEnabled()) {
			return;
		}
		final String joinName = MessageFormatter
				.arrayFormat("Join stats for merge {} to {} with {} strategy and edge of {}", new Object[] {
						sourceWordnet.getName(), targetWordnet.getName(), mergingStrategy, edgeThreshold })
				.getMessage();
		LOG.info("{} running...", joinName);
		MergeStatsPreparator statsPreparator = new MergeStatsPreparator("\n\t");
		LOG.info("Merged SynonymEntries percentage share per part of speech (total: {} merges):{}",
				posToMergedSynonymsCount.values().stream().mapToInt(Integer::intValue).sum(),
				statsPreparator.prepMergedSynsetEntryPercentageStats(posToMergedSynonymsCount));
		LOG.info("Merged WordnetEntries for each PartOfSpeech (total {}):{}",
				posToMergedWordnetEntries.values().stream().mapToInt(Integer::intValue).sum(),
				statsPreparator.prepMergedWordnetEntriesPercentageStats(posToMergedWordnetEntries));
		LOG.info("{} finished.", joinName);
	}

	private void consolidateEntaledEntries() {
		for (Fit fit : nodeEntangledSourceEntryToFit.values()) {
			if (entangledSourceToTargetWordnetEntry.put(fit.getSource(), fit.getTarget()) != null) {
				throw new IllegalStateException("Single entry can be entangled to only one other entry!");
			}
		}
		for (Fit fit : branchEntangledSourceEntryToFit.values()) {
			if (entangledSourceToTargetWordnetEntry.put(fit.getSource(), fit.getTarget()) != null) {
				throw new IllegalStateException("Single entry can be entangled to only one other entry!");
			}
		}
	}

	private void mergeAllEntangledEntries() {
		LOG.info("Merging {} edge entangled entries...", nodeEntangledSourceEntryToFit.size());
		for (Fit fit : nodeEntangledSourceEntryToFit.values()) {
			mergeEntangledEntry(fit);
		}
		LOG.info("Merging {} branch entangled entries...", branchEntangledSourceEntryToFit.size());
		for (Fit fit : branchEntangledSourceEntryToFit.values()) {
			mergeEntangledEntry(fit);
		}
		LOG.info("Finished merging entangled entries.");
	}

	private void mergeEntangledEntry(Fit fit) {
		mergeEntangledEntry(fit.getSource(), fit.getTarget());
	}

	private void mergeEntangledEntry(WordnetEntry sourceEntry, WordnetEntry targetEntry) {
		LOG.trace("Merging WordnetEntries, source id: {}, with target id: {}...", sourceEntry.getId(),
				targetEntry.getId());
		if (sourceEntry.getPos() != targetEntry.getPos()) {
			throw new IllegalArgumentException("Can merge only entries with same pos!");
		}
		posToMergedWordnetEntries.merge(targetEntry.getPos(), 1, Integer::sum);
		mergeSynonymsForWE(sourceEntry, targetEntry);
		mergeHypernymsForWE(sourceEntry, targetEntry);
		mergeHyponymsForWE(sourceEntry, targetEntry);
		mergeGeneralSemanticRelations(sourceEntry, targetEntry);
	}

	private void mergeGeneralSemanticRelations(WordnetEntry sourceEntry, WordnetEntry targetEntry) {
		if (sourceEntry.getGeneralSemanticRelations() == null) {
			return;
		}
		Set<GeneralRelation<String>> targetGSR = targetEntry.getGeneralSemanticRelations();
		if (targetGSR == null) {
			targetGSR = new HashSet<>();
		}
		for (GeneralRelation<String> sourceGeneralRelation : sourceEntry.getGeneralSemanticRelations()) {
			WordnetEntry correspondingTargetEntry = entangledSourceToTargetWordnetEntry
					.get(sourceGeneralRelation.getWordnetEntry());
			if (correspondingTargetEntry != null) {
				// trzeba zrobic new, poniewaz generalRelation juz znajduje si� w
				// potencjalnie
				// kilku setach (zbiorach)
				targetGSR.add(new GeneralRelation<>(correspondingTargetEntry, sourceGeneralRelation.getRelation()));
			} else {
				targetGSR.add(sourceGeneralRelation);
			}
		}
		targetEntry.setGeneralSemanticRelations(targetGSR);
	}

	private void mergeHyponymsForWE(WordnetEntry sourceEntry, WordnetEntry targetEntry) {
		targetEntry.setHyponyms(mergeSourceEntriesToTarget(sourceEntry.getHyponyms(), targetEntry.getHyponyms()));
	}

	private void mergeHypernymsForWE(WordnetEntry sourceEntry, WordnetEntry targetEntry) {
		targetEntry.setHypernyms(mergeSourceEntriesToTarget(sourceEntry.getHypernyms(), targetEntry.getHypernyms()));
	}

	private Set<WordnetEntry> mergeSourceEntriesToTarget(Set<WordnetEntry> sourceEntries,
			Set<WordnetEntry> targetEntries) {
		if (sourceEntries == null) {
			return targetEntries;
		}
		if (targetEntries == null) {
			targetEntries = new HashSet<>();
		}
		for (WordnetEntry sourceEntry : sourceEntries) {
			// znajdz zmapowany entry (source) -> (target)
			WordnetEntry correspondingTargetEntry = entangledSourceToTargetWordnetEntry.get(sourceEntry);
			if (correspondingTargetEntry != null) {
				// istnieje entangled target entry, wi�c do��cz je zamiast orginalnego
				// source
				// entry.
				targetEntries.add(correspondingTargetEntry);
			} else {
				// nie ma entangled entry, wez oryginalny source hypo/hypernym i wklej do target
				// entry
				targetEntries.add(sourceEntry);
			}
		}
		return targetEntries;
	}

	private void mergeSynonymsForWE(WordnetEntry sourceEntry, WordnetEntry targetEntry) {
		Map<String, SynonymEntry> textToSynonym = createMapOfTextToSynonymEntry(sourceEntry.getSynonyms());
		int mergedSynonymEntries = 0;
		for (SynonymEntry targetSynonym : targetEntry.getSynonyms()) {
			SynonymEntry sourceSynonym = textToSynonym.remove(targetSynonym.getText());
			if (sourceSynonym != null) {
				SynonymEntry mapped = sourceSynonymToTargetSynonym.get(sourceSynonym);
				if (mapped == null) {
					// synonimy sa (powinny) unikalne, wiec to przyda sie tylko do sprawdzania
					// scalenia
					sourceSynonymToTargetSynonym.put(sourceSynonym, targetSynonym);
					++mergedSynonymEntries;
					mappedTargetSynonymEntries.add(targetSynonym);
				} else {
					// juz zmapowane, pewnie (99%) w innej czesci mowy
					if (!targetSynonym.equals(mapped)) {
						// zmapowane ale do innego targetSynonym. MOze byc kwestia kolejnosci iteracji
						// po zbiorze w przypadku wielokrotnego wystapienia tego samego tekstu dla
						// syononym w obrebie WE.
						LOG.error(
								"Source SynonymEntry {} is already mapped to {}. Target SynonymEntry {} from {} cannot be mapped.",
								sourceSynonym, mapped, targetSynonym, targetEntry.getId());
//						throw new RuntimeException(
//								"Synonym mapping exceptiom. " + sourceSynonym.toString() + " already mapped to "
//										+ mapped.toString() + ", thus cannot be mapped to " + targetSynonym.toString());
					} else {
						++mergedSynonymEntries;
					}
				}
			}
		}
		posToMergedSynonymsCount.merge(targetEntry.getPos(), mergedSynonymEntries,
				(oldValue, newValue) -> oldValue + newValue);
		// remaining source synonyms are to be moved to target entry
		Collection<SynonymEntry> unmergedSourceEntries = textToSynonym.values();
		// oznacz skopiowane elementy
		movedSourceEntries.addAll(unmergedSourceEntries);
		// nie kopiuj jeden do jednego, bo mo�e wyj��, �e w oryginalnym
		// wordnecie takie
		// istniej�
		Set<SynonymEntry> synonymsToMove = createExtSynonymEntries(unmergedSourceEntries);
		posToMovedSynonymsCount.merge(targetEntry.getPos(), synonymsToMove.size(),
				(oldValue, newValue) -> oldValue + newValue);
		targetEntry.getSynonyms().addAll(synonymsToMove);
		targetWordnet.refreshSynonyms(targetEntry, synonymsToMove);
	}

	private Map<String, SynonymEntry> createMapOfTextToSynonymEntry(Set<SynonymEntry> synonyms) {
		return synonyms.stream().collect(Collectors.toMap(SynonymEntry::getText, Function.identity()));
	}

	private void moveUnmergedSourceWordnetEntriesToTargetWordnet() {
		for (WordnetEntry sourceWordnetEntry : sourceWordnet) {
			if (!entangledSourceToTargetWordnetEntry.containsKey(sourceWordnetEntry)) {
				moveEntangledReferences(sourceWordnetEntry);
				prepareWordnetEntryToMove(sourceWordnetEntry);
				targetWordnet.add(sourceWordnetEntry);
			}
		}
	}

	private void prepareWordnetEntryToMove(WordnetEntry initializedEntry) {
		initializedEntry.setHypernyms(stripToIdOnly(initializedEntry.getHypernyms()));
		initializedEntry.setHyponyms(stripToIdOnly(initializedEntry.getHyponyms()));
		initializedEntry.setGeneralSemanticRelations(stripToIdOnly(initializedEntry.getGeneralSemanticRelations()));
		initializedEntry.setSynonyms(makeMovedSynonymEntries(initializedEntry.getSynonyms()));
		initializedEntry.setInitialized(false);
	}

	private Set<WordnetEntry> stripToIdOnly(Collection<WordnetEntry> wordnetEntries) {
		if (wordnetEntries == null) {
			return null; // FIXME change to empty HashSet
		}
		return wordnetEntries.stream().map(we -> new WordnetEntry(we.getId()))
				.collect(Collectors.toCollection(HashSet::new));
	}

	private Set<GeneralRelation<String>> stripToIdOnly(Set<GeneralRelation<String>> generalSemanticRelations) {
		if (generalSemanticRelations == null) {
			return null; // FIXME change to empty HashSet
		}
		return generalSemanticRelations.stream()
				.map(gr -> new GeneralRelation<>(new WordnetEntry(gr.getId()), gr.getRelation()))
				.collect(Collectors.toCollection(HashSet::new));
	}

	private Set<SynonymEntry> makeMovedSynonymEntries(Collection<SynonymEntry> synonymEntries) {
		return synonymEntries.stream().map(se -> new SynonymEntry(se.getText(), se.getSense() + "_mvd"))
				.collect(Collectors.toSet());
	}

	// FIXME REMOVE ME
	private boolean isReferencing(WordnetEntry parent, WordnetEntry target) {
		if (parent.getHypernyms() != null) {
			if (parent.getHypernyms().contains(target))
				return true;
		}
		if (parent.getHyponyms() != null) {
			if (parent.getHyponyms().contains(target))
				return true;
		}
		if (parent.getGeneralSemanticRelations() != null) {
			if (parent.getGeneralSemanticRelations().stream().map(GeneralRelation::getId)
					.filter(id -> id.equals(target.getId())).findFirst().isPresent())
				return true;
		}
		return false;
	}

	private void moveEntangledReferences(WordnetEntry sourceWordnetEntry) {
		replaceSourceWithTarget(sourceWordnetEntry.getHypernyms());
		replaceSourceWithTarget(sourceWordnetEntry.getHyponyms());
		replaceSourceWithTarget(sourceWordnetEntry.getGeneralSemanticRelations());
	}

	private void replaceSourceWithTarget(Set<GeneralRelation<String>> generalSemanticRelations) {
		if (generalSemanticRelations == null || generalSemanticRelations.isEmpty()) {
			return;
		}
		List<GeneralRelation<String>> replacementEntries = new LinkedList<>();
		final Iterator<GeneralRelation<String>> iterator = generalSemanticRelations.iterator();
		while (iterator.hasNext()) {
			GeneralRelation<String> sourceGeneralRelation = iterator.next();
			WordnetEntry replacement = entangledSourceToTargetWordnetEntry.get(sourceGeneralRelation.getWordnetEntry());
			if (replacement != null) {
				replacementEntries.add(new GeneralRelation<>(replacement, sourceGeneralRelation.getRelation()));
				iterator.remove();
			}
		}
		generalSemanticRelations.addAll(replacementEntries);
	}

	/**
	 * 
	 * @param wordnetEntries
	 * @return
	 */
	private void replaceSourceWithTarget(Collection<WordnetEntry> wordnetEntries) {
		if (wordnetEntries == null || wordnetEntries.isEmpty()) {
			return;
		}
		List<WordnetEntry> replacementEntries = new LinkedList<>();
		final Iterator<WordnetEntry> iterator = wordnetEntries.iterator();
		while (iterator.hasNext()) {
			WordnetEntry sourceWordnetEntry = iterator.next();
			WordnetEntry replacement = entangledSourceToTargetWordnetEntry.get(sourceWordnetEntry);
			if (replacement != null) {
				replacementEntries.add(replacement);
				iterator.remove();
			}
		}
		wordnetEntries.addAll(replacementEntries);
	}

	private void entangleFromHyponymEdge() {
		nodeEntangledSourceEntryToFit = new WordnetEntryNodeEntangler(mergingStrategy,
				WORDNET_CRAWLER.findEdgeHyponyms(targetWordnet, edgeThreshold), sourceWordnet).entangle();
		entangleBranches(nodeEntangledSourceEntryToFit.values());
	}

	private void entangleFromHypernymEdge() {
		nodeEntangledSourceEntryToFit = new WordnetEntryNodeEntangler(mergingStrategy,
				WORDNET_CRAWLER.findEdgeHypernyms(targetWordnet, edgeThreshold), sourceWordnet).entangle();
		entangleBranches(nodeEntangledSourceEntryToFit.values());
	}

	private void entangleBranches(Collection<Fit> originsFits) {
		branchEntangledSourceEntryToFit = new WordnetEntryBranchEntangler(originsFits, mergingStrategy).entangle();
	}

	/**
	 * Make map of synonym to entries (there might be few entries with same synonym
	 * within one tree). It consists of all (sub)childs of given entry
	 * 
	 * @param entry
	 * @return
	 */
	public Map<String, Set<WordnetEntry>> createSynonymTextToChildEntriesSet(WordnetEntry entry,
			Set<WordnetEntry> tabooEntries) {
		Set<WordnetEntry> referencedEntries = findSubchilds(entry);
		referencedEntries.removeAll(tabooEntries);
		Map<String, Set<WordnetEntry>> synonymToReferencedEntriesSet = new HashMap<>();
		Set<WordnetEntry> entriesForSynonym;
		for (WordnetEntry referencedEntry : referencedEntries) {
			for (SynonymEntry synonym : referencedEntry.getSynonyms()) {
				entriesForSynonym = synonymToReferencedEntriesSet.get(synonym.getText());
				if (entriesForSynonym == null) {
					entriesForSynonym = new HashSet<>();
					synonymToReferencedEntriesSet.put(synonym.getText(), entriesForSynonym);
				}
				entriesForSynonym.add(referencedEntry);
			}
		}
		return synonymToReferencedEntriesSet; // FIXME
	}

	public static Set<String> extractSynonymsAsStringSet(Collection<WordnetEntry> wordnetEntries) {
		return wordnetEntries.stream().flatMap(we -> extractSynonymsAsStringSet(we).stream())
				.collect(Collectors.toCollection(HashSet::new));
	}

	public static Set<String> extractSynonymsAsStringSet(WordnetEntry wordnetEntry) {
		return wordnetEntry.getSynonyms().stream().map(SynonymEntry::getText)
				.collect(Collectors.toCollection(HashSet::new));
	}

	/**
	 * Creates _ext SynonymEntries based on the provided sourceEntry. Such source
	 * synonym entries do not have mapping to any target synonym entry.
	 * 
	 * @param sourceEntry
	 * @return
	 */
	private SynonymEntry createExtSynonymEntry(SynonymEntry sourceEntry) {
		return new SynonymEntry(sourceEntry.getText(), sourceEntry.getSense() + "_ext");
	}

	private Set<SynonymEntry> createExtSynonymEntries(Collection<SynonymEntry> synonymEntries) {
		Set<SynonymEntry> extSynonymEntries = new HashSet<>(synonymEntries.size());
		for (SynonymEntry synonymEntry : synonymEntries) {
			extSynonymEntries.add(createExtSynonymEntry(synonymEntry));
		}
		return extSynonymEntries;
	}

	/**
	 * Searches for WordnetEntries which are accessible (in)directly from a given
	 * entry. The search is made depending on the {@link mergingStrategy}
	 * 
	 * @param entry origin entry
	 * @return (sub)child entries
	 */
	private Set<WordnetEntry> findSubchilds(WordnetEntry entry) {
		switch (mergingStrategy) {
		case HYPERNYM_ORIGIN:
			// from hypernym edge to hyponym edge
			return WORDNET_CRAWLER.traverseHyponymDirection(entry);
		case HYPONYM_ORIGIN:
			// from hyponym edge to hypernym edge
			return WORDNET_CRAWLER.traverseHypernymDirection(entry);
		default:
			throw new UnsupportedOperationException(mergingStrategy + " is not supported for finding subchilds");
		}
	}

	public int countSynonymsIntersection(Set<SynonymEntry> sourceSynonymEntries,
			Set<SynonymEntry> targetSynonymEntries) {
		if (sourceSynonymEntries.isEmpty() || targetSynonymEntries.isEmpty()) {
			return 0;
		}
		Set<String> sourceSynonyms = extractSynonymStrings(sourceSynonymEntries);
		Set<String> targetSynonyms = extractSynonymStrings(targetSynonymEntries);
		int intersectionSize = 0;
		for (String sourceSynonym : sourceSynonyms) {
			if (targetSynonyms.contains(sourceSynonym)) {
				++intersectionSize;
			}
		}
		return intersectionSize;
	}

	private Set<String> extractSynonymStrings(Set<SynonymEntry> synonymEntries) {
		return synonymEntries.stream().map(SynonymEntry::getText).collect(Collectors.toSet());
	}

}