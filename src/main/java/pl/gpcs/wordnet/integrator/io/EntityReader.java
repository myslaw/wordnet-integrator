package pl.gpcs.wordnet.integrator.io;

import java.io.Closeable;

import pl.gpcs.wordnet.integrator.exception.EntityReadException;

public interface EntityReader<T, E extends EntityReadException> extends Closeable, AutoCloseable {
	T read() throws E;
}
