package pl.gpcs.wordnet.integrator.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.exception.SynsetReadException;
import pl.gpcs.wordnet.integrator.filter.SynsetFilter;
import pl.gpcs.wordnet.integrator.model.common.Synset;

public class SynsetFileReader<T extends Synset<?, ?>> implements SynsetReader<T> {
	private final BufferedReader input;
	private final Class<T> type;
	private final Unmarshaller unmarshaller;
	private final SynsetFilter<String> rawSynsetFilter;
	private final SynsetFilter<T> pojoSynsetFilter;
	private static final String OPEN_SYNSET = "<SYNSET>";
	private static final String CLOSE_SYNSET = "</SYNSET>";

	private SynsetFileReader(Class<T> clazz, BufferedReader input, SynsetFilter<String> rawSynsetFilter,
			SynsetFilter<T> pojoSynsetFilter, boolean dummy) throws InitializationException {
		if (clazz == null) {
			throw new NullPointerException();
		}
		this.type = clazz;
		this.input = input;
		this.rawSynsetFilter = rawSynsetFilter == null ? new AcceptFilter<>() : rawSynsetFilter;
		this.pojoSynsetFilter = pojoSynsetFilter == null ? new AcceptFilter<>() : pojoSynsetFilter;
		try {
			this.unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
		} catch (JAXBException e) {
			throw new InitializationException(e);
		}
	}

	public SynsetFileReader(Class<T> clazz, Path path) throws InitializationException, IOException {
		this(clazz, new BufferedReader(new InputStreamReader(Files.newInputStream(path), StandardCharsets.UTF_8)), null,
				null, false);
	}

	public SynsetFileReader(Class<T> clazz, Path path, SynsetFilter<String> rawSynsetFilter,
			SynsetFilter<T> pojoSynsetFilter) throws InitializationException, IOException {
		this(clazz, new BufferedReader(new InputStreamReader(Files.newInputStream(path), StandardCharsets.UTF_8)),
				rawSynsetFilter, pojoSynsetFilter, false);
	}

	@Override
	public T read() throws SynsetReadException {
		try {
			String xmlSynset;
			T pojoSynset;
			do {
				while ((xmlSynset = readXmlSynset()) != null) {
					// read until end OR valid synset found
					if (rawSynsetFilter.filter(xmlSynset)) {
						// valid synset found
						break;
					}
				}
				if (xmlSynset == null) {
					return null;
				}
				pojoSynset = type.cast(unmarshaller.unmarshal(new StringReader(xmlSynset)));
			} while (!pojoSynsetFilter.filter(pojoSynset));
			return pojoSynset;
		} catch (JAXBException | IOException e) {
			throw new SynsetReadException(e);
		}
	}

	private boolean isContentEqual(StringBuilder sb, String s) {
		if (sb.length() != s.length()) {
			return false;
		}
		for (int i = 0; i < sb.length(); ++i) {
			if (sb.charAt(i) != s.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	private String readXmlSynset() throws IOException {
		final StringBuilder rawXmlSynset = new StringBuilder();
		final StringBuilder temp = new StringBuilder();
		boolean isSynset = false;
		boolean isTag = false;
		int c;
		char aChar;
		while ((c = input.read()) > -1) {
			aChar = (char) c;
			if ('<' == aChar) { // tag
				isTag = true;
			} else if ('>' == aChar) {
				temp.append(aChar);
				if (isContentEqual(temp, OPEN_SYNSET)) {
					rawXmlSynset.append(OPEN_SYNSET);
					isSynset = true;
					continue;
				} else if (isContentEqual(temp, CLOSE_SYNSET)) {
					rawXmlSynset.append(aChar);
					return rawXmlSynset.toString();
				}
				temp.setLength(0); // clear
				isTag = false;
			}

			if (isTag) {
				temp.append(aChar);
			}

			if (isSynset) {
				rawXmlSynset.append(aChar);
			}

		}
		return null;
	}

	@Override
	public void close() throws IOException {
		input.close();
	}

}

class AcceptFilter<T> implements SynsetFilter<T> {

	@Override
	public boolean filter(T rawSynset) {
		return true;
	}

}
