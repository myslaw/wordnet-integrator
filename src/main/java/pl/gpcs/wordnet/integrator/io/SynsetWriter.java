package pl.gpcs.wordnet.integrator.io;

import java.io.Closeable;

import pl.gpcs.wordnet.integrator.exception.SynsetWriteException;
import pl.gpcs.wordnet.integrator.model.common.Synset;

public interface SynsetWriter<T extends Synset<?, ?>> extends Closeable, AutoCloseable {

	public void write(T synset) throws SynsetWriteException;
}
