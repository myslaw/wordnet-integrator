package pl.gpcs.wordnet.integrator.io;

import pl.gpcs.wordnet.integrator.exception.SynsetReadException;
import pl.gpcs.wordnet.integrator.model.common.Synset;

public interface SynsetReader<T extends Synset<?, ?>> extends EntityReader<T, SynsetReadException> {

}
