package pl.gpcs.wordnet.integrator.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.exception.ConjugationReadException;
import pl.gpcs.wordnet.integrator.model.sjp.SjpVerbConjugation;
import pl.gpcs.wordnet.integrator.model.verb.Conjugation;

public class ConjugationReader implements EntityReader<Conjugation, ConjugationReadException> {
	private final BufferedReader input;

	private ConjugationReader(BufferedReader input) {
		this.input = input;
	}

	public ConjugationReader(Path path) throws IOException {
		this(new BufferedReader(new InputStreamReader(Files.newInputStream(path), StandardCharsets.UTF_8)));
	}

	@Override
	public Conjugation read() throws ConjugationReadException {
		try {
			String line;
			String[] words;
			Conjugation conjugation;
			while ((line = input.readLine()) != null) {
				words = line.split(", ");
				if (words.length > 1) {
					conjugation = new SjpVerbConjugation();
					conjugation.set(words[0]);
					conjugation.setConjugations(Arrays.stream(words, 1, words.length).collect(Collectors.toSet()));
					return conjugation;
				}
			}
		} catch (IOException e) {
			throw new ConjugationReadException(e);
		}
		return null;
	}

	@Override
	public void close() throws IOException {
		input.close();
	}

}
