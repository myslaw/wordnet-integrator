package pl.gpcs.wordnet.integrator.io;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

import pl.gpcs.wordnet.integrator.exception.SynsetReadException;
import pl.gpcs.wordnet.integrator.model.common.Synset;

public class SynsetCollectionReader<T extends Synset<?, ?>> implements SynsetReader<T> {

	private Iterator<T> iterator;

	public SynsetCollectionReader(Collection<T> collection) {
		this.iterator = collection.stream().collect(Collectors.toCollection(LinkedList::new)).iterator();
	}

	@Override
	public T read() throws SynsetReadException {
		if (iterator.hasNext()) {
			return iterator.next();
		}
		return null;
	}

	@Override
	public void close() throws IOException {
		iterator = null;
	}

}
