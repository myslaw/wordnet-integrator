package pl.gpcs.wordnet.integrator.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.exception.SynsetWriteException;
import pl.gpcs.wordnet.integrator.model.common.Synset;

public class SynsetFileWriter<T extends Synset<?, ?>> implements SynsetWriter<T> {

	private final BufferedWriter output;
	private final Marshaller marshaller;

	public SynsetFileWriter(Class<T> clazz, Path path) throws IOException, InitializationException {
		path.subpath(0, path.getNameCount() - 1).toFile().mkdirs();
		this.output = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(path), StandardCharsets.UTF_8));
		try {
			this.marshaller = JAXBContext.newInstance(clazz).createMarshaller();
			this.marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		} catch (JAXBException e) {
			throw new InitializationException(e);
		}
	}

	@Override
	public void write(T synset) throws SynsetWriteException {
		try {
			marshaller.marshal(synset, output);
			output.write("\n");
		} catch (JAXBException | IOException e) {
			throw new SynsetWriteException(e);
		}
	}

	@Override
	public void close() throws IOException {
		output.close();
	}
}
