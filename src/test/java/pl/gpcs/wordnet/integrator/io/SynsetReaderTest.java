package pl.gpcs.wordnet.integrator.io;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.exception.SynsetReadException;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;

public class SynsetReaderTest {

	@Test
	void UnmarshallPlWordnet_ValidFile_NoExceptions() {
		Path path = Paths.get("src", "main", "resources", "plwordnet_2_3_visdic.xml");
		try (SynsetFileReader<PlWordnetSynset> plWordnetReader = new SynsetFileReader<PlWordnetSynset>(
				PlWordnetSynset.class, path)) {
			PlWordnetSynset synset;
			Set<String> posSet = new HashSet<>();
			Map<String, Integer> posToCount = new HashMap<>();
			while ((synset = plWordnetReader.read()) != null) {
				if (synset.getId() == null || synset.getId().isEmpty()) {
					fail("Synset ID is null or emtpy");
				}
				posSet.add(synset.getPos());
			}
			System.out.println("plWordnet:");
			for (String pos : posSet) {
				System.out.println(pos);
			}
		} catch (IOException | InitializationException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			fail(sw.toString());
		} catch (SynsetReadException e) {
			fail("Failed to read synset POJO.");
		}
	}

	@Test
	void UnmarshallPolnet_ValidFile_NoExceptions() {
		Path path = Paths.get("src", "main", "resources", "polnet.xml");
		try (SynsetFileReader<PolnetSynset> polnetReader = new SynsetFileReader<>(PolnetSynset.class, path)) {
			PolnetSynset synset;
			Set<String> posSet = new HashSet<>();
			while ((synset = polnetReader.read()) != null) {
				if (synset.getId() == null || synset.getId().isEmpty()) {
					fail("Synset ID is null or emtpy");
				}
				posSet.add(synset.getPos());
			}
			System.out.println("\nPolnet:");
			for (String pos : posSet) {
				System.out.println(pos);
			}
		} catch (IOException | InitializationException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			fail(sw.toString());
		} catch (SynsetReadException e) {
			fail("Failed to read synset POJO.", e);
		}
	}

}
