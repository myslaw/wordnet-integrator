package pl.gpcs.wordnet.integrator.io;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import pl.gpcs.wordnet.integrator.exception.ConjugationReadException;
import pl.gpcs.wordnet.integrator.model.verb.Conjugation;

class ConjugationReaderTest {
	@Test
	void testVerbConjugationRead() {
		Path path = Paths.get("src", "main", "resources", "odm.txt");
		try (ConjugationReader verbConjugationReader = new ConjugationReader(path)) {
			Conjugation verbConjugation;
			while ((verbConjugation = verbConjugationReader.read()) != null) {
				if (verbConjugation.getConjugations() == null || verbConjugation.getConjugations().isEmpty()) {
					fail("Conjugations are not complete.");
				}
			}
		} catch (IOException | ConjugationReadException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			fail(sw.toString());
		}

	}
}
