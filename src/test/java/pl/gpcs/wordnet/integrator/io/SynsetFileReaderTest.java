package pl.gpcs.wordnet.integrator.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;

import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;

class SynsetFileReaderTest {

	private static final String TEST_RESOURCES = Path.of("src", "test", "resources").toString();

	@Test
	void testRead_plWordnetSynsetWithPolishL_validEntry() throws Exception {
		// prep expected
		final String expectedDomain = "ksz => kształty";
		// validate
		try (SynsetFileReader<PlWordnetSynset> reader = new SynsetFileReader<>(PlWordnetSynset.class,
				Path.of(TEST_RESOURCES, "kwadratPlWordnetSynset.xml"))) {
			final String actualDomain = reader.read().getDomain();
			assertEquals(expectedDomain, actualDomain);
		}
	}

}
