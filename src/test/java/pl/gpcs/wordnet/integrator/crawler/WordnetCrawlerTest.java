package pl.gpcs.wordnet.integrator.crawler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import pl.gpcs.wordnet.integrator.service.WordnetService;
import pl.gpcs.wordnet.integrator.service.impl.WordnetServiceImpl;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.WordnetType;

class WordnetCrawlerTest {

	static final String RESOURCES_ROOT = Paths.get("src", "test", "resources").toString();

	WordnetCrawler wordnetCrawler;
	final WordnetService wordnetService = new WordnetServiceImpl();

	@BeforeEach
	void beforeEachInit() {
		wordnetCrawler = new WordnetCrawler();
	}

	@Test
	void testFindEdgeHypernyms_whenPlWordnetPerdurativeVerbs_thenEntriesOnEdge() {
		Wordnet perdurativeVerbs = wordnetService
				.createWordnetForPlWordnet(Paths.get(RESOURCES_ROOT, "czasowniki perduratywne-DOMAIN.xml"));
		Set<WordnetEntry> wordnetEntries = wordnetCrawler.findEdgeHypernyms(perdurativeVerbs);
		Set<String> actualIds = wordnetEntries.stream().map(WordnetEntry::getId).collect(Collectors.toSet());
		String[] expectedIdsArray = new String[] { "PLWN-00321302-n_pwn", "PLWN-00001720-n", "PLWN-00058644-v",
				"PLWN-00233940-n", "PLWN-07063254-v", "PLWN-00055809-v", "PLWN-00102754-n", "PLWN-00000366-n",
				"PLWN-07079263-v", "PLWN-00000323-n", "PLWN-00010765-n", "PLWN-00006096-n", "PLWN-00000414-v",
				"PLWN-00053623-v", "PLWN-00057024-v", "PLWN-00047401-n", "PLWN-07079729-v", "PLWN-00366831-n_pwn",
				"PLWN-07078236-v", "PLWN-07063525-v", "PLWN-07079727-v", "PLWN-00418464-n", "PLWN-00050721-n",
				"PLWN-00379776-v_pwn", "PLWN-00002324-n", "PLWN-00003998-n", "PLWN-00380159-v_pwn", "PLWN-00103330-n",
				"PLWN-00001088-n", "PLWN-00001524-v", "PLWN-00001083-n", "PLWN-07063244-v", "PLWN-00003163-v",
				"PLWN-00004856-n", "PLWN-00331415-n_pwn", "PLWN-00001168-v", "PLWN-00006045-n", "PLWN-00367057-v_pwn",
				"PLWN-00376601-v_pwn", "PLWN-00247969-n", "PLWN-00006526-n", "PLWN-00006047-n", "PLWN-00368655-v_pwn",
				"PLWN-00003463-v", "PLWN-07071956-v" };
		Set<String> exprectedIds = new HashSet<>(Arrays.asList(expectedIdsArray));
		assertEquals(exprectedIds, actualIds);
	}

	@Test
	void testFindEdgeHypernyms_when3EntriesWordnet_thenTwoEntriesOnHypernymsEdge() {
		Wordnet wordnet = new Wordnet("testNet", WordnetType.PLWORDNET);
		WordnetEntry we1 = new WordnetEntry("id1");
		WordnetEntry we2 = new WordnetEntry("id2");
		WordnetEntry we3 = new WordnetEntry("id3");
		we1.setHyponyms(Collections.singleton(we2));
		we2.setHypernyms(Collections.singleton(we1));
		we2.setHyponyms(Collections.singleton(we3));
		we3.setHypernyms(Collections.singleton(we2));
		wordnet.addAll(List.of(we1, we2, we3));
		Set<WordnetEntry> edgeEntries = wordnetCrawler.findEdgeHypernyms(wordnet, 1);
		assertEquals(Set.of(we1, we2), edgeEntries);
	}

	@Test
	void testFindEdgeHyponyms_whenPlWordnetPerdurativeVerbs_thenEntriesOnEdge() {
		Wordnet perdurativeVerbs = wordnetService
				.createWordnetForPlWordnet(Paths.get(RESOURCES_ROOT, "czasowniki perduratywne-DOMAIN.xml"));
		Set<WordnetEntry> wordnetEntries = wordnetCrawler.findEdgeHyponyms(perdurativeVerbs);
		Set<String> actualIds = wordnetEntries.stream().map(WordnetEntry::getId).collect(Collectors.toSet());
		String[] expectedIdsArray = new String[] { "PLWN-00321302-n_pwn", "PLWN-00001720-n", "PLWN-00065174-v",
				"PLWN-00058644-v", "PLWN-00233940-n", "PLWN-00006112-n", "PLWN-00055809-v", "PLWN-00054719-v",
				"PLWN-00068651-v", "PLWN-00102754-n", "PLWN-00053350-v", "PLWN-00057823-v", "PLWN-07079263-v",
				"PLWN-00065821-v", "PLWN-00000323-n", "PLWN-00006096-n", "PLWN-00000414-v", "PLWN-00047401-n",
				"PLWN-00366831-n_pwn", "PLWN-07063525-v", "PLWN-00418464-n", "PLWN-00001088-n", "PLWN-00065813-v",
				"PLWN-00004856-n", "PLWN-00061530-v", "PLWN-07063281-v", "PLWN-00001168-v", "PLWN-00061018-v",
				"PLWN-00059929-v", "PLWN-00006045-n", "PLWN-00376601-v_pwn", "PLWN-07068565-v", "PLWN-00057032-v",
				"PLWN-07068588-v", "PLWN-07063517-v", "PLWN-07074305-v", "PLWN-00061349-v", "PLWN-00061328-v",
				"PLWN-00063355-v", "PLWN-00006047-n", "PLWN-07078454-v", "PLWN-00368655-v_pwn", "PLWN-00003463-v",
				"PLWN-07071956-v", "PLWN-07063547-v", "PLWN-00053858-v", "PLWN-07079827-v", "PLWN-07071065-v",
				"PLWN-00000366-n", "PLWN-00061027-v", "PLWN-00010765-n", "PLWN-07072155-v", "PLWN-07079729-v",
				"PLWN-07071792-v", "PLWN-07079727-v", "PLWN-00002324-n", "PLWN-00003998-n", "PLWN-00103330-n",
				"PLWN-00001524-v", "PLWN-00001083-n", "PLWN-00065451-v", "PLWN-07063244-v", "PLWN-07079817-v",
				"PLWN-00003163-v", "PLWN-00053363-v", "PLWN-00060082-v", "PLWN-00331415-n_pwn", "PLWN-00367057-v_pwn",
				"PLWN-00057220-v", "PLWN-07074995-v", "PLWN-07068568-v", "PLWN-07076398-v", "PLWN-00247969-n",
				"PLWN-00006526-n", "PLWN-00068058-v", "PLWN-00063354-v" };
		Set<String> exprectedIds = new HashSet<>(Arrays.asList(expectedIdsArray));
		assertEquals(exprectedIds, actualIds);
	}

	@Test
	void testFindEdgeHyponyms_when3EntriesWordnet_then2EntriesOnHyponymsEdge() {
		Wordnet wordnet = new Wordnet("testNet", WordnetType.PLWORDNET);
		WordnetEntry we1 = new WordnetEntry("id1");
		WordnetEntry we2 = new WordnetEntry("id2");
		WordnetEntry we3 = new WordnetEntry("id3");
		we1.setHyponyms(Collections.singleton(we2));
		we2.setHypernyms(Collections.singleton(we1));
		we2.setHyponyms(Collections.singleton(we3));
		we3.setHypernyms(Collections.singleton(we2));
		wordnet.addAll(List.of(we1, we2, we3));
		Set<WordnetEntry> edgeEntries = wordnetCrawler.findEdgeHyponyms(wordnet, 1);
		assertEquals(Set.of(we2, we3), edgeEntries);
	}


}
