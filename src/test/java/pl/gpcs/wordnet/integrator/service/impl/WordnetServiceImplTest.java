package pl.gpcs.wordnet.integrator.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;
import pl.gpcs.wordnet.integrator.exception.InitializationException;
import pl.gpcs.wordnet.integrator.io.SynsetFileReader;
import pl.gpcs.wordnet.integrator.model.synset.plwordnet.PlWordnetSynset;
import pl.gpcs.wordnet.integrator.model.synset.polnet.PolnetSynset;
import pl.gpcs.wordnet.integrator.service.MergingStrategy;
import pl.gpcs.wordnet.integrator.service.WordnetService;
import pl.gpcs.wordnet.integrator.util.GeneralRelation;
import pl.gpcs.wordnet.integrator.util.SynonymEntry;
import pl.gpcs.wordnet.integrator.util.Wordnet;
import pl.gpcs.wordnet.integrator.util.WordnetEntry;
import pl.gpcs.wordnet.integrator.util.WordnetType;

@Slf4j
class WordnetServiceImplTest {

	WordnetService wordnetService;

	@BeforeEach
	void init() {
		wordnetService = new WordnetServiceImpl();
	}

	void cleanUp(Wordnet wordnet) {
		for (WordnetEntry we : wordnet.getAllEntries()) {
			if (we.getHypernyms() != null) {
				we.getHypernyms().clear();
			}
			if (we.getHyponyms() != null) {
				we.getHyponyms().clear();
			}
		}
	}

	@Test
	void CreateWordnetFromPlWordnet_ValidFile_30268Fuzzynims() {
		try (SynsetFileReader<PlWordnetSynset> plWordnetReader = new SynsetFileReader<>(PlWordnetSynset.class,
				Paths.get("src", "main", "resources", "plwordnet_4_2_visdisc.xml"))) {
			Wordnet wordnetForPlWordnet = wordnetService.createWordnetForPlWordnet(plWordnetReader);
			if (wordnetForPlWordnet == null || wordnetForPlWordnet.isEmpty()) {
				fail(Wordnet.class + " is null or empty.");
			}
			int fuzzynimiaSynsetowCount = 0;
			for (WordnetEntry we : wordnetForPlWordnet.getAllEntries()) {

				if (we.getGeneralSemanticRelations() == null) {
					continue;
				}
				for (GeneralRelation<String> relation : we.getGeneralSemanticRelations()) {
					if ("fuzzynimia_synset�w".equals(relation.getRelation())) {
						++fuzzynimiaSynsetowCount;
					}
				}
			}
			if (fuzzynimiaSynsetowCount != 45120) { // 30268 for 2_3
				fail("Not all fuzzynyms were found in wordnet");
			}
		} catch (IOException | InitializationException e) {
			fail("Failed to create " + Wordnet.class + " from " + PlWordnetSynset.class + " reader.");
		}
	}

	@Test
	void testCreateWordnetFromPolnet_whenValidFile_thenEqualHyponymsAndHypernymsCount() {
		try (SynsetFileReader<PolnetSynset> polnetReader = new SynsetFileReader<>(PolnetSynset.class,
				Paths.get("src", "main", "resources", "polnet.xml"))) {
			Wordnet wordnetForPolnet = wordnetService.createWordnetForPolnet(polnetReader);
			assertNotNull(wordnetForPolnet);
			assertFalse(wordnetForPolnet.isEmpty());
			int hypernymCount = 0;
			int hyponymCount = 0;
			for (WordnetEntry wordnetEntry : wordnetForPolnet.getAllEntries()) {
				if (wordnetEntry.getHypernyms() != null) {
					hypernymCount += wordnetEntry.getHypernyms().size();
				}
				if (wordnetEntry.getHyponyms() != null) {
					hyponymCount += wordnetEntry.getHyponyms().size();
				}
			}
			assertEquals(hypernymCount, hyponymCount);
		} catch (IOException | InitializationException e) {
			fail("Failed to create " + Wordnet.class + " from " + PolnetSynset.class + " reader.");
		}

	}

	// XXX: move to WordnetJoiner testing
	@Test
	void testMergeWordnets_whenWholeWordnets_thenEnrichedDestinationWordnet() throws InterruptedException, ExecutionException {
		Future<Wordnet> futurePlWordnet = wordnetService.createWordnetAsync(
				Paths.get("src", "main", "resources", "plwordnet_4_2_visdisc.xml"), WordnetType.PLWORDNET);
		Future<Wordnet> futurePolnet = wordnetService.createWordnetAsync(
				Paths.get("src", "main", "resources", "polnet.xml"),
				WordnetType.POLNET);
		int size = futurePlWordnet.get().size();
		final Wordnet polnet = futurePolnet.get();
		final Wordnet plWordnet = futurePlWordnet.get();
		wordnetService.mergeWordnets(plWordnet, polnet, MergingStrategy.HYPERNYM_ORIGIN, 0);
		assertTrue(futurePlWordnet.get().size() > size);
	}

	void testMergeWordnets_whenWholeWordnetsAndHyponymOriginStrategy_thenEnrichedDestinationWordnet() throws Exception {

	}

	@Test
	void checkPlWordnetEntries_ifCoreFieldsNotNull_thenOk() {
		for (WordnetEntry wordnetEntry : createPlWordnet()) {
			assertNotNull(wordnetEntry);
			assertNotNull(wordnetEntry.getId());
			assertNotNull(wordnetEntry.getPos());
			assertNotNull(wordnetEntry.getSynonyms());
		}
	}

	@Test
	void checkPolnetEntries_ifCoreFieldsNotNull_thenOk() {
		for (WordnetEntry wordnetEntry : createPolnet()) {
			assertNotNull(wordnetEntry);
			assertNotNull(wordnetEntry.getId());
			assertNotNull(wordnetEntry.getPos());
			assertNotNull(wordnetEntry.getSynonyms());
		}
	}

	private Wordnet createPlWordnet() {
		try (SynsetFileReader<PlWordnetSynset> plWordnetReader = new SynsetFileReader<>(PlWordnetSynset.class,
				Paths.get("src", "main", "resources", "plwordnet_4_2_visdisc.xml"))) {
			return wordnetService.createWordnetForPlWordnet(plWordnetReader);
		} catch (IOException | InitializationException e) {
			fail(e);
			throw new RuntimeException(e);
		}
	}

	private Wordnet createPolnet() {
		try (SynsetFileReader<PolnetSynset> polnetReader = new SynsetFileReader<>(PolnetSynset.class,
				Paths.get("src", "test", "resources", "polnet.xml"))) {
			return wordnetService.createWordnetForPolnet(polnetReader);
		} catch (IOException | InitializationException e) {
			fail(e);
			throw new RuntimeException(e);
		}
	}

}
