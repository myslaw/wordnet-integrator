# WordnetIntegrator

**Przemysław Giza**

WordnetIntegrator is a Java application created for sole purpose of integrating and measuring characteristics of chosen lexical databases (wordnets). It was created as a part of master thesis: "Analysis of Polish wordnets in the context of their integration".

## Compiling

Compiling requires Java 8 ([Java 17](https://jdk.java.net/archive/)) recommended to also run the application), lower version might work, but it is not guaranteed. To compile the application please navigate to main folder where gradlew file resides. Then using console run the compilation

```cmd
gradlew clean build
```

After that, navigate to newly created folder:

```cmd
cd build\libs
```

Should contain the WordnetIntegrator.jar file, that is the fully compiled application. To test if everything went according to the plan please run it with help switch:

```cmd
java -jar WordnetIntegrator.jar -h
```

After that operation help menu should be printed and empty wordnet-integrator.log file should be created.

## Running

To run the application [Java 17](https://jdk.java.net/archive/) is required. Lower or higher version might work, but it is not guaranteed.

In order to check what possible arguments does the application accepts, please issue help command:

```cmd
java -jar WordnetIntegrator.jar -h
```

Application requires the usage of two arguments in order to run: `--polnet-path` and `--plwordnet-path` with them both being followed with paths to xml files of PolNet and plWordNet. Input lexical databases of PolNet and plWordNet are not included with this program and can be retrieved at [plWordNet website](http://plwordnet.pwr.wroc.pl/wordnet/download) and [PolNet webiste](http://ltc.amu.edu.pl/polnet/index.php).

After retrieving both xml files and placing them in the same directory as WordnetIntegrator.jar, merging and benchmarking can be both run after issuing following command (please make note that in this case downloaded files are named polnet.xml and plwordnet.xml):

```cmd
java -jar WordnetIntegrator.jar --polnet-path polnet.xml --plwordnet-path plwordnet.xml
```

That should be enough to after a short run create a folder with merged wordnets. All statistics should be printed to the console and wordnet-integrator.log file in latex compatible format.

In order to run merging and benchmarking for different margins, please use `--margins` argument. For example to make merge and benchmarks using margins 0 and 35, following command should be used:

```cmd
java -jar WordnetIntegrator.jar --polnet-path polnet.xml --plwordnet-path plwordnet.xml --margins 0,35
```